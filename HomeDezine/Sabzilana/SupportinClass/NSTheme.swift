//
//  NSTheme.swift
//  NovoNorDisk
//
//  Created by TNM3 on 6/23/15.
//  Copyright (c) 2015 TNM3. All rights reserved.
//

import UIKit

class NSTheme: NSObject {
    
    let kColorAlpha : CGFloat = 1.0
    
   
    func GetNavigationBGColor() -> UIColor {
//        return UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: kColorAlpha)
        return UIColor(hexString: "#a81d4a")
    }
    func GetNavigationTitleColor() -> UIColor {
        return UIColor.white

    }
    func GetSabzilanaRedColor() -> UIColor {
        return UIColor(red: 234/255, green: 73/255, blue: 95/255, alpha: kColorAlpha)
    }
    func GetSabzilanaGreenColor() -> UIColor {
        
//        return UIColor(red: 92/255, green: 204/255, blue: 120/255, alpha: kColorAlpha)
        return UIColor(hexString: "#c32256")
    }
    
}
