//
//  SliderHeader.h
//  Sabzilana
//
//  Created by Jeevan on 04/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderHeader : UIView
@property(nonatomic,retain)IBOutlet UILabel *userLabel;
@property(nonatomic,retain)IBOutlet UIImageView *userImage;

@end
