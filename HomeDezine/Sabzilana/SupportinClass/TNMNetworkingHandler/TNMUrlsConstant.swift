//
//  TNMUrlsConstant.swift
//  StretfordEnd
//
//  Created by TNM3 on 7/15/16.
//  Copyright © 2016 shoebpersonal. All rights reserved.
//

import Foundation

//WishList TableName And Field Name
let kWishListTableName = "WishList"
let kWishListProductID = "productID"

//API Constant
let kAppName = "Home Dezin"
let kMessage = "message"
let kStatusCode = "msgcode"
let kMessageText = "No results found"


//Qty Update Toast Message
let kQtyAddedMessage = "Successfully Added"
let kQtyUpdatedMessage = "Quantity Updated Successfully"




//Device Configuration
let kiTunesAppID = "id1385012163"
let kPlatformName = "iphone"
var kDeviceToken = ""
let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
//Then just cast the object as a String, but be careful, you may want to double check for nil
let kiPhoneAppVersion = nsObject as! String


let kPostMethod = "POST"
let kGetMethod = "GET"

let kLoginResult = "LoginResult"
let kBasketResult = "BasketResult"
let kSearchResult = "SearchResult"
let kBasketQtyKey = "BasketQty"
let kProductItemID = "ProductItemID"

//PayU Credintial
//LiveKey
let kPayUKEY = "Lcm4Cw"
let kPayUSalt = "Wg7lihyb"
//let kPayUKEY = "gtKFFx"
//let kPayUSalt = "eCwWELxi"
let kPayUMode = ENVIRONMENT_PRODUCTION
//Order Surl, FUrl
let kPayUsurl = "https://www.sabzilana.com/sabzi/index.php?view=order_result"
let kPayUfurl = "https://www.sabzilana.com/sabzi/index.php?view=order_result"

//Wallet Surl, FUrl
//https://www.sabzilana.com/sabzi/index.php?view=walletpayu_result
//let kPayUWalletsurl = "https://payu.herokuapp.com/success"
//let kPayUWalletfurl = "https://payu.herokuapp.com/failure"
let kPayUWalletsurl = "https://www.sabzilana.com/sabzi/index.php?view=walletpayu_result"
let kPayUWalletfurl = "https://www.sabzilana.com/sabzi/index.php?view=walletpayu_result"



let pUUIStoryBoard: String = "PUUIMainStoryBoard"
//PayU-----Close-----

//////Sandbox Url
let kMainDomainUrl = "http://www.homedezin.com/mapp/index.php"


func GetJson(_ param : NSDictionary) -> String {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        
        // here "decoded" is of type `AnyObject`, decoded from JSON data
        //            let string = (data: jsonData, encoding: NSUTF8StringEncoding)
        let tempStr = String(data: jsonData, encoding: String.Encoding.utf8)
        return tempStr!
        //Returns "http://nshipster.com/ios9/" URL
    } catch let error as NSError {
        print(error)
    }
    return ""
}

func GetJsonArray(_ param : NSArray) -> String {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: param, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        
        // here "decoded" is of type `AnyObject`, decoded from JSON data
        //            let string = (data: jsonData, encoding: NSUTF8StringEncoding)
        let tempStr = String(data: jsonData, encoding: String.Encoding.utf8)
        return tempStr!
        //Returns "http://nshipster.com/ios9/" URL
    } catch let error as NSError {
        print(error)
    }
    return ""
}
func GetJsonString(_ param : NSMutableArray) {
//    let dic = ["2": "B", "1": "A", "3": "C"]
  
    
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
        // here "jsonData" is the dictionary encoded in JSON data
        
        let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
        // here "decoded" is of type `Any`, decoded from JSON data
        
        // you can now cast it with the right type
        if let dictFromJSON = decoded as? [String:String] {
            // use dictFromJSON
            print(dictFromJSON)
        }
    } catch {
        print(error.localizedDescription)
    }
}

func GetNewDicationary(_ data : NSDictionary) -> NSDictionary{
    let tempData = NSMutableDictionary()
    for object in data.allKeys {
        if let keyStr = object as? String {

            if let _ = data.object(forKey: keyStr) as? NSNull {
                tempData.setObject("", forKey: keyStr as NSCopying)
            }else{
                tempData.setObject(data.object(forKey: keyStr)!, forKey: keyStr as NSCopying)
            }
            
        }
        
    }
    return tempData
}

//MARK: Set & Get Login Data
func setLoginUser(_ data : NSDictionary){
    let dataStore = NSKeyedArchiver.archivedData(withRootObject: data)
    UserDefaults.standard.set(dataStore, forKey: kLoginResult)
    UserDefaults.standard.synchronize()
    
}

func getLoginUser() -> NSDictionary{
    var dict : NSDictionary = NSDictionary()
    if(UserDefaults.standard.object(forKey: kLoginResult) != nil ){
        let outData = UserDefaults.standard.data(forKey: kLoginResult)
        dict = (NSKeyedUnarchiver.unarchiveObject(with: outData!) as? NSDictionary)!
        
    }
    
    return dict
}
func removeLoginUser(){
    
    UserDefaults.standard.removeObject(forKey: kLoginResult)
    UserDefaults.standard.synchronize()
    
}


//MARK: Set & Get Basket Result
func SetBasketLocalResult(_ data : NSArray){
    let dataStore = NSKeyedArchiver.archivedData(withRootObject: data)
    UserDefaults.standard.set(dataStore, forKey: kBasketResult)
    UserDefaults.standard.synchronize()
    
}

func GetBasketLocalResult() -> NSArray{
    var dict : NSArray = NSArray()
    if(UserDefaults.standard.object(forKey: kBasketResult) != nil ){
        let outData = UserDefaults.standard.data(forKey: kBasketResult)
        dict = (NSKeyedUnarchiver.unarchiveObject(with: outData!) as? NSArray)!
        
    }
    
    return dict
}
func EmptySabzilanaBasket(){
    
    UserDefaults.standard.removeObject(forKey: kBasketResult)
    UserDefaults.standard.synchronize()
    
}


//Search List
//MARK: Set & Get Basket Result
func SetSearchResult(_ data : NSArray){
    let dataStore = NSKeyedArchiver.archivedData(withRootObject: data)
    UserDefaults.standard.set(dataStore, forKey: kSearchResult)
    UserDefaults.standard.synchronize()
    
}

func GetSearchResult() -> NSArray{
    var dict : NSArray = NSArray()
    if(UserDefaults.standard.object(forKey: kSearchResult) != nil ){
        let outData = UserDefaults.standard.data(forKey: kSearchResult)
        dict = (NSKeyedUnarchiver.unarchiveObject(with: outData!) as? NSArray)!
        
    }
    
    return dict
}


//Common Funcation
func GetDateFormat(_ format : String, date: String) -> String{
    
    // let myDateString = dic?.objectForKey("weekDay") as? String
    
    if(date.characters.count != 0){
        let somedateString: String?
        
        // convert to NSDate
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss.A"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
//        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        dateFormatter.dateFormat = "yyy/MM/dd HH:mm:ss"
        let myDate = dateFormatter.date(from: date)!

        // convert to required string
        let Formatter = DateFormatter()
        Formatter.locale = Locale.current
//        Formatter.dateFormat = "MMMM dd"
        if format.isEmpty == true {
            Formatter.dateFormat = "dd-MM-yyyy"
        }else{
            Formatter.dateFormat = format
        }
        
        Formatter.timeZone = TimeZone.current
        somedateString = Formatter.string(from: myDate)
        
        return somedateString!
    }
    
    return ""
}

//Common Funcation
func GetDateFromDate(_ format : String, date: NSDate) -> String{
    
    // let myDateString = dic?.objectForKey("weekDay") as? String
    
    let myDate = date
    
    // convert to required string
    let Formatter = DateFormatter()
    Formatter.locale = Locale.current
    //        Formatter.dateFormat = "MMMM dd"
    if format.isEmpty == true {
        Formatter.dateFormat = "dd-MM-yyyy"
    }else{
        Formatter.dateFormat = format
    }
    
    Formatter.timeZone = TimeZone.current
    let somedateString = Formatter.string(from: myDate as Date)
    
    return somedateString
   
   
}


//MARK: - TNM Error Alert -
func ShowMessage(_ title : String, message : String){
    var titleStr = title
    if titleStr.isEmpty == true {
        titleStr = kAppName
    }
    
    let refreshAlert = UIAlertController(title: titleStr, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    //        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
    //
    //        }))
    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
        
        
    }))
    DispatchQueue.main.async {
        appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
            
        })
        
    }
}


