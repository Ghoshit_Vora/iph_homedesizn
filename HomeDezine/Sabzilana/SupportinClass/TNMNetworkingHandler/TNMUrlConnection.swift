//
//  TNMUrlConnection.swift
//  APlantApprove
//
//  Created by TNM3 on 10/4/16.
//  Copyright © 2016 tnmmac4. All rights reserved.
//

import Foundation

func TNMPost(_ params : Dictionary<String, String>, url : String, postCompleted : @escaping (_ succeeded: Bool, _ msg: String) -> ()) {
    //appDelegate.ShowProgress()
    
    var request = URLRequest(url: URL(string: url)!)
    let session = URLSession.shared
    request.httpMethod = "POST"
    
    request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
    
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
        //appDelegate.HideProgress()
        print("Response: \(response)")
        let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        print("Body: \(strData)")
        
        let json: AnyObject? = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as AnyObject?
        
        // check and make sure that json has a value using optional binding.
        if let parseJSON = json {
            // Okay, the parsedJSON is here, let's get the value for 'success' out of it
            if let success = parseJSON["success"] as? Bool {
                print("Succes: \(success)")
                postCompleted(success, "Logged in.")
            }
            
        }
        else {
            // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
            let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Error could not parse JSON: \(jsonStr)")
            postCompleted(false, "Error")
        }
        
        
    })
    
    task.resume()
}

func TNMWSMethod(_ params : AnyObject?, url : String,isMethod : String, AuthToken : String, postCompleted : @escaping (_ succeeded: Bool, _ data: AnyObject) -> ()) {

    
    
//    DispatchQueue.main.async {
//
////        MOHUD.showSubtitle(title: "Loading", subtitle: "Please wait...", withCancelAndContinue: false)
//    }
    
    if isRechable() == false {
//          let dt : NSDictionary = NSDictionary(object: "The Internet seems to be unavailable. Please check your connection and try again.", forKey: kMessage as NSCopying)
//        postCompleted(false, dt)

        let alertController = JHTAlertController(title: "Oops!!!", message: "The Internet seems to be unavailable. Please check your connection and try again.", preferredStyle: .alert, iconImage: #imageLiteral(resourceName: "internet") )
        alertController.titleViewBackgroundColor = .white
        alertController.titleTextColor = .black
        alertController.alertBackgroundColor = .white
        alertController.messageFont = .systemFont(ofSize: 15)
        alertController.messageTextColor = .black
        alertController.setAllButtonBackgroundColors(to: .white)
        alertController.setButtonTextColorFor(.default, to: .black)
        alertController.setButtonTextColorFor(.cancel, to: .black)
        alertController.dividerColor = .black
        alertController.hasRoundedCorners = true
        
        // Create the action.
        let cancelAction = JHTAlertAction(title: "Cancel", style: .cancel,  handler: nil)
        let okAction = JHTAlertAction(title: "Retry", style: .default) { _ in
            
            if isRechable() == false {
                DispatchQueue.main.async {
                    appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                    MOHUD.showWithStatus("Loading")
                    
                }
                let requestUrl = url.EncodeUrlFormat(text: url)
                var request = URLRequest(url: URL(string: requestUrl)!)
                let session = URLSession.shared
                request.httpMethod = isMethod
                if params != nil {
                    request.httpBody = try! JSONSerialization.data(withJSONObject: params!, options: [])
                }
                
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
                
                if AuthToken.isEmpty == false {
                    request.addValue(AuthToken, forHTTPHeaderField: "Authorisation-Token")
                }
                
                let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
                    
                    
                    var statusCode : Int = 0
                    if let status = response as? HTTPURLResponse {
                        print("status code \(status.statusCode)")
                        statusCode = status.statusCode
                    }
                    
                    DispatchQueue.main.async {
                        MOHUD.dismiss()
                    }
                    
                    if data == nil {
                        print("%@",error.debugDescription)
                        let dt : NSDictionary = NSDictionary(object: "Something went wrong please try again later.", forKey: kMessage as NSCopying)
                        postCompleted(false, dt)
                    }else{
                        
                        do {
                            
                            if statusCode == 200 {
                                
                                let json: AnyObject? = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject?
                                
                                
                                // check and make sure that json has a value using optional binding.
                                if let parseJSON = json {
                                    // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                                    
                                    if statusCode == 200 {
                                        postCompleted(true, parseJSON)
                                    }else{
                                        postCompleted(false, parseJSON)
                                    }
                                    
                                    
                                }else if statusCode == 200 {
                                    let dt : NSDictionary = NSDictionary(object: kMessageText, forKey: kMessage as NSCopying)
                                    postCompleted(true, dt)
                                }
                                else {
                                    
                                    // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                                    print("Error could not parse JSON: \(jsonStr)")
                                    let dt : NSDictionary = NSDictionary(object: jsonStr!, forKey: kMessage as NSCopying)
                                    postCompleted(false, dt)
                                    
                                }
                            }
                            else {
                                
                                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                                print("Error could not parse JSON: \(jsonStr)")
                                let dt : NSDictionary = NSDictionary(object: jsonStr!, forKey: kMessage as NSCopying)
                                postCompleted(false, dt)
                                
                            }
                            
                            
                            
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                        
                        
                    }
                    
                })
                
                task.resume()

            }

        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        
        // Show alert
        DispatchQueue.main.async {
             appDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
       
        
        DispatchQueue.main.async {
            MOHUD.dismiss()
        }
        
    }else{
        MOHUD.showWithStatus("Loading")
        print(url)
        let requestUrl = url.EncodeUrlFormat(text: url)
        var request = URLRequest(url: URL(string: requestUrl)!)
        let session = URLSession.shared
        request.httpMethod = isMethod
        if params != nil {
            request.httpBody = try! JSONSerialization.data(withJSONObject: params!, options: [])
        }
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        
        if AuthToken.isEmpty == false {
            request.addValue(AuthToken, forHTTPHeaderField: "Authorisation-Token")
        }
        
        let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
            DispatchQueue.main.async {
                MOHUD.dismiss()
            }
            
            
            var statusCode : Int = 0
            if let status = response as? HTTPURLResponse {
                print("status code \(status.statusCode)")
                statusCode = status.statusCode
            }
            
            
            
            if data == nil {
                print("%@",error.debugDescription)
                let dt : NSDictionary = NSDictionary(object: "Something went wrong please try again later.", forKey: kMessage as NSCopying)
                postCompleted(false, dt)
            }else{
                
                do {
                    
                    if statusCode == 200 {
                        print(data)
                        let json: AnyObject? = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as AnyObject?
                        
                        
                        // check and make sure that json has a value using optional binding.
                        if let parseJSON = json {
                            // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                            
                            if statusCode == 200 {
                                postCompleted(true, parseJSON)
                            }else{
                                postCompleted(false, parseJSON)
                            }
                            
                            
                        }else if statusCode == 200 {
                            let dt : NSDictionary = NSDictionary(object: kMessageText, forKey: kMessage as NSCopying)
                            postCompleted(true, dt)
                        }
                        else {
                            
                            // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                            let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                            print("Error could not parse JSON: \(jsonStr)")
                            let dt : NSDictionary = NSDictionary(object: jsonStr!, forKey: kMessage as NSCopying)
                            postCompleted(false, dt)
                            
                        }
                    }
                    else {
                        
                        // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                        let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print("Error could not parse JSON: \(jsonStr)")
                        let dt : NSDictionary = NSDictionary(object: jsonStr!, forKey: kMessage as NSCopying)
                        postCompleted(false, dt)
                        
                    }
                    
                    
                    
                    
                } catch {
                    print(error.localizedDescription)
                }
                
                
            }
            
        })
        
        task.resume()

    }
    
    
}

func TNMWSMethodNoLoading(_ params : AnyObject?, url : String,isMethod : String, AuthToken : String, postCompleted : @escaping (_ succeeded: Bool, _ data: AnyObject) -> ()) {
    
    if isRechable() == false {
        
        let dt : NSDictionary = NSDictionary(object: "The Internet seems to be unavailable. Please check your connection and try again.", forKey: kMessage as NSCopying)
        postCompleted(false, dt)
        
        return
    }
    
    //appDelegate.ShowProgress()
    var request = URLRequest(url: URL(string: url)!)
    let session = URLSession.shared
    request.httpMethod = isMethod
    if params != nil {
        request.httpBody = try! JSONSerialization.data(withJSONObject: params!, options: [])
    }
    
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
    
    if AuthToken.isEmpty == false {
        request.addValue(AuthToken, forHTTPHeaderField: "Authorisation-Token")
    }
    
    let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
        
        
        var statusCode : Int = 0
        if let status = response as? HTTPURLResponse {
            print("status code \(status.statusCode)")
            statusCode = status.statusCode
        }
        
        
        if data == nil {
            print("%@",error.debugDescription)
            let dt : NSDictionary = NSDictionary(object: "Something went wrong please try again later.", forKey: kMessage as NSCopying)
            postCompleted(false, dt)
        }else{
            let json: AnyObject? = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as AnyObject?
            
            
            // check and make sure that json has a value using optional binding.
            if let parseJSON = json {
                // Okay, the parsedJSON is here, let's get the value for 'success' out of it
                
                if statusCode == 200 {
                    postCompleted(true, parseJSON)
                }else{
                    postCompleted(false, parseJSON)
                }
                
                
            }else if statusCode == 200 {
                let dt : NSDictionary = NSDictionary(object: kMessageText, forKey: kMessage as NSCopying)
                postCompleted(true, dt)
            }
            else {
                
                // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: \(jsonStr)")
                let dt : NSDictionary = NSDictionary(object: jsonStr!, forKey: kMessage as NSCopying)
                postCompleted(false, dt)
                
            }
        }
        
    })
    
    task.resume()
}

func isRechable() -> Bool{
    var isConnected : Bool = false
    let status = Reachability().connectionStatus()
    switch status {
    case .unknown, .offline:
        print("Not connected")
        isConnected = false
    case .online(.wwan):
        print("Connected via WWAN")
        isConnected = true
    case .online(.wiFi):
        print("Connected via WiFi")
        isConnected = true
    }
    return isConnected
}
