//
//  FeedbackViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 05/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class HelpNSupportViewController: UIViewController, BSKeyboardControlsDelegate {

    var keyboard: BSKeyboardControls!
    
    @IBOutlet var textUserName : ACFloatingTextfield!
    @IBOutlet var textEmail : ACFloatingTextfield!
    @IBOutlet var textMobile : ACFloatingTextfield!
    @IBOutlet var textMessage : ACFloatingTextfield!
    
    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    
    
    @IBAction func submitClick(){
        self.SubmitFeedback()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableview.tableHeaderView = self.headerView
        
        //Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedbackViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [self.textUserName,self.textEmail,self.textMobile,self.textMessage])
        
        keyboard.delegate = self
        
        
        self.SetLoginUserResult()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Help Center"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func SetLoginUserResult(){
        let data = getLoginUser()
        print(data)
        if let tempStr = data.object(forKey: "name") as? String {
            self.textUserName.text = tempStr
        }
        if let tempStr = data.object(forKey: "email") as? String {
            self.textEmail.text = tempStr
        }
        if let tempStr = data.object(forKey: "phone") as? String {
            self.textMobile.text = tempStr
        }
    }
    
    func SubmitFeedback(){
        
        if textUserName.text!.isEmpty {
            ShowMessage("", message: "Please enter your name")
            return
        }
        else if textEmail.text!.isEmpty {
            ShowMessage("", message: "Please enter your email")
            return
            
        }
        else if textMobile.text!.isEmpty {
            ShowMessage("", message: "Please enter your phone number")
            return
        }
        else if textMessage.text!.isEmpty {
            
            ShowMessage("", message: "Please enter your message")
            return
        }
        
        let urlStr = String(format: "%@?view=help&userID=%@&name=%@&email=%@&phone=%@&msg=%@&phoneType=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,self.textUserName.text!,self.textEmail.text!,self.textMobile.text!,self.textMessage.text!,kPlatformName])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            if let message = result.object(forKey: "message") as? String {
                                self.ShowAlert(title: "", message: message)
                            }
                        }
                    }
                }
            }
        }
        
    }
    func ShowAlert(title : String, message : String){
        var newTitle = ""
        if title.isEmpty == true {
            newTitle = kAppName
        }else{
            newTitle = title
        }
        let alert=UIAlertController(title: newTitle, message: message, preferredStyle: UIAlertControllerStyle.alert);
        
        let okClick: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
            //Code for launching the camera goes here
            self.navigationController!.popViewController(animated: true)
        }
        alert.addAction(okClick)
        //event handler with closure
        DispatchQueue.main.async
            {
                self.present(alert, animated: true, completion: nil);
        }
        
    }
    
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
