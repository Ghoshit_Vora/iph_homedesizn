//
//  TopCategoryCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class HomeCategory2Cell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    var delegate : DeskBoardCategoryDelegate?
    @IBOutlet var collectionView : UICollectionView!
    var ResponseResult = NSArray() {
        didSet{
            self.LoadResult()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let nibName5 = UINib(nibName: "Category2CollectionCell", bundle:nil)
        self.collectionView.register(nibName5, forCellWithReuseIdentifier: "Category2CollectionCell")
        
       
    }
    func LoadResult(){
        if self.collectionView != nil {
            DispatchQueue.main.async
                {
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.ReloadWithAnimation()
                    
            }
        }else{
            self.perform(#selector(HeaderHomeCell.LoadResult), with: nil, afterDelay: 0.2)
        }
        
    }

    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {

//        return CGSize(width: 100, height: 100);
        return CGSize(width: 150, height: 90);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ResponseResult.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.ResponseResult.count > indexPath.row {
            if let result = self.ResponseResult.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Category2CollectionCell", for: indexPath) as? Category2CollectionCell {
                    cell.resultData = result
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if self.delegate != nil {
            if let cell = collectionView.cellForItem(at: indexPath) as? Category2CollectionCell {
                self.delegate?.didLoadCategoryResult(data: cell.resultData)
            }
        }
    }
    
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
