//
//  Category2CollectionCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class Category2CollectionCell: UICollectionViewCell {

    @IBOutlet var imageview : UIImageView!
    var resultData = NSDictionary(){
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        if self.imageview != nil {
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
                
            }
        }else{
            self.perform(#selector(HeaderCollectionCell.SetResultUI))
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
