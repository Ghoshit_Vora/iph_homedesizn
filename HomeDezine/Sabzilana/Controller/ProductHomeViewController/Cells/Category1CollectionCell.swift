//
//  Category1CollectionCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class Category1CollectionCell: UICollectionViewCell {

    @IBOutlet var imageview : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    
    var resultData = NSDictionary(){
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        if self.imageview != nil {
            if let tempStr = resultData.object(forKey: "icon") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
                
            }
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.titleLabel.text = tempStr
            }
        }else{
            self.perform(#selector(HeaderCollectionCell.SetResultUI))
        }
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
