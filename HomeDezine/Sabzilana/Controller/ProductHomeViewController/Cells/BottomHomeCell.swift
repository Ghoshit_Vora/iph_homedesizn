//
//  TopCategoryCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class BottomHomeCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var delegate : DeskBoardCategoryDelegate?
    
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var pageControl : UIPageControl!
    var ResponseResult = NSArray() {
        didSet{
            self.LoadResult()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let nibName5 = UINib(nibName: "HeaderCollectionCell", bundle:nil)
        self.collectionView.register(nibName5, forCellWithReuseIdentifier: "HeaderCollectionCell")
        
        
    }

    func LoadResult(){
        if self.collectionView != nil {
            DispatchQueue.main.async
                {
                    if self.ResponseResult.count > 1 {
                       self.pageControl.numberOfPages = self.ResponseResult.count
                        self.pageControl.isHidden = false
                    }else{
                        self.pageControl.isHidden = true
                    }
                    
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.ReloadWithAnimation()
            }
        }else{
            self.perform(#selector(HeaderHomeCell.LoadResult), with: nil, afterDelay: 0.2)
        }
        
    }
    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {

//        return CGSize(width: 100, height: 100);
        return CGSize(width: UIScreen.main.bounds.width, height: 150);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ResponseResult.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.ResponseResult.count > indexPath.row {
            if let result = self.ResponseResult.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionCell", for: indexPath) as? HeaderCollectionCell {
                    cell.resultData = result
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
//        if let url = URL(string: "http://www.thedezine.in/") {
//            UIApplication.shared.openURL(url)
//        }
//        
        
        
//        if self.delegate != nil {
//            if let cell = collectionView.cellForItem(at: indexPath) as? HeaderCollectionCell {
//                self.delegate?.didLoadCategoryResult(data: cell.resultData)
//            }
//        }
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
