//
//  ProductHomeViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
let kOrderPaymentStatusNotification = "OrderPaymentStatusNotification"

protocol DeskBoardCategoryDelegate {
    func didLoadCategoryResult(data : NSDictionary)
}

class ProductHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DeskBoardCategoryDelegate, VKSideMenuDelegate, VKSideMenuDataSource {

    var barButtonBasket : MJBadgeBarButton = MJBadgeBarButton()
    
    @IBOutlet var tableview : UITableView!
    var NoOfResultCell = NSMutableArray()
    
    var menuLeft: VKSideMenu!
    
    var sliderResult : NSArray = []

    
    @IBOutlet var bottomAdView : UIView?
    @IBOutlet var adImageView : UIImageView?
    
    func SetLoginNLogoutField(){
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            sliderResult = [["title" : "Categories","image":"ic_category"],
                            ["title" : "Offers","image":"ic_offer"],
                            ["title" : "Inquiry List","image":"getquote"],
                            ["title" : "Refer","image":"groupmenu"],
                            ["title" : "About Us","image":"aboutside"],
                            
                            ["title" : "Rate Us","image":"ic_rate"],
                            ["title" : "Need Help","image":"helpmenu"],
                            ["title" : "Contact Us","image":"contact"],
                            ["title" : "Login","image":"ic_logout"]
                            ]
        }else{
            sliderResult = [["title" : "Categories","image":"ic_category"],
                            ["title" : "Offers","image":"ic_offer"],
                            ["title" : "Inquiry List","image":"getquote"],
                            ["title" : "Refer","image":"groupmenu"],
                            ["title" : "About Us","image":"aboutside"],
                            
                            ["title" : "Rate Us","image":"ic_rate"],
                            ["title" : "Need Help","image":"helpmenu"],
                            ["title" : "Contact Us","image":"contact"],
                            ["title" : "Logout","image":"ic_logout"]
            ]
            
//            sliderResult = [["title" : "Categories","image":"ic_category"],["title" : "Your Last Choice","image":"ic_myaccount"],
//                                          ["title" : "Offers","image":"ic_offer"],
//                                          ["title" : "My Wallet","image":"ic_bank"],
//                                          ["title" : "My Wishlist","image":"ic_mywishlist"],
//                                          ["title" : "Inquiry List","image":"getquote"],
//
//                                          ["title" : "Refer","image":"groupmenu"],
//                                          ["title" : "About Us","image":"ic_about"],
//
//                                          ["title" : "Rate Us","image":"ic_rate"],
//                                          ["title" : "Need Help","image":"helpmenu"],
//                                          ["title" : "Contact Us","image":"contact"],
//
//                                          ["title" : "Logout","image":"ic_logout"]
//            ]

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.SetLoginNLogoutField()
        
        self.navigationController?.navigationBar.topItem?.title = "Home Dezin"
        
        /*
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true // Navigation bar
            self.navigationItem.largeTitleDisplayMode = .never
            navigationItem.title = "Home Dezin"
//            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
//            navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 0/255, green: 150/255, blue: 136/255, alpha: 1.0)
            
            let searchController = UISearchController(searchResultsController: nil) // Search Controller
           
            navigationItem.hidesSearchBarWhenScrolling = false
            navigationItem.searchController = searchController
            searchController.searchBar.placeholder = "Find Products"
          
            
            
            if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
                
                
//            textfield.isEnabled = false
                let tap = UITapGestureRecognizer(target: self, action: #selector(SearchClick))
                textfield.placeholder = "Find Products"
                
                
                
                
                textfield.addGestureRecognizer(tap)
                textfield.isUserInteractionEnabled = true
                if let backgroundview = textfield.subviews.first {

                    backgroundview.backgroundColor = UIColor.gray
                    backgroundview.layer.cornerRadius = 10
                    backgroundview.clipsToBounds = true
                   
                }
                
              

            }
            
           
 
        }else{
            let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductHomeViewController.SearchClick))
            
            self.navigationItem.rightBarButtonItems = [search]
        }
       */
        
        let nibName = UINib(nibName: "HeaderHomeCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "HeaderHomeCell")
        
        let nibName1 = UINib(nibName: "HomeCategory1Cell", bundle: nil)
        self.tableview .register(nibName1, forCellReuseIdentifier: "HomeCategory1Cell")
        
        let nibName2 = UINib(nibName: "HomeCategory2Cell", bundle: nil)
        self.tableview .register(nibName2, forCellReuseIdentifier: "HomeCategory2Cell")
        
        let nibName3 = UINib(nibName: "ProductTableCell", bundle: nil)
        self.tableview .register(nibName3, forCellReuseIdentifier: "ProductTableCell")
        
        let nibName4 = UINib(nibName: "BottomHomeCell", bundle: nil)
        self.tableview .register(nibName4, forCellReuseIdentifier: "BottomHomeCell")
        
        
        let nibName5 = UINib(nibName: "AdBottomHomeCell", bundle: nil)
        self.tableview .register(nibName5, forCellReuseIdentifier: "AdBottomHomeCell")
        
        self.perform(#selector(ProductHomeViewController.GetHomeResult), with: nil, afterDelay: 1.0)
        
        //Slider Menu
        self.SliderMenu()
        
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(buttonMenuLeft))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "sliderMenu"), style: .plain, target: self, action: #selector(buttonMenuLeft))

        self.BarButtonBasket()
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "newcart"), style: .plain, target: self, action: #selector(BasketClic))
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showOrderHistoryViewController(_:)), name: NSNotification.Name(rawValue: kOrderPaymentStatusNotification), object: nil)
        
//        self.GetWishListResult()
    }
//    @objc func handleTap() {
//        // handling code
//        print("Clicked")
//    }
    //MARK:- Payment Status Notification -
    
    
    // handle notification
    func showOrderHistoryViewController(_ notification: NSNotification) {
        let viewController = OrderHistoryViewController(nibName: "OrderHistoryViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.title = "Home Dezin"
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d", arguments: [totalItems])
        self.barButtonBasket.badgeValue = priceStr
        
        
        
        /*
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
            let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductHomeViewController.SearchClick))
            
            self.navigationItem.rightBarButtonItems = [search]

        }
        */
    }
    override func viewWillDisappear(_ animated: Bool) {
        /*
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
            let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductHomeViewController.SearchClick))
           
            self.navigationItem.rightBarButtonItems = [search]
            

        }
         */
    }
    func BarButtonBasket(){
        
        let rightButtonActivity = UIButton(frame: CGRect(x: 0,y: 0,width: 40,height: 40))
        rightButtonActivity.addTarget(self, action: #selector(BasketClic), for: UIControlEvents.touchUpInside)
        rightButtonActivity.setImage(UIImage(named: "newcart"), for: UIControlState())
        rightButtonActivity.setTitleColor(UIColor.blue, for: UIControlState())
        rightButtonActivity.sizeToFit()
        
        self.barButtonBasket = MJBadgeBarButton()
        self.barButtonBasket.setup(rightButtonActivity)
        self.barButtonBasket.badgeValue = ""
        self.barButtonBasket.badgeOriginX = 10.0
        self.barButtonBasket.badgeOriginY = -6
//        self.navigationItem.rightBarButtonItem = self.barButtonBasket
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 0.0
//        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductHomeViewController.SearchClick))
//
//        self.navigationItem.rightBarButtonItems = [search]
//        self.navigationItem.rightBarButtonItems = [self.barButtonBasket,fixedSpace,search]
    }
    @objc func SearchClick(){
        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    @IBAction func searchBarClick(){
        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    func BasketClic(){
        let objRoot = BasketViewController(nibName: "BasketViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    func SliderMenu(){
        if UIScreen.main.bounds.size.width > 320 {
            menuLeft = VKSideMenu(size: 250, andDirection: .fromLeft)
        }else{
            menuLeft = VKSideMenu(size: 220, andDirection: .fromLeft)
        }
        
        let data = getLoginUser()
        print(data)
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            menuLeft.isLoggedUser = false
        }else{
            menuLeft.isLoggedUser = true
        }
        
        menuLeft.loginUser = data as! [AnyHashable : Any]
        menuLeft.dataSource = self
        menuLeft.delegate = self
        menuLeft.addSwipeGestureRecognition(view)
    }
    @IBAction func buttonMenuLeft() {
        UIApplication.shared.statusBarView?.backgroundColor = NSTheme().GetNavigationBGColor()
        
        menuLeft.show()
    }
    
    //  Converted with Swiftify v1.0.6292 - https://objectivec2swift.com/
    // MARK: - VKSideMenuDataSource
    
    func numberOfSections(in sideMenu: VKSideMenu) -> Int {
        return 1
    }
    
    func sideMenu(_ sideMenu: VKSideMenu, numberOfRowsInSection section: Int) -> Int {
       return self.sliderResult.count
    }
    func sideMenu(_ sideMenu: VKSideMenu, itemForRowAt indexPath: IndexPath) -> VKSideMenuItem {
        // This solution is provided for DEMO propose only
        // It's beter to store all items in separate arrays like you do it in your UITableView's. Right?
        let item = VKSideMenuItem()
        if let data = sliderResult.object(at: indexPath.row) as? NSDictionary {
            if let tempStr = data.object(forKey: "title") as? String {
                item.title = tempStr
            }
            if let tempStr = data.object(forKey: "image") as? String {
                item.icon = UIImage(named: tempStr)
            }
        }
        
        return item

    }
    // MARK: - VKSideMenuDelegate
    
    func sideMenu(_ sideMenu: VKSideMenu, didSelectRowAt indexPath: IndexPath) {
        print("SideMenu didSelectRow: \(indexPath)")
        if sliderResult.count > indexPath.row {
            if let data = sliderResult.object(at: indexPath.row) as? NSDictionary {
                if let tempStr = data.object(forKey: "title") as? String {
                    if tempStr.caseInsensitiveCompare("logout") == .orderedSame {
                        DispatchQueue.main.async {
                            //remove case data
                            let refreshAlert = UIAlertController(title: kAppName, message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                            
                            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                                
                            }))
                            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                                
                                removeLoginUser()
                                appDelegate.SabzilanaLoginUserId = ""
                                let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
                                appDelegate.navigationController = UINavigationController(rootViewController: viewController)
                                appDelegate.styleNavigationController(appDelegate.navigationController)
                                appDelegate.window?.rootViewController = appDelegate.navigationController
                            }))
                            DispatchQueue.main.async {
                                appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                                    
                                })
                                
                            }
                        }
                    }else if tempStr.caseInsensitiveCompare("login") == .orderedSame {
                        DispatchQueue.main.async {
                            //remove case data
                            appDelegate.SabzilanaLoginUserId = ""
                            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
                            appDelegate.navigationController = UINavigationController(rootViewController: viewController)
                            appDelegate.styleNavigationController(appDelegate.navigationController)
                            appDelegate.window?.rootViewController = appDelegate.navigationController
                        }
                    }
                    else if tempStr.caseInsensitiveCompare("Categories") == .orderedSame {
                        DispatchQueue.main.async {
                           
                            let viewController = CategoriesViewController(nibName: "CategoriesViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("About Us") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = AboutUsViewController(nibName: "AboutUsViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Feedback") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = FeedbackViewController(nibName: "FeedbackViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Need Help") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = HelpNSupportViewController(nibName: "HelpNSupportViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Contact Us") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = ContactUsViewController(nibName: "ContactUsViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Offers") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = OfferNPromotionViewController(nibName: "OfferNPromotionViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Your Last Choice") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = ProductViewController(nibName: "ProductViewController", bundle: nil)
                            viewController.isLastChoiceEnable = true
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("My Wishlist") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = ProductViewController(nibName: "ProductViewController", bundle: nil)
                            viewController.isWishListEnable = true
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                    else if tempStr.caseInsensitiveCompare("Refer") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = ReferFriendsViewController(nibName: "ReferFriendsViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Inquiry List") == .orderedSame {
                        DispatchQueue.main.async {
                            if appDelegate.SabzilanaLoginUserId.isEmpty == true {
                                appDelegate.LoginRequired(message: "")
                            }else{
                                let viewController = InquiryListViewController(nibName: "InquiryListViewController", bundle: nil)
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                            
                            
                            
                        }
                    }
                    else if tempStr.caseInsensitiveCompare("My Wallet") == .orderedSame {
                        DispatchQueue.main.async {
                            
                            let viewController = WalletHistoryViewController(nibName: "WalletHistoryViewController", bundle: nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }else if tempStr.caseInsensitiveCompare("Order History") == .orderedSame {
                        
                        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
                            appDelegate.LoginRequired(message: "Please login to check orders.")
                        }else{
                            DispatchQueue.main.async {
                                
                                let viewController = OrderHistoryViewController(nibName: "OrderHistoryViewController", bundle: nil)
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                        
                    }
                    else if tempStr.caseInsensitiveCompare("Rate Us") == .orderedSame {
                        self.rateApp(appId: kiTunesAppID) { success in
                            print("RateApp \(success)")
                        }
                    }
                }
                
            }

        }
        
    }
    
    func sideMenuMyAccount(){
        
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
            appDelegate.navigationController = UINavigationController(rootViewController: viewController)
            appDelegate.styleNavigationController(appDelegate.navigationController)
            appDelegate.window?.rootViewController = appDelegate.navigationController
        }else{
            let viewController = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
 
        
    }
    func sideMenuDidShow(_ sideMenu: VKSideMenu) {
        var menu: String = ""
        if sideMenu == menuLeft {
            menu = "LEFT"
        }
        
        
        print("\(menu) VKSideMenue did show")
    }
    func sideMenuDidHide(_ sideMenu: VKSideMenu) {
        var menu: String = ""
        if sideMenu == menuLeft {
            menu = "LEFT"
        }
        
        
        print("\(menu) VKSideMenue did hide")
    }
    func sideMenu(_ sideMenu: VKSideMenu, titleForHeaderInSection section: Int) -> String {
//        if sideMenu == menuLeft {
//            return "Profile"
//        }
        return ""
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NoOfResultCell.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.NoOfResultCell.count > indexPath.row {
            
            if let _ = self.NoOfResultCell.object(at: indexPath.row) as? HeaderHomeCell {
                return 150
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? HomeCategory1Cell {
                return 260
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? HomeCategory2Cell {
                return 90
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
                return 320
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? BottomHomeCell {
                return 150
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? AdBottomHomeCell {
                return 180
            }
        }
//        
//        if indexPath.row == 0 {
//            return 150
//        }else if indexPath.row == 1 {
//            return 125
//        }else if indexPath.row == 2 {
//            return 65
//        }else if indexPath.row == 3 {
//            return 310
//        }

        return 0
//        return UITableViewAutomaticDimension
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.NoOfResultCell.count > indexPath.row {
            
            if let cell = self.NoOfResultCell.object(at: indexPath.row) as? HeaderHomeCell {
                cell.delegate = self
                return cell
            } else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? HomeCategory1Cell {
                cell.delegate = self
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? HomeCategory2Cell {
                cell.delegate = self
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? BottomHomeCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? AdBottomHomeCell {
                return cell
            }
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func GetHomeResult(){
        
//        http://www.homedezin.com/mapp/index.php?view=dashboard&userID=&deviceID=&token=&androidV=&userType=iphone
        
        let urlStr = String(format: "%@?view=dashboard&userID=%@&androidV=%@&userType=%@&token=%@&deviceID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,kiPhoneAppVersion,kPlatformName,kDeviceToken,kDeviceToken])
        print(urlStr)
        //        let data = ["MobileNo" : loginId, "Password" : loginPassword, "Application" : kAppNameInAPI, "WebLogin" : kFalseAPI, "GcmID" : appDelegate.GCMToken, "DeviceID" : appDelegate.iOSDeviceToken]
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                //HeadeBanner
                if let result = data as? NSDictionary {
                    
                    if self.CheckVersionNUnderConstrucation(data: result) == true {
                        return
                    }
                    else if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "2" {
                            
                            if let message = result.object(forKey: "message") as? String {
                                ShowMessage("", message: message)
                                
                                removeLoginUser()
                                
                                let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
                                appDelegate.navigationController = UINavigationController(rootViewController: viewController)
                                appDelegate.styleNavigationController(appDelegate.navigationController)
                                appDelegate.window?.rootViewController = appDelegate.navigationController
                                return
                            }
                        }
                    }
                    
                    //MARK:- Freecharge Delivery Message -
                    var cart_amount = "0"
                    if let tempString = result.object(forKey: "cart_amount") as? String {
                        cart_amount = tempString
                    }
                    if let tempString = result.object(forKey: "cart_amount") as? NSNumber {
                        cart_amount = tempString.stringValue
                    }
                    if cart_amount.isEmpty == false {
                        if let tempFloat = Float(cart_amount) {
                            ChipChargeMinCartAmount = tempFloat
                        }
                    }
                    if let tempString = result.object(forKey: "cart_msg") as? String {
                        ChipChargeCartMessage = tempString
                    }
                    //---------------------------------//
                    
                    if let resultList = result.object(forKey: "banner_list") as? NSArray {
                       
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "HeaderHomeCell") as? HeaderHomeCell {
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }
                //Home Category List
                if let result = data as? NSDictionary {
                    if let resultList = result.object(forKey: "home_category_list") as? NSArray {
//                        print(resultList)
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "HomeCategory1Cell") as? HomeCategory1Cell {
                            cell.ResponseResult = resultList
                            
                            self.NoOfResultCell.add(cell)
//                            if self.NoOfResultCell.count > 1 {
//                                index = self.NoOfResultCell.count - 1
//                            }
//                            self.NoOfResultCell.insert(cell, at: index)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }
                
                //New arrivals
                if let result = data as? NSDictionary {
                    if let resultList = result.object(forKey: "product_list") as? NSArray {
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                            cell.delegate = self
                            cell.productTitleLabel.text = "HOT SELLING PRODUCTS"
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }
               
                
                //Offer Banner List
                if let result = data as? NSDictionary {
                    if let resultList = result.object(forKey: "offer_banner") as? NSArray {
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "HomeCategory2Cell") as? HomeCategory2Cell {
                            cell.ResponseResult = resultList

                            self.NoOfResultCell.add(cell)
//                            var index = 0
//                            if self.NoOfResultCell.count > 1 {
//                                index = self.NoOfResultCell.count - 1
//                            }
//                            self.NoOfResultCell.insert(cell, at: index)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }

                
            }
            DispatchQueue.main.async
                {
                    self.GetHomeProduct()
            }
            
        }
        
    }

    func CheckVersionNUnderConstrucation(data : NSDictionary) -> Bool {
        var isSomethingWrong : Bool = false
        //Under Construcation
        if let tempStr = data.object(forKey: "under_construction") as? String {
            
            if tempStr.caseInsensitiveCompare("yes") == .orderedSame {
                if let message = data.object(forKey: "iphone_msg") as? String {
                    
                    isSomethingWrong = true
                    
                    let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    
                    //        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
                    //
                    //        }))
                    refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                        exit(0)
                        
                    }))
                    DispatchQueue.main.async {
                        appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                            
                        })
                        
                    }
                }
            }else if let version = data.object(forKey: "iphone_version") as? String {
                
                var appVersion : Float = 1.0
                var liveAppVersion : Float = 1.0
                
                //UserApp Version
                if let tempVersion = Float(kiPhoneAppVersion) {
                    appVersion = tempVersion
                }
                
                //LiveApp Version
                if let tempVersion = Float(version) {
                    liveAppVersion = tempVersion
                }
                
                //Compare Version
                if appVersion < liveAppVersion {
                    
                    if let message = data.object(forKey: "iphone_msg") as? String {
                        
                        isSomethingWrong = true
                        
                        let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                            exit(0)
                        }))
                        refreshAlert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action: UIAlertAction!) in
                            self.rateApp(appId: kiTunesAppID) { success in
                                print("RateApp \(success)")
                            }
                            
                        }))
                        DispatchQueue.main.async {
                            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                                
                            })
                            
                        }
                    }
                }
            }
        }
        
        return isSomethingWrong
    }
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    func GetHomeProduct(){
        
        let urlStr = String(format: "%@?view=dashboard_bottom&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
       
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)                
                //New Product
                if let result = data as? NSDictionary {
                    
                    //---------------------------------//
                    
                    if let resultList = result.object(forKey: "bottom_banner") as? NSArray {
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "BottomHomeCell") as? BottomHomeCell {
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                    
                    if let resultList = result.object(forKey: "new_product") as? NSArray {
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                            cell.delegate = self
                            cell.productTitleLabel.text = "NEW ARRIVALS"
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }
                
                //New Product
                if let result = data as? NSDictionary {
                    if let resultList = result.object(forKey: "best_product") as? NSArray {
                        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                            cell.delegate = self
                            cell.productTitleLabel.text = "Best Product"
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
//                            var index = 0
//                            if self.NoOfResultCell.count > 1 {
//                                index = self.NoOfResultCell.count - 1
//                            }
//                            self.NoOfResultCell.insert(cell, at: index)
                            
                            DispatchQueue.main.async
                                {
                                    self.tableview.delegate = self
                                    self.tableview.dataSource = self
                                    self.tableview.ReloadWithAnimation()
                            }
                        }
                        
                        
                    }
                }
                
                 if let result = data as? NSDictionary {
                    if let isAdd = result.object(forKey: "ad") as? String {
                        if isAdd.caseInsensitiveCompare("yes") == .orderedSame {
                            if let resultList = result.object(forKey: "ad_list") as? NSArray {
                                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "AdBottomHomeCell") as? AdBottomHomeCell {
                                    cell.ResponseResult = resultList
                                    if let headingTitle = result.object(forKey: "ad_title") as? String {
                                        cell.headeingLabel?.text = headingTitle
                                    }
                                    self.NoOfResultCell.add(cell)
//
                                    DispatchQueue.main.async
                                        {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                    }
                                }
                                
                                
                            }
                        }
                    }
                }
                
            }
        }
        
    }

    //MARK:- Cell Category Delegate -
    func didLoadCategoryResult(data: NSDictionary) {
        print(data)
        
        if let subcat = data.object(forKey: "subcat") as? String {
            if subcat.caseInsensitiveCompare("yes") == .orderedSame {
                let viewController = CategoriesViewController(nibName: "CategoriesViewController", bundle: nil)
                viewController.homeCategoryResult = data
                self.navigationController?.pushViewController(viewController, animated: true)
                return
            }
        }
        if data.object(forKey: "catID") != nil {
            let objRoot = ProductViewController(nibName: "ProductViewController", bundle: nil)
            objRoot.categoryResultData = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }else if data.object(forKey: "productID") != nil {
            let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func GetWishListResult(){
        
        let products = NSMutableDictionary()
        let productList = NSMutableArray()
        
        let QueryWishList = String(format: "SELECT * FROM %@", kWishListTableName)
        let DBResult = MyDbManager.sharedClass().exicuteSQLiteQuery(QueryWishList)
        if let result = DBResult {
            for data in result {
                if let tempData = data as? NSDictionary {
                    if let tempStr = tempData.object(forKey: kWishListProductID) as? String {
                        productList.add(["p_id":tempStr])
                    }
                }
            }
        }
        if productList.count <= 0 {
            return
        }
        
        products.setValue(productList, forKey: "productID")
        let requestStr = GetJson(products)
        
        let urlStr = String(format: "%@?view=wish_list&page=wish_data_id&pagecode=0&userID=%@&products=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,requestStr])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                    }
                    
                }
                
            }
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
