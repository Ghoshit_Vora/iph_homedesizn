//
//  ContactUsViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/6/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    @IBOutlet var addressLabel : UILabel!
    
    @IBOutlet var websiteButton : UIButton!
    @IBOutlet var phoneButton : UIButton!
    @IBOutlet var emailButton : UIButton!
    
    var responseResult = NSDictionary()
    @IBAction func phoneClick(){
        if let tempStr = self.responseResult.object(forKey: "phone") as? String {
            let result = String(tempStr.characters.filter { "01234567890".characters.contains($0) })
            print(result)
            if let url = URL(string: "tel://\(result)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            //self.openUrl(urlStr: "tel://\(result)")
        }
        
    }
    @IBAction func websiteClick(){
        if let tempStr = self.responseResult.object(forKey: "website") as? String {
            // alternative: not case sensitive
            if tempStr.lowercased().range(of:"http") != nil {
                self.openUrl(urlStr: tempStr)
            }else{
                let newUrl = String(format: "http://%@", tempStr)
                self.openUrl(urlStr: newUrl)
            }
            
        }
    }
    @IBAction func emailClick(){
        if let tempStr = self.responseResult.object(forKey: "email") as? String {
            self.openUrl(urlStr: "mailto:\(tempStr)")
        }
        
    }
    func openUrl(urlStr : String){
        if urlStr.isEmpty == false {
            if let url = URL(string: urlStr) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.perform(#selector(ContactUsViewController.GetContactUs))
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Contact Us"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func GetContactUs(){
        
        let urlStr = String(format: "%@?view=contact&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            
                            if let resultList = result.object(forKey: "contact") as? NSArray {
                                print(resultList)
                                if resultList.count > 0 {
                                    if let data = resultList.object(at: 0) as? NSDictionary {
                                        self.responseResult = NSDictionary(dictionary: data)
                                        
                                        DispatchQueue.main.async
                                            {
                                                self.SetContactUs()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    func SetContactUs(){
        var address = ""
        if let tempStr = self.responseResult.object(forKey: "address_line1") as? String {
            address = tempStr
        }
        if let tempStr = self.responseResult.object(forKey: "address_line2") as? String {
            address = address.appendingFormat("\n%@", tempStr)
        }
        if let tempStr = self.responseResult.object(forKey: "address_line3") as? String {
            address = address.appendingFormat("\n%@", tempStr)
        }
        self.addressLabel.text = address
        
//        if let tempStr = self.responseResult.object(forKey: "time") as? String {
//            self.timeLabel.text = tempStr
//        }
        
        if let tempStr = self.responseResult.object(forKey: "website") as? String {
            self.websiteButton.UnderLineText(text: tempStr)
        }
        if let tempStr = self.responseResult.object(forKey: "call") as? String {
            self.phoneButton.UnderLineText(text: tempStr)
        }
        if let tempStr = self.responseResult.object(forKey: "email") as? String {
            self.emailButton.UnderLineText(text: tempStr)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
