//
//  LoginViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, BSKeyboardControlsDelegate {

    @IBOutlet var txtPassword: ACFloatingTextfield!
    @IBOutlet var txtUserName: ACFloatingTextfield!
    var keyboard: BSKeyboardControls!
    @IBOutlet var btnSignUp : UIButton!
    @IBOutlet var btnShowPwd : UIButton!
    
    @IBAction func SkipClick(){
        appDelegate.SabzilanaLoginUserId = ""
        
        DispatchQueue.main.async {
            let viewController = ProductHomeViewController(nibName: "ProductHomeViewController", bundle: nil)
            appDelegate.navigationController = UINavigationController(rootViewController: viewController)
            appDelegate.styleNavigationController(appDelegate.navigationController)
            appDelegate.window?.rootViewController = appDelegate.navigationController
        }
    }
    @IBAction func showPass(_ sender : UIButton){
        if sender.tag == 1 {
            self.btnShowPwd.tag = 0
            self.btnShowPwd.setTitle("Show", for: .normal)
            self.txtPassword.isSecureTextEntry = true
        }else{
            self.btnShowPwd.tag = 1
            self.btnShowPwd.setTitle("Hide", for: .normal)
            self.txtPassword.isSecureTextEntry = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        EmptySabzilanaBasket()
      self.btnSignUp.UnderLineText(text: "Sign Up Now")
//        self.txtUserName.text = "9510069162"
//        self.txtPassword.text = "admin123"
        
        
        
        
        self.keyboard = BSKeyboardControls(fields: [txtUserName,txtPassword])
        self.keyboard.delegate = self
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBAction func forgotPassword(){
        let objRoot = ForgotPassViewController(nibName: "ForgotPassViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
        
    }
    
    @IBAction func registerUser(){
        let objRoot = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    @IBAction func loginClick(){
        self.LoginResult()
    }
    
    
    func LoginResult(){
        
        if txtUserName.text!.isEmpty {
            ShowMessage("", message: "Please enter your email or phone")
            return
        }
        
        else if txtPassword.text!.isEmpty {
            ShowMessage("", message: "Please enter your password")
            return
        }
        
        self.view.endEditing(true)
        
        let urlStr = String(format: "%@?view=login&user=%@&pass=%@", arguments: [kMainDomainUrl,self.txtUserName.text!, self.txtPassword.text!])
        print(urlStr)
        //        let data = ["MobileNo" : loginId, "Password" : loginPassword, "Application" : kAppNameInAPI, "WebLogin" : kFalseAPI, "GcmID" : appDelegate.GCMToken, "DeviceID" : appDelegate.iOSDeviceToken]
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            print(result)
                            if let loginList = result.object(forKey: "user_detail") as? NSArray {
                                if loginList.count > 0 {
                                    if let loginUserData = loginList.object(at: 0) as? NSDictionary {
                                        //StoreLoginData
                                        if let typeStr = loginUserData.object(forKey: "type") as? String {
                                            if typeStr.caseInsensitiveCompare("OTP_SCREEN") == .orderedSame {
                                                let objRoot = OTPVerifyViewController(nibName: "OTPVerifyViewController", bundle: nil)
                                                objRoot.registerResult = NSDictionary(dictionary: loginUserData)
                                                self.navigationController?.pushViewController(objRoot, animated: true)
                                            }else{
                                                setLoginUser(loginUserData)
                                                self.loadLoginResult(data: loginUserData)
                                            }
                                        }else{
                                            setLoginUser(loginUserData)
                                            self.loadLoginResult(data: loginUserData)
                                        }
                                        
                                    }
                                }
                            }
                            
                            
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    func ForgotPassword(text : String){
        
        if text.isEmpty {
            ShowMessage("", message: "Please enter your email or phone")
            return
        }
        
        let urlStr = String(format: "%@?view=login&page=forgot_password&user=%@", arguments: [kMainDomainUrl,text])
        print(urlStr)
        //        let data = ["MobileNo" : loginId, "Password" : loginPassword, "Application" : kAppNameInAPI, "WebLogin" : kFalseAPI, "GcmID" : appDelegate.GCMToken, "DeviceID" : appDelegate.iOSDeviceToken]
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                            
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    func loadLoginResult(data : NSDictionary){
        if let tempStr = data.object(forKey: "userID") as? String {
            appDelegate.SabzilanaLoginUserId = tempStr
            
            self.GetUserInfo()
        }
        
        
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }

    func GetUserInfo(){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=getinfo&userID=%@&phone=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo])
        
        print(urlStr)
        
        request(urlStr, headers: nil).responseJSON { response in
            
            if let result = response.result.value as? NSDictionary {
                print(result)
                if let statusCode = result.object(forKey: kStatusCode) as? String {
                    if statusCode == "0" {
                        if let userDetailsList = result.object(forKey: "getdetail") as? NSArray {
                            if userDetailsList.count > 0 {
                                if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                                    setLoginUser(userResult)
                                }
                            }
                        }
                        
                    }
                }
                
            }
            DispatchQueue.main.async {
                let viewController = ProductHomeViewController(nibName: "ProductHomeViewController", bundle: nil)
                appDelegate.navigationController = UINavigationController(rootViewController: viewController)
                appDelegate.styleNavigationController(appDelegate.navigationController)
                appDelegate.window?.rootViewController = appDelegate.navigationController
            }
        }
        
        
        
    }
    
    //MARK:- Login With Facebook -
    @IBAction func loginWithFacebook(){
//        https://developers.facebook.com/docs/graph-api/reference/user
        
        /*
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager .logIn(withReadPermissions: ["public_profile", "email", "user_friends"], handler: { (result, error) -> Void in
            if (error == nil){
                if let fbloginresult : FBSDKLoginManagerLoginResult = result {
                    print(fbloginresult.token)
                    print(fbloginresult.token.tokenString)
                }
                
                
                FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name, last_name, picture.type(large),birthday,email,gender,hometown"]).start { (connection, result, error) -> Void in
                    print(result ?? "No Result Found")
                    
                    fbLoginManager.logOut()
                }
            }
        })
         */

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
