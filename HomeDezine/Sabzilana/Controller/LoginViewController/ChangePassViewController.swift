//
//  ForgotPassViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 5/15/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ChangePassViewController: UIViewController,BSKeyboardControlsDelegate {

    @IBOutlet var txtCurrent: ACFloatingTextfield!
    @IBOutlet var txtNew: ACFloatingTextfield!
    @IBOutlet var txtConfirm: ACFloatingTextfield!
    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    
    var keyboard: BSKeyboardControls!
    
    @IBAction func submitClick(_ sender : UIButton){
        self.ChangePassword()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        EmptySabzilanaBasket()
        
        DispatchQueue.main.async {
            self.tableview.tableHeaderView = self.headerView
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePassViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePassViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [txtCurrent,txtNew,txtConfirm])
        
        keyboard.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Forgot Password"
        self.navigationItem.title = "Forgot Password"
    }

    func ChangePassword(){
        
        if (self.txtCurrent.text?.isEmpty)! {
            ShowMessage("", message: "Please enter current password")
            return
        }
        
        if (self.txtNew.text?.isEmpty)! {
            ShowMessage("", message: "Please enter new password")
            return
        }
        
        if self.txtNew.text != self.txtConfirm.text {
            ShowMessage("", message: "Confirm password not match.")
            return
        }
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
 
        let urlStr = String(format: "%@?view=change_password&page=change_password&userID=%@&userPhone=%@&current_pass=%@&new_pass=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,self.txtCurrent.text!,self.txtNew.text!])
        print(urlStr)
        //        let data = ["MobileNo" : loginId, "Password" : loginPassword, "Application" : kAppNameInAPI, "WebLogin" : kFalseAPI, "GcmID" : appDelegate.GCMToken, "DeviceID" : appDelegate.iOSDeviceToken]
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            if let message = result.object(forKey: kMessage) as? String {
                                self.ShowAlertWithAction(message: message)
                            }
                            
                            
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                self.ShowAlertWithAction(message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    func ShowAlertWithAction(message : String){
        let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            DispatchQueue.main.async {
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }))
        
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }

    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
