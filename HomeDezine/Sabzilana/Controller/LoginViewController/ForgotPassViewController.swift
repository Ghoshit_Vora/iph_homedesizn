//
//  ForgotPassViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 5/15/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ForgotPassViewController: UIViewController,BSKeyboardControlsDelegate {

    @IBOutlet var txtUserName: ACFloatingTextfield!
    var keyboard: BSKeyboardControls!
    
    @IBAction func submitClick(_ sender : UIButton){
        if txtUserName.text?.isEmpty == true {
            ShowMessage("", message: "Please enter your email or phone")
        }else{
            self.ForgotPassword(text: txtUserName.text!)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        EmptySabzilanaBasket()
        
        //        self.txtUserName.text = "9510069162"
        //        self.txtPassword.text = "admin123"
        
        
        
        
        self.keyboard = BSKeyboardControls(fields: [txtUserName])
        
        self.keyboard.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.topItem?.title = "Forgot Password"
        self.navigationItem.title = "Forgot Password"
    }

    func ForgotPassword(text : String){
        
        if text.isEmpty {
            ShowMessage("", message: "Please enter your email or phone")
            return
        }
        
        let urlStr = String(format: "%@?view=forgot_password&name=%@", arguments: [kMainDomainUrl,text])
        print(urlStr)
        //        let data = ["MobileNo" : loginId, "Password" : loginPassword, "Application" : kAppNameInAPI, "WebLogin" : kFalseAPI, "GcmID" : appDelegate.GCMToken, "DeviceID" : appDelegate.iOSDeviceToken]
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            if let message = result.object(forKey: kMessage) as? String {
                                self.ShowAlertWithAction(message: message)
                            }
                            
                            
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                self.ShowAlertWithAction(message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    func ShowAlertWithAction(message : String){
        let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            DispatchQueue.main.async {
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        }))
        
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }

    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
