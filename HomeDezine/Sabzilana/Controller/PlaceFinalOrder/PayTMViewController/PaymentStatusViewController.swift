//
//  PaymentStatusViewController.swift
//  Sabzilana
//
//  Created by Apple on 30/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class PaymentStatusViewController: UIViewController {

    @IBOutlet var actionButtonTitle : UIButton!
    
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var messageLabel : UILabel!
    @IBOutlet var statusImage : UIImageView!
    var paymentStatusImage : String = ""
    
    var paymentStatusMessage : String = ""
    var paymentTitle : String = ""
    
    var statusTypeGateway : String = ""
    
    @IBAction func MyOrderListClick(){
        // post a notification
        if self.statusTypeGateway.caseInsensitiveCompare("wallet") == .orderedSame {
            
            var isRedirect : Bool = false
            let viewContollers = self.navigationController?.viewControllers
            
            if let conrollerList = viewContollers {
                for controler in conrollerList {
                    if let tempVC = controler as? AddMoneyViewController {
                        isRedirect = true
                        tempVC.UpdateWalletBalance()
                        _ = navigationController?.popToViewController(tempVC, animated: true)
                        break
                    }
                }
            }
            
            if isRedirect == false {
                _ = navigationController?.popViewController(animated: true)
            }
            
//            let objRoot = AddMoneyViewController(nibName: "AddMoneyViewController", bundle: nil)
//            
//            _ = navigationController?.popToViewController(objRoot, animated: true)
        }else{
            _ = navigationController?.popToRootViewController(animated: false)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kOrderPaymentStatusNotification), object: nil, userInfo: nil)
    
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationItem.hidesBackButton = true
    }
    func UpdateUI(){
        if self.paymentStatusImage.isEmpty == false {
            self.statusImage.image = UIImage(named: self.paymentStatusImage)
        }
        
        self.titleLabel.text = paymentTitle
        self.messageLabel.text = self.paymentStatusMessage
        
        if self.statusTypeGateway.caseInsensitiveCompare("wallet") == .orderedSame {
            self.actionButtonTitle.setTitle("Return To Add Money", for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.UpdateUI()
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
