//
//  PayTMViewController.swift
//  Sabzilana
//
//  Created by Apple on 29/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class PayTMViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webview : UIWebView!
    var paytmRequestUrl : String = ""
    var statusTypeGateway : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        DispatchQueue.main.async {
            self.RequestDidLoad()
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Paytm"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backClick))
    }
    
    func backClick(){
        self.ShowWarningMessage("Cancel Transaction", message: "Pressing back would cancel your transaction. Proceed to cancel?")
//        _ = navigationController?.popToRootViewController(animated: true)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kOrderPaymentStatusNotification), object: nil, userInfo: nil)
        
    }
    func RequestDidLoad(){
        if paytmRequestUrl.isEmpty == false {
            let requestUrl = URL(string: paytmRequestUrl)
            self.webview.delegate = self
            if requestUrl != nil {
                print(requestUrl ?? "Request Url Not Found")
                let myURLRequest:NSURLRequest = NSURLRequest(url: requestUrl!)
                self.webview.loadRequest(myURLRequest as URLRequest)
            }
            
        }
        
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        print(request.url?.absoluteString ?? "shouldStartLoadWith Not Found")
//        print(request.url?.query)
//        print(request.url?.scheme)
//        print(request.url?.lastPathComponent)
        let urlStr = String(format: "%@", (request.url?.absoluteString)!)
//
//
        var paymentStatusType = ""
        if self.statusTypeGateway.isEmpty == true {
            paymentStatusType = "success"
        }else{
            paymentStatusType = "captured"
        }
        if let statusMessage = getQueryStringParameter(url: urlStr, param: "payment_status") {
            print(statusMessage)
            if statusMessage.caseInsensitiveCompare(paymentStatusType) == .orderedSame {
                print("Sucess Handler")
                DispatchQueue.main.async {
                    
                    //MARK:- Empty Basket -
                    if self.statusTypeGateway.isEmpty == true {
                        EmptySabzilanaBasket()
                    }
                    
                    
                    let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                    if let statusMessage = self.getQueryStringParameter(url: urlStr, param: "msg1") {
                        print(statusMessage)
                        objRoot.paymentTitle = statusMessage
                        
                    }
                    if let statusMessage = self.getQueryStringParameter(url: urlStr, param: "msg2") {
                        print(statusMessage)
                        objRoot.paymentStatusMessage = statusMessage
                    }
                    objRoot.statusTypeGateway = self.statusTypeGateway
                    
                    objRoot.paymentStatusImage = "payment_sucess"
                    
                    self.navigationController?.pushViewController(objRoot, animated: true)
                   
                }

            }else{
                print("Field Handler")
                DispatchQueue.main.async {
                    
                    //MARK:- Empty Basket -
                    if self.statusTypeGateway.isEmpty == true {
                        EmptySabzilanaBasket()
                    }
                    
                    let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                    if let statusMessage = self.getQueryStringParameter(url: urlStr, param: "msg1") {
                        print(statusMessage)
                        objRoot.paymentTitle = statusMessage
                        
                    }
                    if let statusMessage = self.getQueryStringParameter(url: urlStr, param: "msg2") {
                        print(statusMessage)
                       objRoot.paymentStatusMessage = statusMessage
                    }
                    objRoot.statusTypeGateway = self.statusTypeGateway
                    
                    objRoot.paymentStatusImage = "payment_field"
                    
                    self.navigationController?.pushViewController(objRoot, animated: true)
                    
                    
                }
            }
        }
        
//        failed
        
        
//        https://www.sabzilana.com/sabzi/index.php?view=orderpaytm_result
//        2017-04-29 08:48:26.469 Sabzilana[3387:47454] void SendDelegateMessage(NSInvocation *): delegate (webView:decidePolicyForNavigationAction:request:frame:decisionListener:) failed to return after waiting 10 seconds. main run loop mode: kCFRunLoopDefaultMode
//        https://www.sabzilana.com/index.php?payment_status=failed&msg1=online%20payment%20failed&msg2=sl25%20ordedr%20placed%20COD
        return true
    }
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        
    }
    
    func ShowWarningMessage(_ title : String, message : String){
        var titleStr = title
        if titleStr.isEmpty == true {
            titleStr = kAppName
        }
        
        let refreshAlert = UIAlertController(title: titleStr, message: message, preferredStyle: UIAlertControllerStyle.alert)
       
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            DispatchQueue.main.async {
                
                let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                objRoot.paymentTitle = "Ooh no!"
                objRoot.paymentStatusImage = "payment_field"
                objRoot.paymentStatusMessage = "Unfortunately we have an issue with your payment try again."
                self.navigationController?.pushViewController(objRoot, animated: true)
                
            }
        }))
        
        
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
