//
//  ReviewCell.swift
//  Sabzilana
//
//  Created by Desap Team on 14/02/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet var usernameLabel : UILabel?
    @IBOutlet var rateLabel : UILabel?
    @IBOutlet var dateLabel : UILabel?
    @IBOutlet var messageLabel : UILabel?
    @IBOutlet var rateview : UIView?
    
    var resultJson = NSDictionary() {
        didSet{
            self.UpdateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func UpdateUI(){
        if self.usernameLabel != nil {
            
            if let tempStr = resultJson.object(forKey: "name") as? String {
                usernameLabel?.text = tempStr
            }
            if let tempStr = resultJson.object(forKey: "date") as? String {
                dateLabel?.text = tempStr
            }
            if let tempStr = resultJson.object(forKey: "message") as? String {
                messageLabel?.text = tempStr
            }
            if let tempStr = resultJson.object(forKey: "rate") as? String {
                rateLabel?.text = "\(tempStr)/5"
                
                let rate = EZRatingView(frame: (self.rateview?.bounds)!)
                rate.isUserInteractionEnabled = false
                //            rate.stepInterval = 0.5
                if let start = Float(tempStr) {
                    rate.value = CGFloat(start)
                }
                
                self.rateview?.addSubview(rate)

            }
        }else{
            self.perform(#selector(self.UpdateUI), with: nil, afterDelay: 0.2)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
}
