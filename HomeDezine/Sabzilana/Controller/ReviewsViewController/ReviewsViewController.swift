//
//  ReviewsViewController.swift
//  Sabzilana
//
//  Created by Desap Team on 14/02/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var productDetailsResult = NSDictionary() {
        didSet{
            GetEnquiryList()
        }
    }
    
    var reviewList = NSArray()
    @IBOutlet var tableview : UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Reviews"
        
        let nibName = UINib(nibName: "ReviewCell", bundle: nil)
        self.tableview?.register(nibName, forCellReuseIdentifier: "ReviewCell")
        self.tableview?.estimatedRowHeight = 109
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
         self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.reviewList.count > indexPath.row {
            if let cell = self.tableview?.dequeueReusableCell(withIdentifier: "ReviewCell") as? ReviewCell {
                if let result = self.reviewList.object(at: indexPath.row) as? NSDictionary {
                    cell.resultJson = result
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetEnquiryList(){
        
        var productId = ""
        if let product = self.productDetailsResult.object(forKey: "productID") as? String {
            productId = product
        }
        
        let urlStr = String(format: "%@?view=review_list&userID=%@&productID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,productId])
        print(urlStr)
       
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                
                //New Product
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let inquiry = result.object(forKey: "review_list") as? NSArray {
                                self.reviewList = NSArray(array: inquiry)
                                DispatchQueue.main.async {
                                    self.tableview?.delegate = self
                                    self.tableview?.dataSource = self
                                    self.tableview?.reloadData()
                                }
                                
                            }
                            //
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
                
            }
            
            
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
