//
//  SabzilanaWebViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 5/15/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class SabzilanaWebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webview : UIWebView!
    var pageTitle : String = "Terms & Conditions"
    var pageUrl : String = "http://www.homedezin.com/terms-amp-conditions.html"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.RequestDidLoad()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func RequestDidLoad(){
        if pageUrl.isEmpty == false {
            let requestUrl = URL(string: pageUrl)
            self.webview.delegate = self
            if requestUrl != nil {
                print(requestUrl ?? "Request Url Not Found")
                let myURLRequest:NSURLRequest = NSURLRequest(url: requestUrl!)
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                self.webview.loadRequest(myURLRequest as URLRequest)
            }
            
        }
        
        if pageTitle.isEmpty == false {
            self.navigationItem.title = pageTitle
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        
    }

    func webViewDidStartLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
