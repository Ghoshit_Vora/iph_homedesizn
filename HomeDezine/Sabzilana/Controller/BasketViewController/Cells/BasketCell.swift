//
//  ProductListCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol BasketCellDelegate {
    func BasketDeleteItemClick(data : NSDictionary)
    func UpdateBasketPriceNItems()
}
class BasketCell: UITableViewCell {

    var delegate : BasketCellDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var captionLabel : UILabel!
    
    @IBOutlet var priceLabel : UILabel!
    
    @IBOutlet var weightLabel : UILabel!
    @IBOutlet var qtyLabel : UILabel!
    
    var Qty_Price_ID : String = ""
    var selectedQty : Int = 0
    var selectedQtyPrice : Float = 0
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

   
    @IBAction func DeleteItemClick(){
        if self.delegate != nil {
            self.delegate?.BasketDeleteItemClick(data: self.resultData)
        }
        
        //Update Basket Total Price
        if self.delegate != nil {
            self.delegate?.UpdateBasketPriceNItems()
        }
    }
    
    @IBAction func plusQtyClick(){
        self.selectedQty+=1
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
        self.AddProductInBasket()
        
        //Update Basket Total Price
        if self.delegate != nil {
            self.delegate?.UpdateBasketPriceNItems()
        }
    }
    @IBAction func minusQtyClick(){
        self.selectedQty-=1
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
        self.AddProductInBasket()
        
        //Update Basket Total Price
        if self.delegate != nil {
            self.delegate?.UpdateBasketPriceNItems()
        }
    }
    func UpdateNCheckQty(){
        if self.selectedQty <= 0 {
            self.selectedQty = 0
        }
        self.qtyLabel.text = String(format: "%d", self.selectedQty)
        
        let totalPrice : Float = Float(self.selectedQty) * selectedQtyPrice
        
        let priceStr = String(format: "₹ %.2f", arguments: [totalPrice])
        self.priceLabel.text = priceStr
    }
    
    func SetResultUI(){
        
        if self.imageview != nil && self.resultData.object(forKey: "productID") != nil {
            
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "caption") as? String {
                self.captionLabel.text = tempStr
            }
            //Get Prices Data
            if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                var ProductItemId = ""
                if let tempStr = self.resultData.object(forKey: kProductItemID) as? String {
                    ProductItemId = tempStr
                }
                for tempData in priceList {
                    if let itemData = tempData as? NSDictionary {
                        if let itemId = itemData.object(forKey: "price_ID") as? String {
                            if ProductItemId == itemId {
                                self.UpdateGMOrPcsQty(priceData: itemData)
                                break
                            }
                        }
                    }
                }
                
            }
            //Update and Check Qty
            self.UpdateNCheckQty()
            
            
        }else{
            self.perform(#selector(BasketCell.SetResultUI))
        }
        
    }
    
    //Update GM or PCS
    func UpdateGMOrPcsQty(priceData : NSDictionary){
        if let tempStr = priceData.object(forKey: "price_ID") as? String {
            self.Qty_Price_ID = tempStr
        }
       
        if let tempStr = priceData.object(forKey: "weight") as? String {
            
            // alternative: not case sensitive
            if tempStr.lowercased().range(of:"pcs") != nil {
                self.weightLabel.text = tempStr
                
            }else{
                self.weightLabel.text = String(format: "Weight: %@", tempStr)
                
            }
        }
        
        if let tempStr = priceData.object(forKey: "price") as? String {
            
            if let price = Float(tempStr) {
                selectedQtyPrice = price
            }
            
        }
        
        let tempQty = self.QtyProductInBasket()
        
        if let intQty = Int(tempQty) {
            self.selectedQty = intQty
        }else {
            self.selectedQty = 0
        }
       
        
        self.UpdateNCheckQty()
        
    }
    
    //MARK:- Database Query
    func AddProductInBasket(){
        
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        
        //Mutable Basket Object and Update Qty
        let liveDataObject = NSMutableDictionary(dictionary: self.resultData)
        let liveQty = String(format: "%d", self.selectedQty)
        print(liveQty)
        liveDataObject.setValue(liveQty, forKey: kBasketQtyKey)
        liveDataObject.setValue(livePriceId, forKey: kProductItemID)
        
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            if self.selectedQty > 0 {
                                liveMutableBasket.add(liveDataObject)
                            }
                        }else{
                            if self.selectedQty > 0 {
                                if liveMutableBasket.count > index {
                                    liveMutableBasket.replaceObject(at: index, with: liveDataObject)
                                }
                                
                            }else{
                                if liveMutableBasket.count > index {
                                    liveMutableBasket.removeObject(at: index)
                                }
                            }
                        }
                    }else{
                        if self.selectedQty > 0 {
                            liveMutableBasket.add(liveDataObject)
                        }
                    }
                }else{
                    if self.selectedQty > 0 {
                        liveMutableBasket.add(liveDataObject)
                    }
                }
                
            }else{
                if self.selectedQty > 0 {
                    liveMutableBasket.add(liveDataObject)
                }
            }
            
            //Update Basket Result in Local DB
            SetBasketLocalResult(liveMutableBasket)
        }
        
        
        print(GetBasketLocalResult())
        
        
        
    }
    
    func QtyProductInBasket() -> String {
        
        var LiveItemQty = "0"
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            LiveItemQty = "0"
                        }else{
                            if liveMutableBasket.count > index {
                                if let product = liveMutableBasket.object(at: index) as? NSDictionary {
                                    if let tempQty = product.object(forKey: kBasketQtyKey) as? String {
                                        LiveItemQty = tempQty
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
        }
        return LiveItemQty
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
