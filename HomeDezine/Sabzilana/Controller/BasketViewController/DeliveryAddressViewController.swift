//
//  DeliveryAddressViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 08/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class DeliveryAddressViewController: UIViewController,DropDownMenuDelegate, ChangeAddressDelegate {

    var isDeliveryAvailable : Bool = false
    
    @IBOutlet var userDetailsLabel : UILabel!
    @IBOutlet var buttonDate : UIButton!
    @IBOutlet var buttonDateArrow : UIButton!
    @IBOutlet var buttonTime : UIButton!
    @IBOutlet var buttonTimeArrow : UIButton!
    @IBOutlet var deliveryChargeLabel : UILabel!
    @IBOutlet var btnBasket : UIButton!
    var ResponseResult = NSDictionary()
    var TimeSlotResult = NSDictionary()
    
    var datePicker : UIDatePicker = UIDatePicker()
    var SelectedDay : String = ""
    
    var DeliveryTimeSlot : String = ""
    var DeliveryDateSlot : String = ""
    
    
    @IBAction func proceedToPay(){
        
        if self.isDeliveryAvailable == true {
            var address = ""
            var area = ""
            var pincode = ""
            
            if let userDetailsList = self.ResponseResult.object(forKey: "get_detail") as? NSArray {
                if userDetailsList.count > 0 {
                    if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                        
                        if let tempStr = userResult.object(forKey: "address") as? String {
                            address = tempStr
                        }
                        if let tempStr = userResult.object(forKey: "area") as? String {
                            area = tempStr
                        }
                        if let tempStr = userResult.object(forKey: "pincode") as? String {
                            pincode = tempStr
                        }
                    }
                }
            }

            if address.isEmpty == true {
                ShowMessage("", message: "Address can not be blank")
                return
            }
            if area.isEmpty == true {
                ShowMessage("", message: "Area can not be blank")
                return
            }
            if area.isEmpty == true {
                ShowMessage("", message: "Pincode can not be blank")
                return
            }
            
            let objRoot = PaymentOptionViewController(nibName: "PaymentOptionViewController", bundle: nil)
            objRoot.DeliveryTimeSlot = DeliveryTimeSlot
            objRoot.DeliveryDateSlot = DeliveryDateSlot
            
            objRoot.ResponseResultDelivery = NSDictionary(dictionary: ResponseResult)
            self.navigationController?.pushViewController(objRoot, animated: true)
        }else{
            ShowMessage("", message: "Delivery not available at this time, please choose different time.")
        }
        
    }
    
    @IBAction func changeAddressClick(){
        let objRoot = ChangeAddressViewController(nibName: "ChangeAddressViewController", bundle: nil)
        objRoot.delegate = self
        objRoot.ResponseResultDelivery = NSDictionary(dictionary: ResponseResult)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    @IBAction func changeTimeClick(){
        self.SelectTimesSlot()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.SelectedDay = GetSelectedDay(date: Date())
        
        self.GetDeliveryInfo(DayStr: self.SelectedDay)
        
        self.GetSelectedDate(date: Date())
        
        self.UpdateBasketPriceNItems()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Delivery Address"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - DATE PICKER
    @IBAction func DateClick(_ sender : AnyObject){
        self.CreateDatePickerViewWithAlertController()
    }
    func CreateDatePickerViewWithAlertController()
    {
        let label = UILabel(frame: CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 30))
        label.textAlignment = .center
        label.text = "Set delivery date:"
        label.font = UIFont.systemFont(ofSize: 20)
        
        let viewDatePicker: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250))
        viewDatePicker.backgroundColor = UIColor.clear
        
        
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 200))
        self.datePicker.minimumDate = Date()
        
        //NextDay Condition
        //Nextday Condtion
        //Incremnt days
        var nextDay = ""
        if let tempStr = self.ResponseResult.object(forKey: "next_days") as? String {
            nextDay = tempStr
        }else if let tempStr = self.ResponseResult.object(forKey: "next_days") as? NSNumber {
            nextDay = tempStr.stringValue
        }
        var updateday : Int = 1
        
        if nextDay.isEmpty == false {
            if let tempDay = Int(nextDay) {
                updateday = tempDay
            }
        }
        
        let currentDate = Date()
        
        var dateComponent = DateComponents()
        dateComponent.day = updateday
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        print(currentDate)
        print(futureDate!)
        
        self.datePicker.maximumDate = futureDate
        //Finish NextDay
        
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.locale = Locale(identifier: "en_GB")
        //        self.datePicker.addTarget(self, action: "datePickerSelected", forControlEvents: UIControlEvents.ValueChanged)
        self.datePicker.addTarget(self, action: #selector(DeliveryAddressViewController.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
        
        viewDatePicker.addSubview(label)
        
        viewDatePicker.addSubview(self.datePicker)
        
        let alertController = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alertController.view.addSubview(viewDatePicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        { (action) in
            // ...
        }
        
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Done", style: .default)
        { (action) in
            
            self.setDate()
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true)
        {
            // ...
        }
        
        
        
        
        
    }
    func setDate(){
        self.GetSelectedDate(date: self.datePicker.date)
        self.SelectedDay = self.GetSelectedDay(date: self.datePicker.date)
        self.GetNewDayDeliveryInfo(DayStr: self.SelectedDay)
        
    }
    func handleDatePicker(_ sender: UIDatePicker) {
        self.GetSelectedDate(date: self.datePicker.date)
       
    }

    
    func GetDeliveryInfo(DayStr : String){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=get_info&userID=%@&userPhone=%@&day=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,DayStr.lowercased()])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.ResponseResult = NSDictionary(dictionary: result)
                            DispatchQueue.main.async {
                                self.UpdateUI()
                            }
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    func GetNewDayDeliveryInfo(DayStr : String){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=get_info&userID=%@&userPhone=%@&day=%@&type=newday", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,DayStr.lowercased()])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.TimeSlotResult = NSDictionary(dictionary: result)
                            DispatchQueue.main.async {
                                self.UpdateDeliveryTimeSlot(data: result)
                            }
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }

    func UpdateUI(){
        
        if let userDetailsList = self.ResponseResult.object(forKey: "get_detail") as? NSArray {
            if userDetailsList.count > 0 {
                if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                    var userDetailsStr = ""
                    if let tempStr = userResult.object(forKey: "name") as? String {
                        userDetailsStr = userDetailsStr.appending(tempStr)
                    }
                    
                    if let tempStr = userResult.object(forKey: "address") as? String {
                        userDetailsStr = userDetailsStr.appending(",\n")
                        userDetailsStr = userDetailsStr.appending(tempStr)
                    }
                    if let tempStr = userResult.object(forKey: "area") as? String {
                        userDetailsStr = userDetailsStr.appending(",\n")
                        userDetailsStr = userDetailsStr.appending(tempStr)
                    }
                    if let tempStr = userResult.object(forKey: "pincode") as? String {
                        userDetailsStr = userDetailsStr.appending(" - ")
                        userDetailsStr = userDetailsStr.appending(tempStr)
                    }
                    
                    if let tempStr = userResult.object(forKey: "phone") as? String {
                        userDetailsStr = userDetailsStr.appending("\n\n")
                        userDetailsStr = userDetailsStr.appending(tempStr)
                    }
                    
                    self.userDetailsLabel.text = userDetailsStr
                    
                    var areaStr = ""
                    if let tempStr = userResult.object(forKey: "area") as? String {
                        areaStr = tempStr
                    }
                    var charge = ""
                    if let tempStr = userResult.object(forKey: "ship_charge") as? String {
                       charge = tempStr
                    }
                    if let tempStr = userResult.object(forKey: "ship_charge") as? NSNumber {
                        charge = tempStr.stringValue
                    }
                    self.deliveryChargeLabel.text = String(format: "Delivery charge ₹%@ Applied for %@ area", arguments: [charge,areaStr])

//                    Delivery charge ₹29 Applied for AIRFORCE COLONY area
                }
            }
        }
        
        
        //Update Delivery Time
        self.TimeSlotResult = NSDictionary(dictionary: self.ResponseResult)
        self.UpdateDeliveryTimeSlot(data: self.TimeSlotResult)
        
    }
    func UpdateDeliveryTimeSlot(data : NSDictionary){
        
        
        //Check Delivery Avialble
        
        if let delivery = data.object(forKey: "data") as? String {
            if delivery.caseInsensitiveCompare("no") == .orderedSame {
                if let message = data.object(forKey: "time_slot_msg") as? String {
                    self.buttonTime.setTitle(message, for: .normal)
                    self.buttonTime.isEnabled = false
                    self.buttonTimeArrow.isHidden = true
                    self.isDeliveryAvailable = false
                }
            }else{
                self.buttonTime.isEnabled = true
                self.buttonTimeArrow.isHidden = false
                self.isDeliveryAvailable = true
                if let timeSlotList = data.object(forKey: "timeslot") as? NSArray {
                    if timeSlotList.count > 0 {
                        if let timeData = timeSlotList.object(at: 0) as? NSDictionary {
                            if let startTime = timeData.object(forKey: "start_time") as? String {
                                self.buttonTime.setTitle(startTime, for: .normal)
                                self.DeliveryTimeSlot = startTime
                            }
                        }
                    }
                }
            }
        }
    }
    func GetSelectedDay(date : Date) -> String{
        let formatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        
        formatter.dateFormat = "EEEE"
        return formatter.string(from: date)
    }

    func GetSelectedDate(date : Date) {
        let formatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        
        formatter.dateFormat = "dd MMM yyyy"
        let dateStr = formatter.string(from: date)
        self.buttonDate.setTitle(dateStr, for: .normal)
        
        
        //OrderDate Format
        formatter.dateFormat = "dd-MM-yyyy"
        let orderDate = formatter.string(from: date)
        self.DeliveryDateSlot = orderDate
    }
    
    
    //MARK: - Time Slot -
    func SelectTimesSlot(){
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = TimeSlotController.instance()
        container.delegate = self
        
        if let timeSlotList = self.TimeSlotResult.object(forKey: "timeslot") as? NSArray {
            container.responseResult = NSArray(array: timeSlotList)
        }
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    //MARK:- DropDown Delegate -
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        if let timeData = data as? NSDictionary {
            if let startTime = timeData.object(forKey: "start_time") as? String {
                self.buttonTime.setTitle(startTime, for: .normal)
                self.DeliveryTimeSlot = startTime
            }
        }
    }
    
    //MARK:- Get Basket Price -
    func UpdateBasketPriceNItems() {
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        var basketTotalPrice : Float = 0
        
        for tempData in basketLocalResul {
            if let resultData = tempData as? NSDictionary {
                if let tempQty = resultData.object(forKey: kBasketQtyKey) as? String {
                    var LiveItemQty : Float = 0
                    if let tempQty = Float(tempQty) {
                        LiveItemQty = tempQty
                    }
                    
                    //Get Prices Data
                    if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                        var ProductItemId = ""
                        if let tempStr = resultData.object(forKey: kProductItemID) as? String {
                            ProductItemId = tempStr
                        }
                        for tempData in priceList {
                            if let itemData = tempData as? NSDictionary {
                                if let itemId = itemData.object(forKey: "price_ID") as? String {
                                    if ProductItemId == itemId {
                                        //GetPrice
                                        var LiveItemPrice : Float = 0
                                        if let itemPrice = itemData.object(forKey: "price") as? String {
                                            
                                            if let tempPrice = Float(itemPrice) {
                                                LiveItemPrice = tempPrice
                                            }
                                        }
                                        
                                        basketTotalPrice = basketTotalPrice + (LiveItemQty * LiveItemPrice)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }
        
        
        let priceStr = String(format: "₹ %.2f", arguments: [basketTotalPrice])
        self.btnBasket.setTitle(priceStr, for: .normal)
        self.btnBasket.isEnabled = false
     
        if ChipChargeMinCartAmount < basketTotalPrice {
            self.deliveryChargeLabel.isHidden = true
        }else{
            self.deliveryChargeLabel.isHidden = false
        }
    }
    
    //MARK:- Change Address Delegate -
    func didChangedAddress(data: NSDictionary) {
        
//        print(data)
        self.ResponseResult = NSDictionary(dictionary: data)
//        print(self.ResponseResult)
        DispatchQueue.main.async {
            self.UpdateUI()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
