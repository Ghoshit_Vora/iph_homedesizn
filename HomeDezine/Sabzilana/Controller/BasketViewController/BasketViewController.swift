//
//  BasketViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 07/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
var ChipChargeMinCartAmount : Float = 0
var ChipChargeCartMessage : String = ""
class BasketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,BasketCellDelegate,MLKMenuPopoverDelegate {

    @IBOutlet var lblDeliveryChargeMsg : UILabel!
    @IBOutlet var deliveryHeightConstant : NSLayoutConstraint!
    @IBOutlet var bottomViewHeightConstant : NSLayoutConstraint!
    
    @IBOutlet var emptyImage : UIImageView!
    @IBOutlet var emptyLabel : UILabel!
    
    @IBOutlet var btnItemNPrice : UIButton!
    
    @IBOutlet var tableview : UITableView!
    var orderResul = NSArray()
    var sabzilanaBasketPrice : Float = 0
    @IBOutlet weak var growingTextView: NextGrowingTextView!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    let placeHolder : String = "Special Instruction for your Order"
    
    var menuPopover = MLKMenuPopover()
    var isMenuPopoverVisible : Bool = false
    
    @IBAction func placeOrder(){
        
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to login before checkout")
        }else{
            if self.sabzilanaBasketPrice > 0 {
                if self.orderResul.count > 0 {
                    if let tempStr = self.growingTextView.text {
                        orderSpecialInstruction = tempStr
                    }
                    //        print(orderSpecialInstruction)
                    
                    let objRoot = DeliveryAddressViewController(nibName: "DeliveryAddressViewController", bundle: nil)
                    self.navigationController?.pushViewController(objRoot, animated: true)
                }
            }else{
                ShowMessage("", message: "A minimum 1 product is required before checking out.")
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.orderResul = NSArray(array: GetBasketLocalResult())
        
        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "BasketCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "BasketCell")
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.ReloadWithAnimation()
        
        //Update Basket Total Price
        self.UpdateBasketPriceNItems()
        
        //Comment Text Field
        NotificationCenter.default.addObserver(self, selector: #selector(BasketViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(BasketViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.growingTextView.layer.cornerRadius = 4
        self.growingTextView.backgroundColor = UIColor.white
        
        self.growingTextView.textContainerInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.growingTextView.placeholderAttributedText = NSAttributedString(string: placeHolder,
                                                                            attributes: [NSFontAttributeName: self.growingTextView.font!,
                                                                                         NSForegroundColorAttributeName: UIColor.gray
            ]
        )
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(BasketViewController.handleTap))
        
        self.view.addGestureRecognizer(tap)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(BasketViewController.handleTap))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        // Set 26px of fixed space between the two UIBarButtonItems
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 0.0
        
        let emptyCart = UIBarButtonItem(image: #imageLiteral(resourceName: "more_empty"), style: .plain, target: self, action: #selector(BasketViewController.EmptyCart))
        
        
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(BasketViewController.SearchClick))
        
        
//        self.navigationItem.rightBarButtonItems = [emptyCart,fixedSpace,search]
        self.navigationItem.rightBarButtonItems = [search]
        //        self.navigationItem.rightBarButtonItems = [self.barButtonBasket,fixedSpace,search]
        
        self.EmptyCartImageEnable()
        

       
    }
    
    func EmptyCartPopver(){
        
        if self.isMenuPopoverVisible == false {
//            let frame = CGRect(x: 50, y: 100, width: 140, height: 100)
            let frame = CGRect(x: UIScreen.main.bounds.size.width - (140 + 22), y: 0, width: 140, height: 40)
            self.isMenuPopoverVisible = true
            menuPopover = MLKMenuPopover(frame: frame, menuItems: ["Empty Basket"])
            menuPopover.menuPopoverDelegate = self
            menuPopover.show(in: view)
        }else{
            self.isMenuPopoverVisible = false
            menuPopover.dismiss()
        }
        
    }
    func menuPopover(_ menuPopover: MLKMenuPopover, didSelectMenuItemAt selectedIndex: Int) {
        self.isMenuPopoverVisible = false
        self.menuPopover.dismiss()
        
        //Empty Basket
        EmptySabzilanaBasket()
        
        //Update Cart Values
        self.orderResul = NSArray(array: GetBasketLocalResult())
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.ReloadWithAnimation()
        
        //Update Basket Total Price
        self.UpdateBasketPriceNItems()
        
        self.EmptyCartImageEnable()
        
        
    }
    func EmptyCart(){
        EmptyCartPopver()
        
    }
    func SearchClick(){
        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    func EmptyCartImageEnable(){
        if self.orderResul.count > 0 {
            self.tableview.isHidden = false
            self.emptyImage.isHidden = true
            self.emptyLabel.isHidden = true
        }else{
            self.tableview.isHidden = true
            self.emptyImage.isHidden = false
            self.emptyLabel.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "My Cart"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.orderResul.count > indexPath.row {
            if let data = self.orderResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "BasketCell") as? BasketCell {
                    cell.delegate = self
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Basket Cell Delegate -
    func BasketDeleteItemClick(data: NSDictionary) {
        var liveProductId = ""
        if let tempStr = data.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                let index = basketResult.index(of: liveProductId)
                
                if index == NSNotFound {
                    print("Item ID Not Found")
                }else{
                    let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
                    if liveMutableBasket.count > index {
                        liveMutableBasket.removeObject(at: index)
                        SetBasketLocalResult(liveMutableBasket)
                        self.orderResul = NSArray(array: GetBasketLocalResult())
                        //Reload Table
                        DispatchQueue.main.async {
                            self.tableview.delegate = self
                            self.tableview.dataSource = self
                            self.tableview.ReloadWithAnimation()
                        }
                    }
                }
            }
        }
        
        self.EmptyCartImageEnable()
    }
    
    func UpdateBasketPriceNItems() {
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        var basketTotalPrice : Float = 0
        
        for tempData in basketLocalResul {
            if let resultData = tempData as? NSDictionary {
                if let tempQty = resultData.object(forKey: kBasketQtyKey) as? String {
                    var LiveItemQty : Float = 0
                    if let tempQty = Float(tempQty) {
                        LiveItemQty = tempQty
                    }
                    
                    //Get Prices Data
                    if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                        var ProductItemId = ""
                        if let tempStr = resultData.object(forKey: kProductItemID) as? String {
                            ProductItemId = tempStr
                        }
                        for tempData in priceList {
                            if let itemData = tempData as? NSDictionary {
                                if let itemId = itemData.object(forKey: "price_ID") as? String {
                                    if ProductItemId == itemId {
                                        //GetPrice
                                        var LiveItemPrice : Float = 0
                                        if let itemPrice = itemData.object(forKey: "price") as? String {
                                            
                                            if let tempPrice = Float(itemPrice) {
                                                LiveItemPrice = tempPrice
                                            }
                                        }
                                        
                                        basketTotalPrice = basketTotalPrice + (LiveItemQty * LiveItemPrice)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                }

            }
        }
        
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d Items ₹ %.2f", arguments: [totalItems,basketTotalPrice])
        self.btnItemNPrice.setTitle(priceStr, for: .normal)
        self.btnItemNPrice.isEnabled = false
        
        self.DeliveryCharge(basketTotalPrice: basketTotalPrice)
        
        self.sabzilanaBasketPrice = basketTotalPrice
    }
    
    func DeliveryCharge(basketTotalPrice : Float){
        self.lblDeliveryChargeMsg.text = ChipChargeCartMessage
        if ChipChargeMinCartAmount < basketTotalPrice {
            self.lblDeliveryChargeMsg.isHidden = true
            self.deliveryHeightConstant.constant = 0
            self.bottomViewHeightConstant.constant = 70
            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }else{
            self.lblDeliveryChargeMsg.isHidden = false
            self.deliveryHeightConstant.constant = 20
            self.bottomViewHeightConstant.constant = 90
            UIView.animate(withDuration: 0.0, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
        }
        
        
    }
    
    //MARK:- Special Instrucation Text Field -
    func handleTap() {
        self.view.endEditing(true)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func handleDoneButton(_ sender: AnyObject) {
        
        self.view.endEditing(true)
    }
    
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.inputContainerViewBottom.constant =  0
                //textViewBottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
