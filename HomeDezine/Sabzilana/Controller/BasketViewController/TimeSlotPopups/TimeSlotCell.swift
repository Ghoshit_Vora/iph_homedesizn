//
//  RegisterListCell.swift
//  Sabzilana
//
//  Created by Jeevan on 25/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class TimeSlotCell: UITableViewCell {

    @IBOutlet var titleLabel : UILabel!
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    func SetResultUI(){
        
        if self.titleLabel != nil  {
            
            if let tempStr = resultData.object(forKey: "start_time") as? String {
                self.titleLabel.text = tempStr
            }
            
        }else{
            self.perform(#selector(ProductListCell.SetResultUI), with: nil, afterDelay: 0.2)
            
        }
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
