//
//  InputPromoCodeCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/26/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class InputPromoCodeCell: UITableViewCell {

    @IBOutlet var inputField : UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 30))
        self.inputField.leftView = paddingView;
        self.inputField.leftViewMode = UITextFieldViewMode.always
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
