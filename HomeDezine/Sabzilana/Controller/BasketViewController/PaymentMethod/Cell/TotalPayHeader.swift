//
//  TotalPayHeader.swift
//  Sabzilana
//
//  Created by TNM3 on 4/26/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class TotalPayHeader: UIView {

    var isExpand : Bool = false
    @IBOutlet var totalAmountLabel : UILabel!
    @IBOutlet var totalAmountTitleLabel : UILabel!
    
    class func instanceFromNib() -> TotalPayHeader {
        return UINib(nibName: "TotalPayHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! TotalPayHeader
    }
    
    override func awakeFromNib() {
//        self.layer.shadowColor = UIColor.gray.cgColor
//        self.layer.shadowOffset = CGSize(width: 1, height: 1)
//        self.layer.shadowOpacity = 1
//        self.layer.shadowRadius = 1.0
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
