//
//  TotalPayableCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/26/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class TotalPayableCell: UITableViewCell {

    @IBOutlet var OrderItems : UILabel!
    @IBOutlet var SubTotal : UILabel!
    @IBOutlet var DeliveryCharge : UILabel!
    @IBOutlet var CODCharge : UILabel!
    @IBOutlet var CouponDiscount : UILabel!
    @IBOutlet var OrderTotal : UILabel!
    @IBOutlet var WalletAmount : UILabel!
    @IBOutlet var TotalPayable : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
