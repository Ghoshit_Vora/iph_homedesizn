//
//  PaymentOptionViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/24/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
enum PaymentOption {
    case PayTM
    case SabilanaWallet
    case COD
    case PayU
}
var orderSpecialInstruction = ""

class PaymentOptionViewController: PUUIBaseVC {
  
    @IBOutlet var btnSabziWallet : UIButton!
    @IBOutlet var btnPaytmWallet : UIButton!
    @IBOutlet var btnCOD : UIButton!
    @IBOutlet var btnPayU : UIButton!
    
    @IBOutlet var sabzilanaWalletLabel : UILabel!
    @IBOutlet var CODChargeLabel : UILabel!
    
    @IBOutlet var tableview : UITableView!
    @IBOutlet var footerView : UIView!
    @IBOutlet var btnBasket : UIButton!
    
    var headerResult = NSMutableArray()
    
    var ResponseResultDelivery = NSDictionary()
    
    //TotalPayableCell
    var TotalPayableCellNib = TotalPayableCell()
    
    var NetPayableAmountGlobal : Float = 0
    var BasketOrderTotalAmountGlobal : Float = 0
    var BasketItesmTotalAmount : Float = 0
    var TotalDeliveryCharge : Float = 0
    var SabzilanaWalletBalance : Float = 0
    
    var DeliveryTimeSlot : String = ""
    var DeliveryDateSlot : String = ""
    
    var paymentOption : PaymentOption?
    
    //PayU
    var defaultActivityIndicator: iOSDefaultActivityIndicator = iOSDefaultActivityIndicator()
    var webServiceResponse: PayUWebServiceResponse = PayUWebServiceResponse()
    
    var payu_order_fail : String = "Online payment failed_Your order(SL99) placed COD"
    var payu_order_success : String = "Online payment Done_Your order(SL99) placed sucess"
    
    var ifPaymentModePayU : Bool = false
    
    //PayU
    
    @IBAction func SabzilanaClick(){
        paymentOption = PaymentOption.SabilanaWallet
        
        self.btnSabziWallet.setImage(#imageLiteral(resourceName: "CheckedButton"), for: .normal)
        self.btnPaytmWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnCOD.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPayU.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        
        //DidChangePayament Option
        DispatchQueue.main.async {
            self.didChangePaymentOption()
        }
        
    }
    @IBAction func PaytmClick(){
        paymentOption = PaymentOption.PayTM
        
        self.btnSabziWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPaytmWallet.setImage(#imageLiteral(resourceName: "CheckedButton"), for: .normal)
        self.btnCOD.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPayU.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        
        //DidChangePayament Option
        DispatchQueue.main.async {
            self.didChangePaymentOption()
        }
    }
    @IBAction func CODClick(){
        paymentOption = PaymentOption.COD
        self.btnSabziWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPaytmWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnCOD.setImage(#imageLiteral(resourceName: "CheckedButton"), for: .normal)
        self.btnPayU.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        
        //DidChangePayament Option
        DispatchQueue.main.async {
            self.didChangePaymentOption()
        }
    }
    @IBAction func PayUClick(){
        
        paymentOption = PaymentOption.PayU
        
        self.btnSabziWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPaytmWallet.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnCOD.setImage(#imageLiteral(resourceName: "CheckButton"), for: .normal)
        self.btnPayU.setImage(#imageLiteral(resourceName: "CheckedButton"), for: .normal)
        
        //DidChangePayament Option
        DispatchQueue.main.async {
            self.didChangePaymentOption()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.estimatedRowHeight = 60
        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "InputPromoCodeCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "InputPromoCodeCell")
        
        let nibName1 = UINib(nibName: "TotalPayableCell", bundle: nil)
        self.tableview .register(nibName1, forCellReuseIdentifier: "TotalPayableCell")
        
        if let cell = self.tableview.dequeueReusableCell(withIdentifier: "TotalPayableCell") as? TotalPayableCell
        {
            self.TotalPayableCellNib = cell
        }
        
        let header1 = TotalPayHeader.instanceFromNib()
        header1.totalAmountTitleLabel.text = "Total payable amount"
        header1.totalAmountTitleLabel.textColor = UIColor.black
        let header2 = TotalPayHeader.instanceFromNib()
        header2.totalAmountTitleLabel.text = "Have a coupon code?"
        header2.totalAmountTitleLabel.textColor = NSTheme().GetSabzilanaGreenColor()
        self.headerResult.add(header1)
        self.headerResult.add(header2)
        
        self.UpdateBasketPriceNItems()
        
        DispatchQueue.main.async {
            self.GetBasketTotalAmount()
        }
        
        self.tableview.tableFooterView = self.footerView
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Payment Option"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if self.ifPaymentModePayU == true {
            self.ShowWarningMessage("Cancel Transaction", message: "Your transaction was cancel")
        }
    }
    
    //MARK:  -  Table View DataSource -
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return self.headerResult.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.headerResult.count > section {
            if let headerView = self.headerResult.object(at: section) as? TotalPayHeader {
                if headerView.isExpand == true {
                    return 1
                }
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        if self.headerResult.count > indexPath.section {
            if let headerView = self.headerResult.object(at: indexPath.section) as? TotalPayHeader {
                if headerView.isExpand == true {
                    if indexPath.section == 0 {
                        return 234
                    }else if indexPath.section == 1 {
                        return 60
                    }
                }
            }
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if self.headerResult.count > section {
//            if let headerView = self.headerResult.object(at: section) as? TotalPayHeader {
//                if headerView.isExpand == true {
//                    return 30
//                }
//            }
//        }
        return 40
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if self.headerResult.count > section {
            if let header = self.headerResult.object(at: section) as? TotalPayHeader {
                header.tag = section
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PaymentOptionViewController.TapGestureRecognizer(_:)))
                
                header.addGestureRecognizer(tapGesture)
                return header
            }
        }
        return UIView()
        
    }
    
    func TapGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        //do your stuff here
        if let headerView = gestureRecognizer.view  {
            if self.headerResult.count > headerView.tag {
                if let tempHeaderView = self.headerResult.object(at: headerView.tag) as? TotalPayHeader {
                    if tempHeaderView.isExpand == false {
                        tempHeaderView.isExpand = true
                        self.headerResult.replaceObject(at: headerView.tag, with: tempHeaderView)
                        self.tableview.reloadSections(NSIndexSet(index: tempHeaderView.tag) as IndexSet, with: .fade)
                    }else{
                        
                        tempHeaderView.isExpand = false
                        self.headerResult.replaceObject(at: headerView.tag, with: tempHeaderView)
                        self.tableview.reloadSections(NSIndexSet(index: tempHeaderView.tag) as IndexSet, with: .fade)
                    }
                }
            }
        }
        
        
        
       
    }
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return self.TotalPayableCellNib
        }else{
            if let cell = self.tableview.dequeueReusableCell(withIdentifier: "InputPromoCodeCell") as? InputPromoCodeCell
            {
                return cell
            }
        }
        
        return UITableViewCell()
        
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
//        footerView.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0)
//        
//        return footerView
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    func GetBasketTotalAmount() {
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        var basketTotalPrice : Float = 0
        
        for tempData in basketLocalResul {
            if let resultData = tempData as? NSDictionary {
                if let tempQty = resultData.object(forKey: kBasketQtyKey) as? String {
                    var LiveItemQty : Float = 0
                    if let tempQty = Float(tempQty) {
                        LiveItemQty = tempQty
                    }
                    
                    //Get Prices Data
                    if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                        var ProductItemId = ""
                        if let tempStr = resultData.object(forKey: kProductItemID) as? String {
                            ProductItemId = tempStr
                        }
                        for tempData in priceList {
                            if let itemData = tempData as? NSDictionary {
                                if let itemId = itemData.object(forKey: "price_ID") as? String {
                                    if ProductItemId == itemId {
                                        //GetPrice
                                        var LiveItemPrice : Float = 0
                                        if let itemPrice = itemData.object(forKey: "price") as? String {
                                            
                                            if let tempPrice = Float(itemPrice) {
                                                LiveItemPrice = tempPrice
                                            }
                                        }
                                        
                                        basketTotalPrice = basketTotalPrice + (LiveItemQty * LiveItemPrice)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }
        var OrderTotalPayableAmount : Float = 0
        //---------UpdateUI-----------
        
        //Basket Items
        if basketLocalResul.count > 1 {
            self.TotalPayableCellNib.OrderItems.text = String(format: "%d Items", arguments: [basketLocalResul.count])
        }else{
            self.TotalPayableCellNib.OrderItems.text = String(format: "%d Item", arguments: [basketLocalResul.count])
        }
        
        //Sub Total
        let subTotal = String(format: "₹ %.2f", arguments: [basketTotalPrice])
        self.TotalPayableCellNib.SubTotal.text = subTotal
        self.BasketItesmTotalAmount = basketTotalPrice
         
        OrderTotalPayableAmount = OrderTotalPayableAmount + basketTotalPrice
        
        //Delivery Charge Calculation
        if OrderTotalPayableAmount > ChipChargeMinCartAmount {
            let flotCharge : Float = 0
            OrderTotalPayableAmount = OrderTotalPayableAmount + flotCharge
            self.TotalDeliveryCharge = flotCharge
            self.TotalPayableCellNib.DeliveryCharge.text = String(format: "₹ %.2f", arguments: [flotCharge])
        }else {
            if let userDetailsList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
                if userDetailsList.count > 0 {
                    if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                        var charge = "0"
                        if let tempStr = userResult.object(forKey: "ship_charge") as? String {
                            charge = tempStr
                        }
                        if let tempStr = userResult.object(forKey: "ship_charge") as? NSNumber {
                            charge = tempStr.stringValue
                        }
                        if let flotCharge = Float(charge) {
                            OrderTotalPayableAmount = OrderTotalPayableAmount + flotCharge
                            self.TotalDeliveryCharge = flotCharge
                            self.TotalPayableCellNib.DeliveryCharge.text = String(format: "₹ %.2f", arguments: [flotCharge])
                        }else{
                            self.TotalPayableCellNib.DeliveryCharge.text = String(format: "₹ %@", arguments: [charge])
                        }
                        
                    }
                }
            }
        }
        
        
        self.BasketOrderTotalAmountGlobal = OrderTotalPayableAmount
        
        self.TotalPayableCellNib.OrderTotal.text = String(format: "₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
        
        self.TotalPayableCellNib.TotalPayable.text = String(format: "₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
        
        if self.headerResult.count > 0 {
            if let tempHeaderView = self.headerResult.object(at: 0) as? TotalPayHeader {
                let total = String(format: "₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
                tempHeaderView.totalAmountLabel.text = total
                self.btnBasket.setTitle(total, for: .normal)
            }
        }
        
        
        
        
        DispatchQueue.main.async {
            self.tableview.ReloadWithAnimation()
        }
        
        //Update Footer UI
        if let userDetailsList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
            if userDetailsList.count > 0 {
                if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                    if let walletBalance = userResult.object(forKey: "wallet") as? String {
                        self.sabzilanaWalletLabel.text = walletBalance
                        if let balance = Float(walletBalance) {
                            self.SabzilanaWalletBalance = balance
                        }
                    }
                }
            }
        }
        
        self.sabzilanaWalletLabel.text = String(format: "Available balance: ₹ %.2f", arguments: [self.SabzilanaWalletBalance])
    }

    //MARK:- Did Change Payment Type -
    func didChangePaymentOption(){
        if self.paymentOption == .SabilanaWallet {
            if self.BasketOrderTotalAmountGlobal <= self.SabzilanaWalletBalance {
                let AvialbleBalance : Float = self.SabzilanaWalletBalance - self.BasketOrderTotalAmountGlobal
                
                //Availble Wallet Balance
                self.sabzilanaWalletLabel.text = String(format: "Available balance: ₹ %.2f", arguments: [AvialbleBalance])
                
                //Deduct Wallet Balance
                self.TotalPayableCellNib.WalletAmount.text = String(format: "- ₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
                
                //Net Payable
                self.TotalPayableCellNib.TotalPayable.text = String(format: "₹ %.2f", arguments: [Float(0)])
                
                if self.headerResult.count > 0 {
                    if let tempHeaderView = self.headerResult.object(at: 0) as? TotalPayHeader {
                        let total = String(format: "₹ %.2f", arguments: [0])
                        tempHeaderView.totalAmountLabel.text = total
                        
                        self.btnBasket.setTitle(total, for: .normal)
                        
                    }
                }
                self.NetPayableAmountGlobal = 0
                
            }else{
                let NetPayableBalance : Float = self.BasketOrderTotalAmountGlobal - self.SabzilanaWalletBalance
                
                //Deduct Wallet Balance
                self.TotalPayableCellNib.WalletAmount.text = String(format: "- ₹ %.2f", arguments: [self.SabzilanaWalletBalance])
                
                //Availble Wallet Balance
                self.sabzilanaWalletLabel.text = String(format: "Available balance: ₹ %.2f", arguments: [Float(0)])
                
                //Net Payable
                self.TotalPayableCellNib.TotalPayable.text = String(format: "₹ %.2f", arguments: [NetPayableBalance])
                
                if self.headerResult.count > 0 {
                    if let tempHeaderView = self.headerResult.object(at: 0) as? TotalPayHeader {
                        let total = String(format: "₹ %.2f", arguments: [NetPayableBalance])
                        tempHeaderView.totalAmountLabel.text = total
                        self.btnBasket.setTitle(total, for: .normal)
                        
                    }
                }
                
                self.NetPayableAmountGlobal = NetPayableBalance
                
            }
        }else {
            self.TotalPayableCellNib.WalletAmount.text = "---"
            self.TotalPayableCellNib.CouponDiscount.text = "---"
            
            //Sabzilana Wallet Balance
            self.sabzilanaWalletLabel.text = String(format: "Available balance: ₹ %.2f", arguments: [self.SabzilanaWalletBalance])
            
            //Net Payable
            self.TotalPayableCellNib.TotalPayable.text = String(format: "₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
            
            if self.headerResult.count > 0 {
                if let tempHeaderView = self.headerResult.object(at: 0) as? TotalPayHeader {
                    let total = String(format: "₹ %.2f", arguments: [self.BasketOrderTotalAmountGlobal])
                    tempHeaderView.totalAmountLabel.text = total
                    self.btnBasket.setTitle(total, for: .normal)
                    
                    self.NetPayableAmountGlobal = self.BasketOrderTotalAmountGlobal
                }
            }
        }
        

    }
    
    //MARK:- Pay Click - 
    @IBAction func PlaceOrderNPayClick(){
        
        self.OrderCheckOut()
    }
    
    
    
    //MARK:- Check Out -
    
    func OrderCheckOut(){
        
        if self.paymentOption == nil {
            ShowMessage("", message: "Please select delivery option")
            return
        }
        
        var deliveryData = NSDictionary()
        
        let OrderUserResult = NSMutableDictionary()
        
        if let dataList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
            if dataList.count > 0 {
                if let tempData = dataList.object(at: 0) as? NSDictionary {
                    deliveryData = NSDictionary(dictionary: tempData)
                }
            }
        }
        if let tempStr = deliveryData.object(forKey: "name") as? String {
            OrderUserResult.setValue(tempStr, forKey: "name")
        }
        if let tempStr = deliveryData.object(forKey: "email") as? String {
            OrderUserResult.setValue(tempStr, forKey: "email")
        }
        if let tempStr = deliveryData.object(forKey: "phone") as? String {
            OrderUserResult.setValue(tempStr, forKey: "phone")
        }
        if let tempStr = deliveryData.object(forKey: "address") as? String {
            OrderUserResult.setValue(tempStr, forKey: "address_1")
        }
        if let tempStr = deliveryData.object(forKey: "area") as? String {
            OrderUserResult.setValue(tempStr, forKey: "area")
        }
        if let tempStr = deliveryData.object(forKey: "city") as? String {
            OrderUserResult.setValue(tempStr, forKey: "city")
        }
        
        if let tempStr = deliveryData.object(forKey: "pincode") as? String {
            OrderUserResult.setValue(tempStr, forKey: "zipcode")
        }
        OrderUserResult.setValue("Gujarat", forKey: "state")
        OrderUserResult.setValue("", forKey: "map_address")
        OrderUserResult.setValue("", forKey: "flat")
        OrderUserResult.setValue("No", forKey: "google_location")
        
        
        //Basket Data Colletion 
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let BasketResult = NSMutableArray()
        for Items in basketLocalResul {
            if let ItemsResult = Items as? NSDictionary {
                var productID = ""
                var price_ID = ""
                var quantity = ""
                var price = ""
                
                if let tempStr = ItemsResult.object(forKey: "productID") as? String {
                    productID = tempStr
                }
                if let tempStr = ItemsResult.object(forKey: "ProductItemID") as? String {
                    price_ID = tempStr
                }
                if let tempStr = ItemsResult.object(forKey: "BasketQty") as? String {
                    quantity = tempStr
                }
                //SubItems 
                if let priceList = ItemsResult.object(forKey: "price_list") as? NSArray {
                    for priceTemp in priceList {
                        if let priceResult = priceTemp as? NSDictionary {
                            if let priceInnerId = priceResult.object(forKey: "price_ID") as? String {
                                if priceInnerId == price_ID {
                                    if let innerPrice = priceResult.object(forKey: "price") as? String {
                                        price = innerPrice
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
                
                let itemsResult = ["productID" : productID,"price_ID":price_ID,"quantity":quantity,"price":price]
                BasketResult.add(itemsResult)
            }
        }
        
        
        //MainData Collect
        let MainDataCollect = NSMutableDictionary()
        if let tempStr = getLoginUser().object(forKey: "userID") as? String {
            MainDataCollect.setValue(tempStr, forKey: "userID")
        }
//        payment_method = cod/wallet/payu/paytm.
        var paymentMethod = "cod"
        if self.paymentOption == .SabilanaWallet {
            paymentMethod = "wallet"
        }else if paymentOption == .COD {
            paymentMethod = "cod"
        }else if paymentOption == .PayU {
            paymentMethod = "payu"
        }else if paymentOption == .PayTM {
            paymentMethod = "paytm"
        }
        MainDataCollect.setValue(paymentMethod, forKey: "payment_method")
        
        MainDataCollect.setValue("", forKey: "DIS_ID")
        MainDataCollect.setValue("", forKey: "DIS_AMOUNT")
//        let orderTotal = String(format: "%.2f", self.BasketItesmTotalAmount)
        let orderTotal = String(format: "%.2f", self.BasketOrderTotalAmountGlobal - self.TotalDeliveryCharge)
        MainDataCollect.setValue(orderTotal, forKey: "order_total")
        
//        let nextTotal = String(format: "%.2f", self.BasketOrderTotalAmountGlobal)
        let netPayable = String(format: "%.2f", self.NetPayableAmountGlobal)
        MainDataCollect.setValue(netPayable, forKey: "grand_total")
        
        let ship_charge = String(format: "%.2f", self.TotalDeliveryCharge)
        MainDataCollect.setValue(ship_charge, forKey: "ship_charge")
        
        //DeliveryDate and Time Static
        MainDataCollect.setValue(self.DeliveryDateSlot, forKey: "del_date")
        MainDataCollect.setValue(self.DeliveryTimeSlot, forKey: "del_time")
        MainDataCollect.setValue(orderSpecialInstruction, forKey: "instruction")
        MainDataCollect.setValue("", forKey: "latitude")
        MainDataCollect.setValue("", forKey: "longitude")
        MainDataCollect.setValue("No", forKey: "exp_delivery")
        
        //UserData Added
        MainDataCollect.setValue(OrderUserResult, forKey: "user_details")
        MainDataCollect.setValue(BasketResult, forKey: "cart_items")
        
        print(MainDataCollect)
        
        
        
        let requestStr = GetJson(MainDataCollect)
        
        let urlStr = String(format: "%@?view=checkout_testing&orderDetails=%@&mobiletype=%@", arguments: [kMainDomainUrl,requestStr,kPlatformName])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    DispatchQueue.main.async {
                        self.FinalPaymentCheckOut(data: result)
                    }
                }
                
                /*
                if let result = data as? NSDictionary {
                    if let message = result.object(forKey: kMessage) as? String {
                        ShowMessage("", message: message)
                    }
                }
                 */
                
//                let objRootq = PayTMViewController(nibName: "PayTMViewController", bundle: nil)
//                self.navigationController?.pushViewController(objRootq, animated: true)
                
//                self.startPayment()
            }
            
            
        }
        
    }

    func FinalPaymentCheckOut(data : NSDictionary){
//        ex.php?view=order_paytm_test&userID=394&amount=100&orderID=99
        if self.paymentOption == .PayTM {
            var userId = ""
            if let tempStr = data.object(forKey: "userID") as? String {
                userId = tempStr
            }
            if let tempStr = data.object(forKey: "userID") as? NSNumber {
                userId = tempStr.stringValue
            }
            
            var orderId = ""
            if let tempStr = data.object(forKey: "orderID") as? String {
                orderId = tempStr
            }
            if let tempStr = data.object(forKey: "orderID") as? NSNumber {
                orderId = tempStr.stringValue
            }
            
            var amount = ""
            if let tempStr = data.object(forKey: "Amount") as? String {
                amount = tempStr
            }
            if let tempStr = data.object(forKey: "Amount") as? NSNumber {
                amount = tempStr.stringValue
            }
            
            let paytmRequestUrl = String(format: "%@?view=order_paytm_test&userID=%@&amount=%@&orderID=%@", arguments: [kMainDomainUrl,userId,amount,orderId])
            
            let objRoot = PayTMViewController(nibName: "PayTMViewController", bundle: nil)
            objRoot.paytmRequestUrl = paytmRequestUrl
            
            self.navigationController?.pushViewController(objRoot, animated: true)
            
        }else if self.paymentOption == .PayU {
            self.initialPayUSetup(data: data)
            self.MakePayUPayment()
        }else{
            self.MakeOrderWalletNCOD(data: data)
        }
    }
    func MakeOrderWalletNCOD(data : NSDictionary){
        DispatchQueue.main.async {
            
            //MARK:- Empty Basket -
            EmptySabzilanaBasket()
            
            let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
            if let tempStr = data.object(forKey: "message") as? String {
                let StringArr = tempStr.components(separatedBy: "_")
                if StringArr.count > 0 {
                    objRoot.paymentStatusMessage = StringArr[0]
                }
                if StringArr.count > 1 {
                    objRoot.paymentTitle = StringArr[1]
                }
            }
            if let tempStr = data.object(forKey: kStatusCode) as? String {
                if tempStr.caseInsensitiveCompare("0") == .orderedSame {
                    objRoot.paymentStatusImage = "payment_sucess"
                }else{
                    objRoot.paymentStatusImage = "payment_field"
                }
            }
            
            self.navigationController?.pushViewController(objRoot, animated: true)
            
        }
    }
    
    //MARK:- PayUBiz Make Payment -
    func initialPayUSetup(data : NSDictionary) {
        defaultActivityIndicator = iOSDefaultActivityIndicator()
        paymentParam = PayUModelPaymentParams()
        
        //Field and Sucess Message
        if let tempStr = data.object(forKey: "payu_order_fail") as? String {
            payu_order_fail = tempStr
        }
        if let tempStr = data.object(forKey: "payu_order_success") as? String {
            payu_order_success = tempStr
        }
        
        var payConfigure = NSDictionary()
        
        if let tempList = data.object(forKey: "online_payment_data") as? NSArray {
            if tempList.count > 0 {
                if let paymentData = tempList.object(at: 0) as? NSDictionary {
                    payConfigure = NSDictionary(dictionary: paymentData)
                }
            }
            
        }
        var amount = ""
        if let tempStr = payConfigure.object(forKey: "amount") as? String {
            amount = tempStr
        }
        if let tempStr = payConfigure.object(forKey: "amount") as? NSNumber {
            amount = tempStr.stringValue
        }
        var product_info = ""
        if let tempStr = payConfigure.object(forKey: "product_info") as? String {
            product_info = tempStr
        }
        var first_name = ""
        if let tempStr = payConfigure.object(forKey: "first_name") as? String {
            first_name = tempStr
        }
        var trans_id = ""
        if let tempStr = payConfigure.object(forKey: "trans_id") as? String {
            trans_id = tempStr
        }
        var email = ""
        if let tempStr = payConfigure.object(forKey: "email") as? String {
            email = tempStr
        }
        
        var udf1 = ""
        if let tempStr = payConfigure.object(forKey: "udf1") as? String {
            udf1 = tempStr
        }
        var udf2 = ""
        if let tempStr = payConfigure.object(forKey: "udf2") as? String {
            udf2 = tempStr
        }
        
        
        
        //DetailsData 
        var deliveryData = NSDictionary()
        if let dataList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
            if dataList.count > 0 {
                if let tempData = dataList.object(at: 0) as? NSDictionary {
                    deliveryData = NSDictionary(dictionary: tempData)
                }
            }
        }
        var phone = ""
        if let tempStr = deliveryData.object(forKey: "phone") as? String {
            phone = tempStr
        }
        
        
        paymentParam.amount = amount
        paymentParam.productInfo = product_info
        paymentParam.firstName = first_name
        paymentParam.transactionID = trans_id
        paymentParam.email = email
        paymentParam.userCredentials = "ra:ra"
        paymentParam.phoneNumber = phone
        paymentParam.surl = kPayUsurl
        paymentParam.furl = kPayUfurl
        paymentParam.udf1 = udf1
        paymentParam.udf2 = udf2
        paymentParam.udf3 = "u3"
        paymentParam.udf4 = "u4"
        paymentParam.udf5 = "u5"
        //    self.paymentParam.environment = ENVIRONMENT_PRODUCTION;
        self.setEnvironment(kPayUMode)
       
        
        addPaymentResponseNotofication()
    }
    
    //Make Payment
    func MakePayUPayment(){
        self.ifPaymentModePayU = true
        
        let obj = PayUDontUseThisClass()
        obj.getPayUHashes(withPaymentParam: paymentParam, merchantSalt: kPayUSalt) { (allHashes, hashString, errorMessage) in
            self.callSDKWithHashes(with: allHashes!, withError: errorMessage)
        }
        
    }
    
    func setEnvironment(_ env: String) {
        paymentParam.environment = env
        if (env == ENVIRONMENT_PRODUCTION) {
            paymentParam.key = kPayUKEY
        }
        else {
            paymentParam.key = kPayUKEY
        }
    }
    func addPaymentResponseNotofication() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentOptionViewController.responseReceived(_:)), name: NSNotification.Name(rawValue: kPUUINotiPaymentResponse), object: nil)
    }
    
    
    
    func callSDKWithHashes(with allHashes: PayUModelHashes, withError errorMessage: String?) {
        if errorMessage == nil {
            self.paymentParam.hashes = allHashes
            self.callSDK()
            
        }
        else {
            print(errorMessage ?? "No Error Found")
        }
    }
    func callSDK(){
        paymentParam.oneTapTokenDictionary = nil
        let respo = PayUWebServiceResponse()
        self.webServiceResponse.getPayUPaymentRelatedDetail(forMobileSDK: self.paymentParam) { (paymentRelatedDetails, errorMessage, extraParam) in
            
            
            respo.callVASForMobileSDK(withPaymentParam: self.paymentParam)
            //FORVAS
            let stryBrd = UIStoryboard(name: pUUIStoryBoard, bundle: nil)
            let paymentOptionVC: PUUIPaymentOptionVC = stryBrd.instantiateViewController(withIdentifier: VC_IDENTIFIER_PAYMENT_OPTION) as! PUUIPaymentOptionVC
            paymentOptionVC.paymentParam = self.paymentParam
            paymentOptionVC.paymentRelatedDetail = paymentRelatedDetails
            
            self.navigationController?.pushViewController(paymentOptionVC, animated: true)
        }
        
    }
    func responseReceived(_ notification: Notification) {
        _ = navigationController?.popViewController(animated: false)
        
        //MARK:- Empty Basket -
        EmptySabzilanaBasket()
        
        //    self.textFieldTransactionID.text = [PUSAHelperClass getTransactionIDWithLength:15];
        let strConvertedRespone: String = "\(notification.object)"
        print("Response Received \(strConvertedRespone)")
        
        if let jsonString = notification.object as? String {
            let jsonData = jsonString.data(using: String.Encoding.utf8)
            let JsonDicationary = try? JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
            
            if let response = JsonDicationary as? NSDictionary {
                if let status = response.object(forKey: "status") as? String {
                    print(status)
                    if status.caseInsensitiveCompare("success") == .orderedSame {
                        let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                        objRoot.paymentTitle = "PayU"
                        objRoot.paymentStatusMessage = payu_order_success
                        
                        let StringArr = payu_order_success.components(separatedBy: "_")
                        if StringArr.count > 0 {
                            objRoot.paymentTitle = StringArr[0]
                        }
                        if StringArr.count > 1 {
                            objRoot.paymentStatusMessage = StringArr[1]
                        }
                        
                        objRoot.paymentStatusImage = "payment_sucess"
                        
                        self.navigationController?.pushViewController(objRoot, animated: true)
                    }else{
                        let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                        objRoot.paymentTitle = "PayU"
                        objRoot.paymentStatusMessage = payu_order_fail
                        
                        let StringArr = payu_order_fail.components(separatedBy: "_")
                        if StringArr.count > 0 {
                            objRoot.paymentTitle = StringArr[0]
                        }
                        if StringArr.count > 1 {
                            objRoot.paymentStatusMessage = StringArr[1]
                        }
                        
                        objRoot.paymentStatusImage = "payment_field"
                        
                        self.navigationController?.pushViewController(objRoot, animated: true)
                    }
                }
            }
            
        }
        
        
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }

    //MARK:- Get Basket Price -
    func UpdateBasketPriceNItems() {
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        var basketTotalPrice : Float = 0
        
        for tempData in basketLocalResul {
            if let resultData = tempData as? NSDictionary {
                if let tempQty = resultData.object(forKey: kBasketQtyKey) as? String {
                    var LiveItemQty : Float = 0
                    if let tempQty = Float(tempQty) {
                        LiveItemQty = tempQty
                    }
                    
                    //Get Prices Data
                    if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                        var ProductItemId = ""
                        if let tempStr = resultData.object(forKey: kProductItemID) as? String {
                            ProductItemId = tempStr
                        }
                        for tempData in priceList {
                            if let itemData = tempData as? NSDictionary {
                                if let itemId = itemData.object(forKey: "price_ID") as? String {
                                    if ProductItemId == itemId {
                                        //GetPrice
                                        var LiveItemPrice : Float = 0
                                        if let itemPrice = itemData.object(forKey: "price") as? String {
                                            
                                            if let tempPrice = Float(itemPrice) {
                                                LiveItemPrice = tempPrice
                                            }
                                        }
                                        
                                        basketTotalPrice = basketTotalPrice + (LiveItemQty * LiveItemPrice)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }
        
        
        
    }
    
    
    func ShowWarningMessage(_ title : String, message : String){
        var titleStr = title
        if titleStr.isEmpty == true {
            titleStr = kAppName
        }
        
        let refreshAlert = UIAlertController(title: titleStr, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            DispatchQueue.main.async {
                
                let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                objRoot.paymentTitle = "Ooh no!"
                objRoot.paymentStatusImage = "payment_field"
                objRoot.paymentStatusMessage = "Unfortunately we have an issue with your payment try again."
                self.navigationController?.pushViewController(objRoot, animated: true)
                
            }
        }))
        
        
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
