//
//  ChangeAddressViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/26/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol ChangeAddressDelegate {
    func didChangedAddress(data : NSDictionary)
}
class ChangeAddressViewController: UIViewController,BSKeyboardControlsDelegate,DropDownDelegate {

    var delegate : ChangeAddressDelegate?
    
    var keyboard: BSKeyboardControls!
    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    
    
    @IBOutlet var textName : ACFloatingTextfield!
    @IBOutlet var textEmail : ACFloatingTextfield!
    @IBOutlet var textMobile : ACFloatingTextfield!
    @IBOutlet var textAddress : ACFloatingTextfield!
    @IBOutlet var textArea : ACFloatingTextfield!
    @IBOutlet var textPin : ACFloatingTextfield!
    @IBOutlet var textCity : ACFloatingTextfield!
    
    
    var AreaNPinCodeResult = NSDictionary()
    var pincodeID : String = ""
    var areaID : String = ""
    var shipping : String = ""
    
    var ResponseResultDelivery = NSDictionary() {
        didSet{
            self.UpdateUI()
        }
    }
    
    @IBAction func GetCurrentAddress(){
        
    }
    
    @IBAction func ChangeAddressClick(){
        self.ChangeUserInfo()
    }
    @IBAction func AreaClick(){
        self.OpenAreaPopups(isPinCode: false)
    }
    @IBAction func PinCodeClick(){
        self.OpenAreaPopups(isPinCode: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableview.tableHeaderView = self.headerView
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeAddressViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeAddressViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [textName,textEmail,textMobile,textAddress,textArea,textPin])
        keyboard.delegate = self
        self.GetAreaNPinCode()
        
       
    }
    func UpdateUI(){
        
        if self.textName != nil {
            print(self.ResponseResultDelivery)
            var deliveryData = NSDictionary()
            if let dataList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
                if dataList.count > 0 {
                    if let tempData = dataList.object(at: 0) as? NSDictionary {
                        deliveryData = NSDictionary(dictionary: tempData)
                    }
                }
            }
            if let tempStr = deliveryData.object(forKey: "name") as? String {
                self.textName.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "email") as? String {
                self.textEmail.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "phone") as? String {
                self.textMobile.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "address") as? String {
                self.textAddress.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "area") as? String {
                self.textArea.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "area_id") as? String {
                self.areaID = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "area_id") as? NSNumber {
                self.areaID = tempStr.stringValue
            }
            if let tempStr = deliveryData.object(forKey: "ship_charge") as? String {
                self.shipping = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "ship_charge") as? NSNumber {
                self.shipping = tempStr.stringValue
            }
//            if let tempStr = deliveryData.object(forKey: "city") as? String {
//                self.textCity.text = tempStr
//            }
            
            if let tempStr = deliveryData.object(forKey: "pincode") as? String {
                self.textPin.text = tempStr
            }

        }else{
            self.perform(#selector(ChangeAddressViewController.UpdateUI), with: nil, afterDelay: 0.2)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Change Address"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    func OpenAreaPopups(isPinCode : Bool){
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = AreaViewController.instance()
        container.delegate = self
        
        if isPinCode == true {
            if let listResult = self.AreaNPinCodeResult.object(forKey: "pincode_list") as? NSArray {
                container.navigationTitleString = "Choose Pincode"
                container.responseResult = NSArray(array: listResult)
            }
        }else{
            if let listResult = self.AreaNPinCodeResult.object(forKey: "area_list") as? NSArray {
                container.navigationTitleString = "Choose Area"
                container.responseResult = NSArray(array: listResult)
            }
        }
        
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }

    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.view.endEditing(true)
    }
//    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
//        self.view.endEditing(true)
//    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    
    //MARK:- API CALL -
    
    
    func ChangeUserInfo(){
        
        if textName.text!.isEmpty {
            ShowMessage("", message: "Please enter name")
            return
        }
        else if textEmail.text!.isEmpty {
            ShowMessage("", message: "Please enter email")
            return
        }
        else if textMobile.text!.isEmpty {
            ShowMessage("", message: "Please enter mobile number")
            return
        }
        else if textAddress.text!.isEmpty {
            ShowMessage("", message: "Please enter address")
            return
        }
        else if textArea.text!.isEmpty {
            ShowMessage("", message: "Please select area")
            return
        }else if textPin.text!.isEmpty {
            ShowMessage("", message: "Please enter pincode")
            return
        }
//        userID,page=update_info,date_of_birth,mrg_anniversary,whatsapp_no,file
        
        let requestBody = NSMutableDictionary(dictionary: self.ResponseResultDelivery)
        
        
        var updateRequest = NSMutableDictionary()
        
        if let dataList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
            if dataList.count > 0 {
                if let tempData = dataList.object(at: 0) as? NSDictionary {
                    updateRequest = NSMutableDictionary(dictionary: tempData)
                }
            }
        }
        updateRequest.setValue(textName.text, forKey: "name")
        updateRequest.setValue(textMobile.text, forKey: "phone")
        updateRequest.setValue(textCity.text, forKey: "city")
        updateRequest.setValue(textAddress.text, forKey: "address")
        updateRequest.setValue(textArea.text, forKey: "area")
        updateRequest.setValue(self.areaID, forKey: "area_id")
        updateRequest.setValue(self.shipping, forKey: "ship_charge")
        updateRequest.setValue(textPin.text, forKey: "pincode")
        updateRequest.setValue(textEmail.text, forKey: "email")
        
        requestBody.setValue([updateRequest], forKey: "get_detail")
        
//        //Collect Data From Delivery
//        var deliveryData = NSDictionary()
//        if let dataList = self.ResponseResultDelivery.object(forKey: "get_detail") as? NSArray {
//            if dataList.count > 0 {
//                if let tempData = dataList.object(at: 0) as? NSDictionary {
//                    deliveryData = NSDictionary(dictionary: tempData)
//                }
//            }
//        }
//
//        var date_of_birth = ""
//        if let tempStr = deliveryData.object(forKey: "date_of_birth") as? String {
//            date_of_birth = tempStr
//        }
//        requestBody.setValue(date_of_birth, forKey: "date_of_birth")
//        
//        var mrg_anniversary = ""
//        if let tempStr = deliveryData.object(forKey: "mrg_anniversary") as? String {
//            mrg_anniversary = tempStr
//        }
//        requestBody.setValue(mrg_anniversary, forKey: "mrg_anniversary")
//        
//        var whatsapp_no = ""
//        if let tempStr = deliveryData.object(forKey: "whatsapp_no") as? String {
//            whatsapp_no = tempStr
//        }
//        requestBody.setValue(whatsapp_no, forKey: "whatsapp_no")
        
        
        print(requestBody)
        
        self.view.endEditing(true)
        
        if self.delegate != nil {
            self.delegate?.didChangedAddress(data: requestBody)
        }
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
        
        /*
        let urlStr = String(format: "%@?view=change_info", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(requestBody, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            DispatchQueue.main.async {
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
         */
        
    }
    
    
    func GetAreaNPinCode(){
        
        let urlStr = String(format: "%@?view=area_pincode", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.AreaNPinCodeResult = NSDictionary(dictionary: result)
                        }
                    }
                }
            }
        }
        
    }
    
    //MARK:- DropDown Delegate -
    func didSelectDropDownResult(data: AnyObject, screenName: String) {
        print(data)
        if let result = data as? NSDictionary {
            if let tempStr = result.object(forKey: "areaID") as? String {
                self.areaID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textArea.text = nameStr
                }
                if let nameStr = result.object(forKey: "shipping") as? String {
                    self.shipping = nameStr
                }
                if let nameStr = result.object(forKey: "shipping") as? NSNumber {
                    self.shipping = nameStr.stringValue
                }
            }else if let tempStr = result.object(forKey: "pincodeID") as? String {
                self.pincodeID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textPin.text = nameStr
                }
            }
            
            
        }
    }
    
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
