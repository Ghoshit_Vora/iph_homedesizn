//
//  RegisterViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/25/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, BSKeyboardControlsDelegate,DropDownDelegate {

    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    @IBOutlet var btnSignIn : UIButton!
    @IBOutlet var btnTermsNCondition : UIButton!
    
    @IBOutlet var textFieldName : ACFloatingTextfield!
    @IBOutlet var textFieldEmail : ACFloatingTextfield!
    @IBOutlet var textFieldMobile : ACFloatingTextfield!
    @IBOutlet var textFieldPasswor : ACFloatingTextfield!
    @IBOutlet var textFieldState : ACFloatingTextfield!
    
    @IBOutlet var textFieldCity : ACFloatingTextfield!
   
    @IBOutlet var btnTerms : UIButton?
    
    var stateList = NSArray()
    var cityList = NSArray()
    
    var keyboard: BSKeyboardControls!
    
    var stateID : String = ""
    var cityID : String = ""
    
    @IBAction func acceptTermsClick(){
        if self.btnTerms?.isSelected == true {
            self.btnTerms?.isSelected = false
        }else{
            self.btnTerms?.isSelected = true
        }
    }
    @IBAction func SignInClick(){
        var isFound : Bool = false
        
        if let viewControlers = self.navigationController?.viewControllers {
            for controller in viewControlers {
                if let tempController = controller as? LoginViewController {
                     isFound = true
                _ = self.navigationController?.popToViewController(tempController, animated: true)
                    break
                }
            }
        }
        if isFound == false {
            let objRoot = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
        
    }
    @IBAction func TermsNCondtiion(){
        
         let objRoot = SabzilanaWebViewController(nibName: "SabzilanaWebViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    @IBAction func registerClick(){
        self.RegisterResult()
    }
    @IBAction func StateClick(){
        self.OpenAreaPopups(isSate: true)
    }
    @IBAction func CityClick(){
        self.OpenAreaPopups(isSate: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        self.btnSignIn.UnderLineText(text: "Sign In")
        self.btnTermsNCondition.UnderLineText(text: "Terms & Condition")
        
        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.tableview.tableHeaderView = self.headerView
            self.navigationController?.navigationBar.isHidden = false
        }
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [textFieldName,textFieldEmail,textFieldMobile,textFieldPasswor])
        
        keyboard.delegate = self
        
        
        
        self.GetStateList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        DispatchQueue.main.async {
            
            self.navigationController?.navigationBar.topItem?.title = "Sign Up"
            self.navigationItem.title = "Sign Up"
            self.navigationController?.navigationBar.isHidden = false
        }
    }
    
    func OpenAreaPopups(isSate : Bool){
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }

        let container = AreaViewController.instance()
        container.delegate = self
        
        if isSate == true {
            container.navigationTitleString = "Choose State"
            container.responseResult = NSArray(array: self.stateList)
        }else{
            container.navigationTitleString = "Choose City"
            container.responseResult = NSArray(array: self.cityList)
        }
        
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    
    //MARK:- API CALL -
    func RegisterResult(){
        
        
        if textFieldName.text!.isEmpty {
            ShowMessage("", message: "Please enter name")
            return
        }
        else if textFieldEmail.text!.isEmpty {
            ShowMessage("", message: "Please enter email")
            return
        }
        else if textFieldMobile.text!.isEmpty {
            ShowMessage("", message: "Please enter mobile number")
            return
        }
        if let pincode = textFieldMobile.text {
            if pincode.count != 10 {
                ShowMessage("", message: "Mobile number must be a 10 digit")
                return
            }
        }
        else if textFieldPasswor.text!.isEmpty {
            ShowMessage("", message: "Please enter password")
            return
        }else if textFieldState.text!.isEmpty {
            ShowMessage("", message: "Please select state")
            return
        }else if textFieldCity.text!.isEmpty {
            ShowMessage("", message: "Please select city")
            return
        }
        
        self.view.endEditing(true)
        
        
        let urlStr = String(format: "%@?view=register&page=signup&name=%@&email=%@&phone=%@&pass=%@&city_id=%@&state_id=%@&device_id=1&gcmregid=", arguments: [kMainDomainUrl,self.textFieldName.text!, self.textFieldEmail.text!,self.textFieldMobile.text!,self.textFieldPasswor.text!,self.cityID,self.stateID])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                             DispatchQueue.main.async {
                                let objRoot = OTPVerifyViewController(nibName: "OTPVerifyViewController", bundle: nil)
                                
                                if let loginList = result.object(forKey: "user_detail") as? NSArray {
                                    if loginList.count > 0 {
                                        if let loginUserData = loginList.object(at: 0) as? NSDictionary {
                                           
                                            objRoot.registerResult = NSDictionary(dictionary: loginUserData)
                                        }
                                    }
                                }
                               
                                
                                self.navigationController?.pushViewController(objRoot, animated: true)
                            }
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    
    func GetStateList(){
        
        let urlStr = String(format: "%@?view=state_list", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "state") as? NSArray {
                                self.stateList = NSArray(array: tempSate)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    func GetCityList(stateId : String){
        
        let urlStr = String(format: "%@?view=city_list&state_id=%@", arguments: [kMainDomainUrl,stateId])
        print(urlStr)
//        http://www.homedezin.com/mapp/index.php?view=city_list&state_id=4
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "City") as? NSArray {
                                self.cityList = NSArray(array: tempSate)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    
    //MARK:- DropDown Delegate -
    func didSelectDropDownResult(data : AnyObject, screenName : String) {
         print(data)
        if let result = data as? NSDictionary {
            if screenName == "Choose State" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.stateID = tempStr
                    self.GetCityList(stateId: tempStr)
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textFieldState.text = nameStr
                    }
                    self.cityID = ""
                    self.textFieldCity.text = ""
                }
            }else if screenName == "Choose City" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.cityID = tempStr
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textFieldCity.text = nameStr
                    }
                }
            }
            
        }
    }
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        /*
        if let result = data as? NSDictionary {
            if let tempStr = result.object(forKey: "ID") as? String {
                self.areaID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textFieldArea.text = nameStr
                }
            }else if let tempStr = result.object(forKey: "pincodeID") as? String {
                self.pincodeID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textFieldPinCode.text = nameStr
                }
            }
            
            
        }
        */
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
