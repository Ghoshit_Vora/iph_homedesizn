//
//  OTPVerifyViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 26/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OTPVerifyViewController: UIViewController,BSKeyboardControlsDelegate {

    @IBOutlet var txtOtp : UITextField!
    var keyboard: BSKeyboardControls!
    var registerResult = NSDictionary()
    var isResend : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        self.keyboard = BSKeyboardControls(fields: [txtOtp])
        self.keyboard.delegate = self
    }

    @IBAction func verifyClick(){
        var serverOTP = ""
        var localOTP = ""
        
        if txtOtp.text!.isEmpty {
            ShowMessage("", message: "Please enter OTP")
            return
        }else{
            localOTP = txtOtp.text!
        }
        if let tempStr = registerResult.object(forKey: "otp") as? String {
            serverOTP = tempStr
        }
        
        if self.isResend == false {
            self.VerifyOTP()
        }else{
            self.VerifyOTP()
        }
        
        
    }
    @IBAction func ResndCode(){
        self.ResendOTP()
    }
    
    func VerifyOTP(){
        var localOTP = ""
        
        if txtOtp.text!.isEmpty {
            ShowMessage("", message: "Please enter OTP")
            return
        }else{
            localOTP = txtOtp.text!
        }
        var userId = ""
        if let tempStr = registerResult.object(forKey: "userID") as? String {
            userId = tempStr
        }
        var phone = ""
        if let tempStr = registerResult.object(forKey: "phone") as? String {
            phone = tempStr
        }
        let urlStr = String(format: "%@?view=otp_verify&page=verify&userPhone=%@&userID=%@&otp=%@", arguments: [kMainDomainUrl,phone,userId,localOTP])
        print(urlStr)
       
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let loginList = result.object(forKey: "user_detail") as? NSArray {
                                if loginList.count > 0 {
                                    if let loginUserData = loginList.object(at: 0) as? NSDictionary {
                                        //StoreLoginData
                                        setLoginUser(loginUserData)
                                        self.loadLoginResult(data: loginUserData)
                                    }
                                }
                            }
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                        }
                    }
                }
            }
        }
        
    }
    func loadLoginResult(data : NSDictionary){
        if let tempStr = data.object(forKey: "userID") as? String {
            appDelegate.SabzilanaLoginUserId = tempStr
            
            DispatchQueue.main.async {
                let viewController = ProductHomeViewController(nibName: "ProductHomeViewController", bundle: nil)
                appDelegate.navigationController = UINavigationController(rootViewController: viewController)
                appDelegate.styleNavigationController(appDelegate.navigationController)
                appDelegate.window?.rootViewController = appDelegate.navigationController
            }
//            self.GetUserInfo()
        }
        
    }
    func ResendOTP(){
        
        var userId = ""
        if let tempStr = registerResult.object(forKey: "userID") as? String {
            userId = tempStr
        }
        var phone = ""
        if let tempStr = registerResult.object(forKey: "phone") as? String {
            phone = tempStr
        }
        let urlStr = String(format: "%@?view=otp_verify&page=send_otp&userPhone=%@&userID=%@", arguments: [kMainDomainUrl,phone,userId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.isResend = true
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func GetUserInfo(){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=getinfo&userID=%@&phone=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            setLoginUser(result)
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
