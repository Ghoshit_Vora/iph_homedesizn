//
//  AreaViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/25/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol DropDownMenuDelegate {
    func didSelectDropDownResult(data : AnyObject)
}

protocol DropDownDelegate {
    func didSelectDropDownResult(data : AnyObject, screenName : String)
}
class AreaViewController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var searchBar: UISearchBar!
    
    var delegate : DropDownDelegate?
    @IBOutlet var titleLabel : UILabel!
    var navigationTitleString : String = ""
    
    @IBOutlet var tableview : UITableView!
    
    //
    var searchingResult = NSMutableArray()
    var is_searching : Bool = false
    
    var responseResult = NSArray() {
        didSet{
            self.UpdateUI()
        }
    }
    var closeHandler: (() -> Void)?
    
    class func instance() -> AreaViewController {
        let storyboard = AreaViewController(nibName: "AreaViewController", bundle: nil)
        return storyboard
    }
    func UpdateUI(){
        if self.tableview != nil  {
            DispatchQueue.main.async
                {
                    self.titleLabel.text = self.navigationTitleString
                    self.tableview.dataSource = self
                    self.tableview.delegate = self
                    self.tableview.ReloadWithAnimation()
            }
            
        }else{
            self.perform(#selector(AreaViewController.UpdateUI), with: nil, afterDelay: 0.2)
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "RegisterListCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "RegisterListCell")
    }

    @IBAction func closeClick(){
        closeHandler?()
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 50, height: UIScreen.main.bounds.height - 40)
    }
    
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.is_searching == true {
            return self.searchingResult.count
        }
        return self.responseResult.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.is_searching == true {
            if self.searchingResult.count > indexPath.row {
                if let data = self.searchingResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tableview.dequeueReusableCell(withIdentifier: "RegisterListCell") as? RegisterListCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }
        }else{
            if self.responseResult.count > indexPath.row {
                if let data = self.responseResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tableview.dequeueReusableCell(withIdentifier: "RegisterListCell") as? RegisterListCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? RegisterListCell {
            if self.delegate != nil {
                self.delegate?.didSelectDropDownResult(data: cell.resultData, screenName: self.navigationTitleString)
                
                closeHandler?()
            }
        }
    }

    
    
    //MARK:- SearchBar Delegate -
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        is_searching = true;
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchBar.text?.isEmpty)!{
            searchBar.showsCancelButton = false
            is_searching = false
            DispatchQueue.main.async
                {
                    self.tableview.reloadData()
            }
        } else {
            
            if let searchText = searchBar.text {
                searchBar.showsCancelButton = true
                print(" search text %@ ",searchBar.text ?? "Not Found")
                is_searching = true
                self.searchingResult.removeAllObjects()
                
                for data in self.responseResult {
                    if let tempData = data as? NSDictionary {
                        if let tempStr = tempData.object(forKey: "name") as? String {
                            if tempStr.lowercased().range(of: (searchText.lowercased())) != nil {
                                self.searchingResult.add(tempData)
                                
                            }
                        }
                    }
                }
                DispatchQueue.main.async
                    {
                        self.tableview.reloadData()
                }

            }
            
            
           
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
