//
//  SearchResultCell.swift
//  Sabzilana
//
//  Created by Apple on 06/05/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet var titleLabel : UILabel!
    var resultData = NSDictionary(){
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        if self.titleLabel != nil  {
            //print(self.resultData)
            DispatchQueue.main.async {
                if let tempStr = self.resultData.object(forKey: "name") as? String {
                    
                    if let typeStr = self.resultData.object(forKey: "type") as? String {
                        if typeStr.caseInsensitiveCompare("product") == .orderedSame {
                            self.titleLabel.text = tempStr
                        }else{
                             let titleStr = String(format: "%@ in Categories", arguments: [tempStr])
                            let combination = NSMutableAttributedString(string: titleStr)
                            combination.addAttribute(NSForegroundColorAttributeName, value: UIColor.blue, range: NSRange(location:0,length:tempStr.characters.count))
                            
                           
                            self.titleLabel.attributedText = combination
                        }
                    }else{
                        self.titleLabel.text = tempStr
                    }
                    
                    
                }
            }
            
        }else{
            self.perform(#selector(SearchResultCell.SetResultUI))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
