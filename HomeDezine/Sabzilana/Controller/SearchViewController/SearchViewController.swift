//
//  SearchViewController.swift
//  Sabzilana
//
//  Created by Apple on 06/05/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var tableview : UITableView!
    var searchOriginalResult = NSArray()
    var searchingResult = NSMutableArray()
    var is_searching : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibName = UINib(nibName: "SearchResultCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "SearchResultCell")
        
        self.tableview.estimatedRowHeight = 44
        // Do any additional setup after loading the view.
//        https://www.sabzilana.com/sabzi/index.php?view=product&userID=Mzk0&catID=&pagecode=0&search=a
        let tempList = GetSearchResult()
        print(tempList)
        if tempList.count > 0 {
            DispatchQueue.main.async
                {
                    self.searchOriginalResult = NSArray(array: tempList)
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.ReloadWithAnimation()
            }
        }else{
            self.GetSearchResultAPI()
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Search"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        
        self.navigationItem.rightBarButtonItems = [search]
    }
    
    
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.is_searching == true {
            return self.searchingResult.count
        }
        return self.searchOriginalResult.count
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.is_searching == true {
            if self.searchingResult.count > indexPath.row {
                if let data = self.searchingResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tableview.dequeueReusableCell(withIdentifier: "SearchResultCell") as? SearchResultCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }

        }else{
            if self.searchOriginalResult.count > indexPath.row {
                if let data = self.searchOriginalResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tableview.dequeueReusableCell(withIdentifier: "SearchResultCell") as? SearchResultCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }

        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? SearchResultCell {
            print(cell.resultData)
            let mustableResult = NSMutableDictionary(dictionary: cell.resultData)
            var idStr = ""
            if let searchID = cell.resultData.object(forKey: "productID") as? String {
                idStr = searchID
            }
            if let tempStr = cell.resultData.object(forKey: "type") as? String {
                if tempStr.caseInsensitiveCompare("product") == .orderedSame {
                    mustableResult.setValue(idStr, forKey: "productID")
                    
                    let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
                    objRoot.productResult = mustableResult
                    self.navigationController?.pushViewController(objRoot, animated: true)
                    
                }else{
                    mustableResult.setValue(idStr, forKey: "catID")
                    let viewController = ProductViewController(nibName: "ProductViewController", bundle: nil)
                    viewController.categoryResultData = mustableResult
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
        }
        

        
    }

    
    func GetSearchResultAPI(){
        
       
        let urlStr = String(format: "%@?view=search&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0"{
                            
                            if let searchList = result.object(forKey: "product_list") as? NSArray {
                                self.searchOriginalResult = NSArray(array: searchList)
                                SetSearchResult(searchList)
                                
                                DispatchQueue.main.async
                                    {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.ReloadWithAnimation()
                                }
                            }
                            
                        }
                    
                    }
                }
            }
        }
        
    }
    

    //MARK:- SearchBar Delegate -
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        is_searching = true;
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchBar.text?.isEmpty)!{
            searchBar.showsCancelButton = false
            is_searching = false
            self.tableview.reloadData()
        } else {
            searchBar.showsCancelButton = true
            let searchText = searchBar.text
            print(" search text %@ ",searchBar.text ?? "Not Found")
            is_searching = true
            self.searchingResult.removeAllObjects()
            
            for data in self.searchOriginalResult {
                if let tempData = data as? NSDictionary {
                    if let tempStr = tempData.object(forKey: "name") as? String {
                        if tempStr.lowercased().range(of: (searchText?.lowercased())!) != nil {
                            self.searchingResult.add(tempData)
                        }
                    }
                }
            }
           
           self.tableview.reloadData()
        }
    }
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    

    ///
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
