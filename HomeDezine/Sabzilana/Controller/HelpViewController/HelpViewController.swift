//
//  HelpViewController.swift
//  synax
//
//  Created by TNM3 on 6/14/16.
//  Copyright © 2016 TNM3. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet var btnNext : UIButton!
    @IBOutlet var btnPrev : UIButton!
    
    var totalPages = 1
    
//    let sampleBGColors: Array<UIColor> = [UIColor.redColor(), UIColor.yellowColor(), UIColor.greenColor(), UIColor.magentaColor(), UIColor.orangeColor()]

//    let helpImage: Array<String> = ["intro_one.jpg","intro_two.jpg","intro_three.jpg","intro_four.jpg","intro_five.jpg"]

    let resultList : NSArray = [["title":"Washing","desc":"Find the complete washed and almost equivalent to organic veggies","image":"intro_one.jpg"],["title":"Assorting","desc":"Untouched assorting and quality control by experienced team","image":"intro_two.jpg"],["title":"Hygenic","desc":"Hygienic packing in non woven bags and food grade cling rolls","image":"intro_three.jpg"],["title":"Home delivery","desc":"To get accurate weight is the prime right of our customer weight is\nour priority","image":"intro_four.jpg"],["title":"Home","desc":"All corner delivery in Nagpur at your suitable delivery slot","image":"intro_five.jpg"],["title":"NQARP\n(No Question Ask Return Policy)","desc":"If you are not satisfied, you may return it on the time of delivery","image":"intro_six.jpg"]]
    
    @IBAction func closeClick(sender : AnyObject){
//        self.navigationController?.popFadeViewController()
        self.dismiss(animated: true) { () -> Void in
//            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = UIScreen.main.bounds
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
        self.btnPrev.isHidden = true
        DispatchQueue.main.async(execute: { () -> Void in
            self.configureScrollView()
            self.configurePageControl()
        })
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    // MARK: Custom method implementation
    
    func configureScrollView() {
        // Enable paging.
        self.scrollView.isPagingEnabled = true
        
        // Set the following flag values.
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.scrollsToTop = false
        
        // Set the scrollview content size.
        
        self.scrollView.contentSize = CGSize(width: CGFloat(scrollView.frame.size.width * CGFloat(totalPages)), height: CGFloat(0))
        
        
        // Set self as the delegate of the scrollview.
        self.scrollView.delegate = self
        
        self.totalPages = resultList.count
        var index : Int = 0
        // Load the TestView view from the TestView.xib file and configure it properly.
        for tempData in resultList {
            if let data = tempData as? NSDictionary {
                // Load the TestView view.
                let testView = HelpView.instanceFromNib()
                testView.frame = UIScreen.main.bounds
                // Set its frame and the background color.
                testView.frame = CGRect(x: CGFloat(CGFloat(index) * scrollView.frame.size.width), y: CGFloat(scrollView.frame.origin.y), width: CGFloat(scrollView.frame.size.width), height: CGFloat(scrollView.frame.size.height))
                
                //set data
                if let imageStr = data.object(forKey: "image") as? String {
                    let imgHelp = UIImage(named: imageStr)
                    testView.helpImage.image = imgHelp
                }
                if let tempStr = data.object(forKey: "title") as? String {
                    testView.titleLabel.text = tempStr
                }
                if let tempStr = data.object(forKey: "desc") as? String {
                    testView.descLabel.text = tempStr
                }
                if index >= 5 {
                    testView.descLabel.frame.origin.y += 15
                }
                index += 1
                self.scrollView.addSubview(testView)
            }
        }
        self.scrollView.contentSize = CGSize(width: CGFloat(self.scrollView.frame.size.width * CGFloat(index)), height: CGFloat(scrollView.frame.size.height))

    }
    
    
    
    
    
    func configurePageControl() {
        // Set the total pages to the page control.
        pageControl.numberOfPages = totalPages
        
        // Set the initial page.
        pageControl.currentPage = 0
    }
    
    
    // MARK: UIScrollViewDelegate method implementation
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        
        print(pageControl.currentPage)
        self.didChangePages(pageNumber: pageControl.currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Calculate the new page index depending on the content offset.
        let currentPage = floor(scrollView.contentOffset.x / UIScreen.main.bounds.size.width);
        
        // Set the new page index to the page control.
        pageControl.currentPage = Int(currentPage)
//
        
        print(pageControl.currentPage)
        self.didChangePages(pageNumber: pageControl.currentPage)
    }
    
    
    // MARK: IBAction method implementation
    
    @IBAction func changePage(sender: AnyObject) {
        // Calculate the frame that should scroll to based on the page control current page.
        var newFrame = scrollView.frame
        newFrame.origin.x = newFrame.size.width * CGFloat(pageControl.currentPage)
        scrollView.scrollRectToVisible(newFrame, animated: true)
        
    }

    func didChangePages(pageNumber : Int){
        pageControl.currentPage = pageNumber
        
        if pageNumber >= 5 {
            self.btnNext.setTitle("GOT IT", for: .normal)
        }else{
            self.btnNext.setTitle("NEXT", for: .normal)
        }
        if pageNumber == 0 {
            self.btnPrev.isHidden = true
        }else{
            self.btnPrev.isHidden = false
        }
    }
    
    @IBAction func nextePage(sender: AnyObject) {
        // Calculate the frame that should scroll to based on the page control current page.
        print(pageControl.currentPage)
        var newFrame = scrollView.frame
        newFrame.origin.x = newFrame.size.width * CGFloat(pageControl.currentPage + 1)
        scrollView.scrollRectToVisible(newFrame, animated: true)
        
        if btnNext.titleLabel?.text == "GOT IT" {
            
            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
            appDelegate.navigationController = UINavigationController(rootViewController: viewController)
            appDelegate.styleNavigationController(appDelegate.navigationController)
            appDelegate.window?.rootViewController = appDelegate.navigationController
           
        }else{
            print("change page")
        }
        
        self.didChangePages(pageNumber: pageControl.currentPage)
        
        
    }
    @IBAction func prevPage(sender: AnyObject) {
        // Calculate the frame that should scroll to based on the page control current page.
        print(pageControl.currentPage)
        var newFrame = scrollView.frame
        newFrame.origin.x = newFrame.size.width * CGFloat(pageControl.currentPage - 1)
        scrollView.scrollRectToVisible(newFrame, animated: true)
        
        self.didChangePages(pageNumber: pageControl.currentPage)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
