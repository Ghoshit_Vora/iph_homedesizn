//
//  InquiryListViewController.swift
//  Sabzilana
//
//  Created by Desap Team on 26/01/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class InquiryListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

     @IBOutlet var tableview : UITableView?
    var inquiryList = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "Inquiry List"
        
        let nibName = UINib(nibName: "InquiryCell", bundle: nil)
        self.tableview?.register(nibName, forCellReuseIdentifier: "InquiryCell")
        
        self.GetEnquiryList()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inquiryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.inquiryList.count > indexPath.row {
            if let cell = self.tableview?.dequeueReusableCell(withIdentifier: "InquiryCell") as? InquiryCell {
                if let result = self.inquiryList.object(at: indexPath.row) as? NSDictionary {
                    if let tempStr = result.object(forKey: "enquiryID") as? String {
                        cell.inquiryIdLabel?.text = "Inquiry Id: \(tempStr)"
                    }
                    if let tempStr = result.object(forKey: "quantity") as? String {
                        if tempStr.isEmpty == false {
                            cell.qtyLabel?.text = "Quantity: \(tempStr)"
                        }else{
                            cell.qtyLabel?.text = "Quantity: 0"
                        }
                        
                    }
                    if let tempStr = result.object(forKey: "product_name") as? String {
                        cell.productNameLabel?.text = tempStr
                    }
                    if let tempStr = result.object(forKey: "date") as? String {
                        cell.dateLabel?.text = tempStr
                    }
                    if let tempStr = result.object(forKey: "message") as? String {
                        cell.messageLabel?.text = tempStr
                    }
                    if let tempStr = result.object(forKey: "phone") as? String {
                        cell.phoneLabel?.text = tempStr
                    }
                    if let tempStr = result.object(forKey: "image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                            cell.imageview?.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                                if image != nil {
                                    cell.imageview?.image = image
                                }
                            })
                            
                        }
                    }
                }
                return cell
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
   
    func GetEnquiryList(){
        
        let urlStr = String(format: "%@?view=enquiry_list&userID=%@&page_no=0", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                
                //New Product
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let inquiry = result.object(forKey: "enquiry_list") as? NSArray {
                                self.inquiryList = NSArray(array: inquiry)
                                DispatchQueue.main.async {
                                    self.tableview?.delegate = self
                                    self.tableview?.dataSource = self
                                    self.tableview?.reloadData()
                                }
                                
                            }
//
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
                
            }
           
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
