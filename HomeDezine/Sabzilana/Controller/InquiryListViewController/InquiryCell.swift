//
//  InquiryCell.swift
//  Sabzilana
//
//  Created by Desap Team on 26/01/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class InquiryCell: UITableViewCell {

    @IBOutlet var inquiryIdLabel : UILabel?
    @IBOutlet var productNameLabel : UILabel?
    @IBOutlet var messageLabel : UILabel?
    @IBOutlet var qtyLabel : UILabel?
    @IBOutlet var phoneLabel : UILabel?
    @IBOutlet var dateLabel : UILabel?
    @IBOutlet var imageview : UIImageView?
    
    @IBOutlet var containerView : UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        containerView?.layer.cornerRadius = 10
//        containerView?.layer.shadowColor = UIColor.gray.cgColor
//        containerView?.layer.shadowOffset = CGSize.zero
//        containerView?.layer.shadowOpacity = 1.0
//        containerView?.layer.shadowRadius = 7.0
//        containerView?.layer.masksToBounds =  false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
