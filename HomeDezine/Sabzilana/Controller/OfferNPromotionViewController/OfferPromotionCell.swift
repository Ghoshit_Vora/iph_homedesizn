//
//  OfferPromotionCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/6/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol OfferNPromotionCellDelegate {
    func BuyButtonTapped(data : NSDictionary)
}
class OfferPromotionCell: UITableViewCell {

    var delegate : OfferNPromotionCellDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var descLabel : UILabel!
    @IBOutlet var dateLabel : UILabel!
    
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    @IBAction func shareClick(){
        var image : UIImage?
        
        if let tempImage = self.imageview.image {
            image = tempImage
        }
        
        var shareText = ""
        if let tempStr = self.descLabel.text {
            shareText = tempStr
        }
        
        let shareResult = NSMutableArray()
        if image != nil {
            shareResult.add(image ?? #imageLiteral(resourceName: "ic_logo"))
        }
        shareResult.add(shareText)
        
        appDelegate.ShareAppObject(shareResult)
    }
    
    @IBAction func buyClick(){
        if self.delegate != nil {
            self.delegate?.BuyButtonTapped(data: self.resultData)
        }
    }
    func SetResultUI(){
        
        if self.imageview != nil  {
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "title") as? String {
                self.descLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "added_on") as? String {
                self.dateLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "product") as? String {
                if tempStr.isEmpty == false {
//                    self.buyButton.isHidden = false
                }else{
//                    self.buyButton.isHidden = true
                }
            }
            
        }else{
            self.perform(#selector(OfferPromotionCell.SetResultUI))
        }
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
