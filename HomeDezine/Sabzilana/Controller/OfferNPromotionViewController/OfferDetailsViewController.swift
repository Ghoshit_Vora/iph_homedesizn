//
//  OfferDetailsViewController.swift
//  Sabzilana
//
//  Created by Desap Team on 22/03/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class OfferDetailsViewController: UIViewController {

    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var messageLabel : UILabel?
    @IBOutlet var timeLabel : UILabel?
    @IBOutlet var imageview : UIImageView?
    
    @IBOutlet var exploreButton : UIButton?
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    @IBAction func exploreClick(){
        var cat_button = ""
        
        
        if let tempStr = self.resultData.object(forKey: "cat_button") as? String {
            cat_button = tempStr
        }
        
        if cat_button.caseInsensitiveCompare("yes") == .orderedSame
        {
            let result = NSMutableDictionary(dictionary: self.resultData)
            if let buttonID = self.resultData.object(forKey: "buttonID") as? String {
                result.setObject(buttonID, forKey: "catID" as NSCopying)
                
                let objRoot = ProductViewController(nibName: "ProductViewController", bundle: nil)
                objRoot.categoryResultData = result
                self.navigationController?.pushViewController(objRoot, animated: true)
                
            }
            
        }else{
            var product_button = ""
            if let tempStr = resultData.object(forKey: "product_button") as? String {
                product_button = tempStr
            }
            if product_button.caseInsensitiveCompare("yes") == .orderedSame {
                let result = NSMutableDictionary(dictionary: self.resultData)
                if let buttonID = self.resultData.object(forKey: "buttonID") as? String {
                  
                    result.setObject(buttonID, forKey: "productID" as NSCopying)
                    let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
                    objRoot.productResult = result
                    self.navigationController?.pushViewController(objRoot, animated: true)
                    
                }
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(self.shareClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
        
        
    }
//    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = .clear
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        appDelegate.styleNavigationController(self.navigationController!)
//    }
    func SetResultUI(){
        
        if self.imageview != nil  {
            print(resultData)
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview?.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview?.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "title") as? String {
                self.titleLabel?.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "message") as? String {
                self.messageLabel?.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "added_on") as? String {
                self.timeLabel?.text = tempStr
            }
            
            var cat_button = ""
            var product_button = ""
            
            if let tempStr = resultData.object(forKey: "cat_button") as? String {
                cat_button = tempStr
            }
            if let tempStr = resultData.object(forKey: "product_button") as? String {
                product_button = tempStr
            }
            if cat_button.caseInsensitiveCompare("yes") == .orderedSame || product_button.caseInsensitiveCompare("yes") == .orderedSame {
                self.exploreButton?.isHidden = false
            }else{
                self.exploreButton?.isHidden = true
            }
        }else{
            self.perform(#selector(SetResultUI), with: nil, afterDelay: 0.2)
        }
        
    }
    @IBAction func shareClick(){
        var image : UIImage?
        
        if let tempImage = self.imageview?.image {
            image = tempImage
        }
        
        var shareText = ""
        if let tempStr = self.messageLabel?.text {
            shareText = tempStr
        }
        
        let shareResult = NSMutableArray()
        if image != nil {
            shareResult.add(image ?? #imageLiteral(resourceName: "ic_logo"))
        }
        shareResult.add(shareText)
        
        appDelegate.ShareAppObject(shareResult)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
