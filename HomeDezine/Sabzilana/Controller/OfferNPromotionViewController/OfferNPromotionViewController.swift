//
//  OfferNPromotionViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/6/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OfferNPromotionViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, OfferNPromotionCellDelegate {

    @IBOutlet var tableview : UITableView!
    var offerListResul = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "OfferPromotionCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "OfferPromotionCell")
//        self.tableview.estimatedRowHeight = 274
        self.GetOffers()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Notification"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.offerListResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.offerListResul.count > indexPath.row {
            if let data = self.offerListResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "OfferPromotionCell") as? OfferPromotionCell {
                    cell.delegate = self
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? OfferPromotionCell {
            let controller = OfferDetailsViewController(nibName: "OfferDetailsViewController", bundle: nil)
            controller.resultData = cell.resultData
            self.navigationController?.pushViewController(controller, animated: true)
        }
//
    }
    
    
    func GetOffers(){
        
        let urlStr = String(format: "%@?view=notification", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let resultList = result.object(forKey: "offer_list") as? NSArray {
                                self.offerListResul = NSMutableArray(array: resultList)
                                DispatchQueue.main.async
                                    {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.ReloadWithAnimation()
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }

    func BuyButtonTapped(data: NSDictionary) {
        var productId = ""
        if let tempStr = data.object(forKey: "product") as? String {
            productId = tempStr
        }
        let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
        objRoot.productResult = ["productID":productId]
        self.navigationController?.pushViewController(objRoot, animated: true)
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
