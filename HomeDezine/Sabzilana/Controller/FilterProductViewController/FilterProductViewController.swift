//
//  FilterProductViewController.swift
//  TrendOnTap
//
//  Created by Desap Team on 15/02/2018.
//  Copyright © 2018 com.technetapp.trendontap. All rights reserved.
//

import UIKit

protocol FilterProductDelegate {
    func applyProductFilter(filterIds : String)
}
class FilterProductViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,FilterMenuViewDelegate {

    var delegate : FilterProductDelegate?
    
    @IBOutlet var tableview : UITableView?
    @IBOutlet var leftView : UIView?
    var filterList = NSArray()
    
    let letfMenu = FilterMenuView.instanceFromNib()
    
    var selectedFilterIds = NSMutableArray()
    
    
    var categoryId : String = "" {
        didSet{
            GetFilterList()
        }
        
    }
    @IBAction func clearClick(){
        self.selectedFilterIds.removeAllObjects()
        self.tableview?.reloadData()
    }
    @IBAction func applyClick(){
        if let items = (self.selectedFilterIds as NSArray?) as? [String] {
            print(items)
            print(items.joined(separator: ","))
            let filterStr = items.joined(separator: ",")
            if self.delegate != nil {
                self.delegate?.applyProductFilter(filterIds: filterStr)
            }
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "FILTER BY"
        if self.leftView != nil {
            
            self.letfMenu.frame = (leftView?.bounds)!
            self.letfMenu.delegate = self
            self.leftView?.addSubview(letfMenu)
        }
        
//        self.GetFilterList()
    }
    override func viewWillAppear(_ animated: Bool) {
        
//        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        
        let closeButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_white_close"), landscapeImagePhone: #imageLiteral(resourceName: "ic_white_close"), style: .plain, target: self, action: #selector(closeClick))
        
//        self.navigationItem.rightBarButtonItems = [closeButton,homeButton]
        self.navigationItem.rightBarButtonItem = closeButton
        
        self.navigationItem.hidesBackButton = true
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func closeClick(){
        self.navigationController?.popViewController(animated: true)
    }
    func UpdateUI(){
        if self.tableview != nil {
            let nibName1 = UINib(nibName: "FilterDetailsCell", bundle: nil)
            self.tableview?.register(nibName1, forCellReuseIdentifier: "FilterDetailsCell")
            self.tableview?.backgroundColor = UIColor.groupTableViewBackground
            
            self.tableview?.dataSource = self
            self.tableview?.delegate = self
            self.tableview?.reloadData()
        }else{
            self.perform(#selector(UpdateUI))
        }
    }
    
    //MARK:  -  Table View DataSource -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.filterList.count > indexPath.row {
            if let data = self.filterList.object(at: indexPath.row) as? NSDictionary {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "FilterDetailsCell") as? FilterDetailsCell {
                    cell.jsonData = data
                    if let tempStr = data.object(forKey: "name") as? String {
                        cell.descLabel?.text = tempStr
                    }
                    if let productId = data.object(forKey: "id") as? String {
                        if self.selectedFilterIds.contains(productId) == true {
                            cell.btnCheckBox?.image = #imageLiteral(resourceName: "checked-filter")
                        }else{
                            cell.btnCheckBox?.image = #imageLiteral(resourceName: "unchecked-filter")
                        }
                    }
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? FilterDetailsCell {
            if let productId = cell.jsonData.object(forKey: "id") as? String {
                //Check already selected
//                var isFound : Bool = false
//                for (index, idStr) in self.selectedFilterIds.enumerated() {
//                    if let tempId = idStr as? String {
//                        if tempId == productId {
//                            isFound = true
//                            self.selectedFilterIds.removeObject(at: index)
//
//                            self.tableview?.reloadData()
//                            break
//                        }
//                    }
//                }
                
                if self.selectedFilterIds.contains(productId) == true {
                    self.selectedFilterIds.remove(productId)
                }else{
                    self.selectedFilterIds.add(productId)
                }
                self.tableview?.reloadData()
            }
        }
//
        
    }
    
    func GetFilterList(){
        
        //Use alamofire api
        self.GetFilterResult()
        return
        
        //remove manual api integration
        
        let urlStr = String(format: "%@?view=category_filter&categoryID=\(self.categoryId)", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                
                //New Product
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let inquiry = result.object(forKey: "filter") as? NSArray {

                                DispatchQueue.main.async {
                                    self.letfMenu.frame = (self.leftView?.bounds)!
                                    self.letfMenu.filterList = inquiry
                                    if inquiry.count > 0 {
                                        if let filterData = inquiry.object(at: 0) as? NSDictionary {
                                            if let currentList = filterData.object(forKey: "detail_data") as? NSArray {
                                                self.filterList = NSArray(array: currentList)
                                                self.UpdateUI()
                                            }
                                        }
                                    }
//
                                    
                                    
                                }
                                
                            }
                            //
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
                
            }
            
            
        }
        
    }

    func GetFilterResult(){
        
        let urlStr = String(format: "%@?view=category_filter&categoryID=\(self.categoryId)", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        DispatchQueue.main.async {
            MOHUD.showWithStatus("Loading")
        }
        
        request(urlStr, headers: nil).responseJSON { response in
            DispatchQueue.main.async {
                MOHUD.dismiss()
            }
            
             if let result = response.result.value as? NSDictionary {
                if let statusCode = result.object(forKey: kStatusCode) as? String {
                    if statusCode == "0" {
                        if let inquiry = result.object(forKey: "filter") as? NSArray {
                            
                            DispatchQueue.main.async {
                                self.letfMenu.frame = (self.leftView?.bounds)!
                                self.letfMenu.filterList = inquiry
                                if inquiry.count > 0 {
                                    if let filterData = inquiry.object(at: 0) as? NSDictionary {
                                        if let currentList = filterData.object(forKey: "detail_data") as? NSArray {
                                            self.filterList = NSArray(array: currentList)
                                            self.UpdateUI()
                                        }
                                    }
                                }
                                //
                                
                                
                            }
                            
                        }
                        //
                    }else{
                        if let message = result.object(forKey: kMessage) as? String {
                            ShowMessage("", message: message)
                        }
                        
                    }
                }

            }
            
        }
    }
    //MARK:- Delegate -
    func menuDidSelectOption(data: NSDictionary) {
        print(data)
        if let currentList = data.object(forKey: "detail_data") as? NSArray {
            self.filterList = NSArray(array: currentList)
            self.tableview?.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
