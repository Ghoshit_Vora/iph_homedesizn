//
//  FilterMenuView.swift
//  TrendOnTap
//
//  Created by Desap Team on 15/02/2018.
//  Copyright © 2018 com.technetapp.trendontap. All rights reserved.
//

import UIKit

protocol FilterMenuViewDelegate {
    func menuDidSelectOption(data : NSDictionary)
}
class FilterMenuView: UIView, UITableViewDataSource, UITableViewDelegate {

    var delegate : FilterMenuViewDelegate?
    
    @IBOutlet var tableview : UITableView?
    var filterList = NSArray() {
        didSet{
            UpdateUI()
        }
    }
    class func instanceFromNib() -> FilterMenuView {
        return UINib(nibName: "FilterMenuView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FilterMenuView
    }
    
    override func awakeFromNib() {
        
    }
    
    func UpdateUI(){
        if self.tableview != nil {
            let nibName1 = UINib(nibName: "FilterMenuCell", bundle: nil)
            self.tableview?.register(nibName1, forCellReuseIdentifier: "FilterMenuCell")
            
            self.tableview?.dataSource = self
            self.tableview?.delegate = self
            self.tableview?.reloadData()
        }else{
            self.perform(#selector(UpdateUI))
        }
    }
    //MARK:  -  Table View DataSource -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.filterList.count > indexPath.row {
            if let data = self.filterList.object(at: indexPath.row) as? NSDictionary {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "FilterMenuCell") as? FilterMenuCell {
                    cell.jsonData = data
                    if let tempStr = data.object(forKey: "filter_name") as? String {
                        cell.descLabel?.text = tempStr
                    }
                    if indexPath.row == 0 {
                        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    }
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        
        if let cell = tableView.cellForRow(at: indexPath) as? FilterMenuCell {
            
            if self.delegate != nil {
                self.delegate?.menuDidSelectOption(data: cell.jsonData)
            }
        }
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
