//
//  OrderHistoryCell.swift
//  Sabzilana
//
//  Created by Jeevan on 07/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol OrderHistoryDelegate {
    func GetOrderDetail(data : NSDictionary)
}
class OrderHistoryCell: UITableViewCell {

    var delegate : OrderHistoryDelegate?
    
    @IBOutlet var orderNoLabel : UILabel!
    @IBOutlet var statusLabel : UILabel!
    @IBOutlet var dateLabel : UILabel!
    @IBOutlet var amountLabel : UILabel!
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        
        if self.orderNoLabel != nil  {
            
            if let tempStr = resultData.object(forKey: "orderNo") as? String {
                self.orderNoLabel.text = String(format: "ORDER #%@", arguments: [tempStr])
            }
            if let tempStr = resultData.object(forKey: "date") as? String {
                self.dateLabel.text = tempStr
            }
            
            if let tempStr = resultData.object(forKey: "amount") as? String {
                self.amountLabel.text = String(format: "₹ %@", arguments: [tempStr])
            }
            if let tempStr = resultData.object(forKey: "status") as? String {
                self.statusLabel.text = tempStr
                if tempStr.caseInsensitiveCompare("Delivered") == .orderedSame {
                    self.statusLabel.textColor = NSTheme().GetSabzilanaGreenColor()
                }else{
                    self.statusLabel.textColor = NSTheme().GetSabzilanaRedColor()
                }
            }
            
        }else{
            self.perform(#selector(WalletHistoryCell.SetResultUI))
        }
        
    }

    @IBAction func OrderDetailClick(){
        if self.delegate != nil {
            self.delegate?.GetOrderDetail(data: self.resultData)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
