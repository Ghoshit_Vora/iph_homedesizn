//
//  OrderHistoryViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 06/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OrderHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, OrderHistoryDelegate {

    @IBOutlet var tableview : UITableView!
    var orderResul = NSMutableArray()
    
    var currentPageCode : Int = 0
    var loadingStatus = LoadMoreStatus.haveMore
    @IBOutlet var footerView : UIView!
    
    func loadMore() {
        if loadingStatus != .finished {
            self.currentPageCode = self.currentPageCode + 1
            self.GetOrderHistory(pageCode: self.currentPageCode)
            
        }else{
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "OrderHistoryCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "OrderHistoryCell")
        //        self.tableview.estimatedRowHeight = 274
        self.tableview.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        self.GetOrderHistory(pageCode: self.currentPageCode)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Order History"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.orderResul.count > indexPath.row {
            if let data = self.orderResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as? OrderHistoryCell {
                    cell.delegate = self
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if loadingStatus == .haveMore {
            return self.footerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if loadingStatus == .haveMore {
            return 60.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int){
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            self.perform(#selector(OrderHistoryViewController.loadMore), with: nil, afterDelay: 0.5)
        }
    }
    
    func GetOrderHistory(pageCode : Int){
        
        
        let urlStr = String(format: "%@?view=orders&page=list&userID=%@&pagecode=%d", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,pageCode])
        print(urlStr)
        
        if pageCode <= 0 {
            self.orderResul = NSMutableArray()
        }
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            
                            if let resultList = result.object(forKey: "order_list") as? NSArray {
                                
                                for tempData in resultList {
                                    self.orderResul.add(tempData)
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                        }else{
                                            self.tableview.reloadData()
                                        }
                                }
                                
                                self.loadingStatus = .haveMore
                                
                            }else{
                                self.loadingStatus = .finished
                                DispatchQueue.main.async
                                    {
                                        self.tableview.reloadData()
                                        
                                }
                            }
                        }else{
                            self.loadingStatus = .finished
                            DispatchQueue.main.async
                                {
                                    self.tableview.reloadData()
                                    
                            }
                        }
                    }
                    
                    
                }
            }
        }
        
    }

    //MARK:- Cell Delegate - 
    func GetOrderDetail(data: NSDictionary) {
        print(data)
        let objRoot = OrderDetailsViewController(nibName: "OrderDetailsViewController", bundle: nil)
        objRoot.responseData = data
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
