//
//  OrderDetailsViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/7/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {

    @IBOutlet var deliveryDateLabel : UILabel!
    @IBOutlet var orderDateLabel : UILabel!
    @IBOutlet var orderStatusLabel : UILabel!
    @IBOutlet var orderAmount : UILabel!
    @IBOutlet var paymentMethodLabel : UILabel!
    @IBOutlet var payValueLabel : UILabel!
    @IBOutlet var walletValueLabel : UILabel!
    @IBOutlet var qtyLabel : UILabel!
    @IBOutlet var addressLabel : UILabel!
    
    var resultData = NSDictionary()
    var responseData = NSDictionary(){
        didSet{
            self.GetOrderDetails()
        }
    }
    
    @IBAction func itemClick(){
        let objRoot = OrderItemListViewController(nibName: "OrderItemListViewController", bundle: nil)
        objRoot.responseData = self.resultData
        var navigationTitle = ""
        if let tempStr = self.responseData.object(forKey: "orderID") as? String {
            navigationTitle = String(format: "Order Status - %@", tempStr)
        }else{
            navigationTitle = "Order Items"
        }
        objRoot.navigationTitleStr = navigationTitle
        self.navigationController?.pushViewController(objRoot, animated: true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if let tempStr = self.responseData.object(forKey: "orderID") as? String {
            self.navigationItem.title = String(format: "Order Status - %@", tempStr)
        }else{
            self.navigationItem.title = "Order Status"
        }
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func GetOrderDetails(){
        var orderId = "0"
        if let tempStr = self.responseData.object(forKey: "orderID") as? String {
            orderId = tempStr
        }
        
        let urlStr = String(format: "%@?view=orders&page=detail&userID=%@&orderID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,orderId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.resultData = NSDictionary(dictionary: result)
                            DispatchQueue.main.async
                                {
                                  self.SetOrderDetails()
                            }
                        }
                    }
                }
            }
        }
        
    }
    func SetOrderDetails(){
        if let tempStr = self.resultData.object(forKey: "del_date") as? String {
            if tempStr.isEmpty == true {
                self.deliveryDateLabel.text = ""
//                if let dateStr = self.resultData.object(forKey: "order_date") as? String {
//                    self.deliveryDateLabel.text = dateStr
//                }
            }else{
                self.deliveryDateLabel.text = tempStr
            }
            
        }
        if let tempStr = self.resultData.object(forKey: "order_date") as? String {
            self.orderDateLabel.text = tempStr
        }
        if let tempStr = self.resultData.object(forKey: "status") as? String {
            self.orderStatusLabel.text = tempStr
            if tempStr.caseInsensitiveCompare("Delivered") == .orderedSame {
                self.orderStatusLabel.textColor = NSTheme().GetSabzilanaGreenColor()
            }else{
                self.orderStatusLabel.textColor = NSTheme().GetSabzilanaRedColor()
            }
        }
        if let tempStr = self.resultData.object(forKey: "grand_total") as? String {
            self.orderAmount.text = String(format: "₹ %@", arguments: [tempStr])
        }
        if let tempStr = self.resultData.object(forKey: "order_type") as? String {
            self.paymentMethodLabel.text = tempStr
        }
        if let tempStr = self.resultData.object(forKey: "pay_value") as? String {
            self.payValueLabel.text = String(format: "₹ %@", arguments: [tempStr])
        }
        if let tempStr = self.resultData.object(forKey: "wallet") as? String {
            self.walletValueLabel.text = String(format: "₹ %@", arguments: [tempStr])
        }
        if let tempStr = self.resultData.object(forKey: "item") as? String {
            self.qtyLabel.text = tempStr
        }
        if let tempStr = self.resultData.object(forKey: "address") as? String {
            self.addressLabel.text = tempStr
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
