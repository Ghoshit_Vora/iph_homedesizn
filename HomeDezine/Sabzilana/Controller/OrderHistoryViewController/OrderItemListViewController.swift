//
//  OrderItemListViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/7/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OrderItemListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var lblShipping : UILabel!
    @IBOutlet var lblSubTotal : UILabel!
    @IBOutlet var lblGrandTotal : UILabel!
    @IBOutlet var footerView : UIView!
    
    @IBOutlet var tableview : UITableView!
    var itemsResul = NSArray()
    var navigationTitleStr : String = ""
    var responseData = NSDictionary(){
        didSet{
            self.SetItemsResult()
        }
    }
    func SetItemsResult(){
        
        if self.tableview != nil {
            if let result = self.responseData.object(forKey: "products_list") as? NSArray {
                self.itemsResul = NSArray(array: result)
            }
            
            DispatchQueue.main.async
                {
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.ReloadWithAnimation()
            }
            
            //Set SubTotal And Grand Total
            if let tempStr = self.responseData.object(forKey: "subtotal") as? String {
                self.lblSubTotal.text = String(format: "₹ %@", arguments: [tempStr])
            }
            if let tempStr = self.responseData.object(forKey: "grand_total") as? String {
                self.lblGrandTotal.text = String(format: "₹ %@", arguments: [tempStr])
            }
            if let tempStr = self.responseData.object(forKey: "shipping") as? String {
                self.lblShipping.text = String(format: "₹ %@", arguments: [tempStr])
            }
            

        }else {
            self.perform(#selector(OrderItemListViewController.SetItemsResult), with: nil, afterDelay: 0.1)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "OrderDetailsCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "OrderDetailsCell")
        
        DispatchQueue.main.async
            {
                self.tableview.tableFooterView = self.footerView
                
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.title = self.navigationTitleStr
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.itemsResul.count > indexPath.row {
            if let data = self.itemsResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "OrderDetailsCell") as? OrderDetailsCell {
                    
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
