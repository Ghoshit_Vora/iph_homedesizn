//
//  ReferFriendsViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/6/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ReferFriendsViewController: UIViewController {

    @IBOutlet var imageview : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var descLabel : UILabel!
    @IBOutlet var codeLabel : UILabel!
    var ReferResult = NSDictionary()
    
    @IBAction func shareClick(){
        var image : UIImage?
        
        if let tempImage = self.imageview.image {
            image = tempImage
        }
        
        var shareText = ""
        if let tempStr = self.ReferResult.object(forKey: "message") as? String {
            shareText = tempStr
        }
        
        let shareResult = NSMutableArray()
        if image != nil {
            shareResult.add(image ?? #imageLiteral(resourceName: "ic_logo"))
        }
        shareResult.add(shareText)
        
        appDelegate.ShareAppObject(shareResult)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.GetRefer()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Refer"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
//        self.navigationItem.rightBarButtonItems = [homeButton]
        
        let shareButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(self.shareClick))
        self.navigationItem.rightBarButtonItems = [shareButton,homeButton]
    }
    
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func GetRefer(){
        
        let urlStr = String(format: "%@?view=refer&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            
                            if let resultList = result.object(forKey: "refer") as? NSArray {
                                print(resultList)
                                if resultList.count > 0 {
                                    if let data = resultList.object(at: 0) as? NSDictionary {
                                        self.ReferResult = NSDictionary(dictionary: data)
                                        
                                        DispatchQueue.main.async
                                            {
                                                self.SetRefresResult()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func SetRefresResult(){
//        var titleText = ""
//        if let tempStr = self.ReferResult.object(forKey: "you_get") as? String {
//            titleText = tempStr
//        }
//        if let tempStr = self.ReferResult.object(forKey: "you_friend_get") as? String {
//            titleText = titleText.appendingFormat(" And %@", tempStr)
//        }
//        self.titleLabel.text = titleText
//        
//        if let tempStr = self.ReferResult.object(forKey: "message") as? String {
//           self.descLabel.text = tempStr
//        }
//        if let tempStr = self.ReferResult.object(forKey: "link") as? String {
//            self.codeLabel.text = tempStr
//        }
        if let tempStr = self.ReferResult.object(forKey: "share_image") as? String {
            if tempStr.isEmpty == false {
                let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                    if image != nil {
                        self.imageview.image = image
                    }
                })
                
            }
        }
        
    }

    @IBAction func copyCodeClick(){
        
        if let tempStr = self.ReferResult.object(forKey: "link") as? String {
            UIPasteboard.general.string = tempStr
            self.view.makeToast("Download link copied!")
        }
        
//        if self.codeLabel.text?.isEmpty == false {
//            if let tempStr = self.codeLabel.text {
//                UIPasteboard.general.string = tempStr
//            }
//
//        }
        
    }
    
    /* Make Toast Method
     Basic Examples
     
     // basic usage
     [self.view makeToast:@"This is a piece of toast."];
     
     // toast with a specific duration and position
     [self.view makeToast:@"This is a piece of toast with a specific duration and position."
     duration:3.0
     position:CSToastPositionTop];
     
     // toast with all possible options
     [self.view makeToast:@"This is a piece of toast with a title & image"
     duration:3.0
     position:[NSValue valueWithCGPoint:CGPointMake(110, 110)]
     title:@"Toast Title"
     image:[UIImage imageNamed:@"toast.png"]
     style:nil
     completion:^(BOOL didTap) {
     if (didTap) {
     NSLog(@"completion from tap");
     } else {
     NSLog(@"completion without tap");
     }
     }];
     
     // display toast with an activity spinner
     [self.view makeToastActivity:CSToastPositionCenter];
     
     // display any view as toast
     [self.view showToast:myView];
     But wait, there's more!
     
     // create a new style
     CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
     
     // this is just one of many style options
     style.messageColor = [UIColor orangeColor];
     
     // present the toast with the new style
     [self.view makeToast:@"This is a piece of toast."
     duration:3.0
     position:CSToastPositionBottom
     style:style];
     
     // or perhaps you want to use this style for all toasts going forward?
     // just set the shared style and there's no need to provide the style again
     [CSToastManager setSharedStyle:style];
     
     // toggle "tap to dismiss" functionality
     [CSToastManager setTapToDismissEnabled:YES];
     
     // toggle queueing behavior
     [CSToastManager setQueueEnabled:YES];
     */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
