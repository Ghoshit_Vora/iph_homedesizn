//
//  CategorySectionHeader.swift
//  TrendOnTap
//
//  Created by Desap Team on 30/01/2018.
//  Copyright © 2018 com.technetapp.trendontap. All rights reserved.
//

import UIKit

protocol CategorySectionHeaderDelegate {
    func didPressSection(header : CategorySectionHeader)
}

class CategorySectionHeader: UIView {

    var delegate : CategorySectionHeaderDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    
    @IBOutlet var expandButton : UIButton?
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    class func instanceFromNib() -> CategorySectionHeader  {
        return UINib(nibName: "CategorySectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CategorySectionHeader
        
    }
    func SetResultUI(){
        
        if self.imageview != nil  {
            if let tempStr = resultData.object(forKey: "icon") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.titleLabel.text = tempStr
            }
            
            if let isExpand = resultData.object(forKey: "isExpand") as? String {
                if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                    self.expandButton?.setImage(#imageLiteral(resourceName: "minus-symbol"), for: .normal)
                }else{
                    self.expandButton?.setImage(#imageLiteral(resourceName: "plus-button"), for: .normal)
                }
            }
            
        }else{
            self.perform(#selector(CategorySectionHeader.SetResultUI))
        }
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func touchButtonAction(){
        if self.delegate != nil {
            self.delegate?.didPressSection(header: self)
        }
       
    }
    override func awakeFromNib() {
        
    }
}
