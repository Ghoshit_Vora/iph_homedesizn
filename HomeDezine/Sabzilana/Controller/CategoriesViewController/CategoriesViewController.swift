//
//  CategoriesViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/5/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,CategorySectionHeaderDelegate,CategoriesListCellDelegate {

    @IBOutlet var tableview : UITableView!
    var CategoriesListResul = NSMutableArray()
    
    var homeCategoryResult = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let nibName = UINib(nibName: "CategoriesListCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "CategoriesListCell")
        
        self.GetProductCategories()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Categories"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK:- Tableview Delegate -
    //MARK:  -  Table View DataSource -
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.CategoriesListResul.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = CategorySectionHeader.instanceFromNib()
        header.delegate = self
        if self.CategoriesListResul.count > section {
            if let data = self.CategoriesListResul.object(at: section) as? NSDictionary {
                header.resultData = data
            }
        }
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.CategoriesListResul.count > section {
            if let data = self.CategoriesListResul.object(at: section) as? NSDictionary {
                if let isExpand = data.object(forKey: "isExpand") as? String {
                    //ExpandCell
                    if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                        if let listOfOrder = data.object(forKey: "subcat_list") as? NSArray {
                            
                            let count = Float(listOfOrder.count) / Float(2)
                            return Int(ceil(Double(count)))
                        }
                    }
                }
                
            }
        }
        return  0
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.CategoriesListResul.count > indexPath.section {
            if let data = self.CategoriesListResul.object(at: indexPath.section) as? NSDictionary {
                if let listOfOrder = data.object(forKey: "subcat_list") as? NSArray {
                    
                   var plusIndex = indexPath.row + indexPath.row
                    if plusIndex == 1{
                        plusIndex = 0
                    }
                    if listOfOrder.count > indexPath.row {
                        if let data = listOfOrder.object(at: plusIndex) as? NSDictionary {
                            if let cell = self.tableview.dequeueReusableCell(withIdentifier: "CategoriesListCell") as? CategoriesListCell {
                                cell.delegate = self
                                cell.resultData = data
                                
                                let secondLavelIndex = plusIndex + 1
                                if listOfOrder.count > secondLavelIndex {
                                    if let data2 = listOfOrder.object(at: secondLavelIndex) as? NSDictionary {
                                        cell.resultData2 = data2
                                    }
                                    cell.view2?.isHidden = false
                                }else {
                                    cell.view2?.isHidden = true
                                }
                                
                                return cell
                            }
                        }
                        
                    }
                }
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        if let cell = tableView.cellForRow(at: indexPath) as? CategoriesListCell {
//
//            let objRoot = ProductViewController(nibName: "ProductViewController", bundle: nil)
//            objRoot.categoryResultData = cell.resultData
//            self.navigationController?.pushViewController(objRoot, animated: true)
//        }
    }

    
    func GetProductCategories(){
        
        let urlStr = String(format: "%@?view=category&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            if let resultList = result.object(forKey: "category_list_new") as? NSArray {
                                
                                DispatchQueue.main.async
                                    {
//                                        self.CategoriesListResul = NSMutableArray(array: resultList)
                                        self.CategoriesListResul = NSMutableArray()
                                        for modify in resultList {
                                            if let modifyData = modify as? NSDictionary {
                                                let mutableData = NSMutableDictionary(dictionary: modifyData)
                                                mutableData.setObject("false", forKey: "isExpand" as NSCopying)
                                                if let catId = self.homeCategoryResult.object(forKey: "catID") as? String {
                                                    if let liveCatId = modifyData.object(forKey: "catID") as? String {
                                                        if catId == liveCatId {
                                                            mutableData.setObject("true", forKey: "isExpand" as NSCopying)
                                                        }
                                                    }
                                                }
                                                
                                                self.CategoriesListResul.add(mutableData)
                                            }
                                        }
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.ReloadWithAnimation()
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    func CollsPanAllCells(){
        
        for (index, element) in self.CategoriesListResul.enumerated() {
            if let orderData = element as? NSDictionary {
                if let isExpand = orderData.object(forKey: "isExpand") as? String {
                    
                    //ExpandCell
                    if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                        let mutableOrderData = NSMutableDictionary(dictionary: orderData)
                        mutableOrderData.setValue("false", forKey: "isExpand")
                        
                        self.CategoriesListResul.replaceObject(at: index, with: mutableOrderData)
                        
                    }
                }
                
                
            }
        }
    }
    //MARK:- ExpandCell Delegate -
    func didSelectCategoryClick(data: NSDictionary) {
        let objRoot = ProductViewController(nibName: "ProductViewController", bundle: nil)
        objRoot.categoryResultData = data
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    func didPressSection(header: CategorySectionHeader) {
        if let categoryID = header.resultData.object(forKey: "catID") as? String {
            if let isExpand = header.resultData.object(forKey: "isExpand") as? String {
                if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                    header.expandButton?.setImage(#imageLiteral(resourceName: "plus-button"), for: .normal)
                    self.CollsPanAllCells()
                }else{
                    self.CollsPanAllCells()
                    let mutableOrderData = NSMutableDictionary(dictionary: header.resultData)
                    mutableOrderData.setValue("true", forKey: "isExpand")
                    header.expandButton?.setImage(#imageLiteral(resourceName: "minus-symbol"), for: .normal)
                    //Expand Cell
                    for (index, element) in self.CategoriesListResul.enumerated() {
                        if let orderData = element as? NSDictionary {
                            if let catId = orderData.object(forKey: "catID") as? String {
                                
                                //ExpandCell
                                if catId.caseInsensitiveCompare(categoryID) == .orderedSame {
                                    
                                    self.CategoriesListResul.replaceObject(at: index, with: mutableOrderData)
                                    
                                }
                            }
                            
                        }
                    }
                }
            }
        }
        
        
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
