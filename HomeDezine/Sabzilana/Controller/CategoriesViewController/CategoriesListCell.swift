//
//  CategoriesListCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/5/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol CategoriesListCellDelegate {
    func didSelectCategoryClick(data : NSDictionary)
}
class CategoriesListCell: UITableViewCell {

    var delegate : CategoriesListCellDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    
    @IBOutlet var imageview2 : UIImageView!
    @IBOutlet var titleLabel2 : UILabel!
    
    @IBOutlet var view1 : UIView?
    @IBOutlet var view2 : UIView?
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    var resultData2 = NSDictionary() {
        didSet{
            self.SetResultUI2()
        }
    }
    
    @IBAction func firstCatClick(){
        if self.delegate != nil {
            self.delegate?.didSelectCategoryClick(data: self.resultData)
        }
    }
    @IBAction func secondCatClick(){
        if self.delegate != nil {
            self.delegate?.didSelectCategoryClick(data: self.resultData2)
        }
    }
    
    func SetResultUI(){
        
        if self.imageview != nil  {
            if let tempStr = resultData.object(forKey: "icon") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.titleLabel.text = tempStr
            }
            
        }else{
            self.perform(#selector(ProductListCell.SetResultUI))
        }
        
    }
    func SetResultUI2(){
        
        if self.imageview2 != nil  {
            if let tempStr = resultData2.object(forKey: "icon") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview2.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview2.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData2.object(forKey: "name") as? String {
                self.titleLabel2.text = tempStr
            }
            
        }else{
            self.perform(#selector(ProductListCell.SetResultUI))
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
