//
//  WalletHistoryViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 06/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class WalletHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var walletBalance : String = ""
    
    @IBOutlet var balanceLabel : UILabel!
    @IBOutlet var tableview : UITableView!
    var walletResul = NSMutableArray()
    
    var currentPageCode : Int = 0
    var loadingStatus = LoadMoreStatus.haveMore
    @IBOutlet var footerView : UIView!
    
    func loadMore() {
        if loadingStatus != .finished {
            self.currentPageCode = self.currentPageCode + 1
            self.GetWallet(pageCode: self.currentPageCode)
            
        }else{
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }
    
    @IBAction func AddMoneyClick(){
        let objRoot = AddMoneyViewController(nibName: "AddMoneyViewController", bundle: nil)
        objRoot.walletAvlBalance = self.walletBalance
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        isUpdateWalletTransation = false
        
        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "WalletHistoryCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "WalletHistoryCell")
        //        self.tableview.estimatedRowHeight = 274
        self.tableview.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        self.GetWallet(pageCode: self.currentPageCode)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Wallet History"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        //Update Wallet Transaction
        if isUpdateWalletTransation == true {
            isUpdateWalletTransation = false
            self.currentPageCode = 0
            self.GetWallet(pageCode: self.currentPageCode)
        }
        
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.walletResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.walletResul.count > indexPath.row {
            if let data = self.walletResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "WalletHistoryCell") as? WalletHistoryCell {
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if loadingStatus == .haveMore {
            return self.footerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if loadingStatus == .haveMore {
            return 60.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int){
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            self.perform(#selector(WalletHistoryViewController.loadMore), with: nil, afterDelay: 0.5)
        }
    }
    
    func GetWallet(pageCode : Int){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=wallet_transction&userID=%@&userPhone=%@&pagecode=%d", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,pageCode])
        print(urlStr)
        
        if pageCode <= 0 {
            self.walletResul = NSMutableArray()
        }
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                   
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                             //Set Balance
                            if let tempStr = result.object(forKey: "walletBal") as? String {
                                DispatchQueue.main.async
                                    {
                                        self.walletBalance = tempStr
                                        if tempStr.caseInsensitiveCompare("N/A") == .orderedSame {
                                            self.balanceLabel.text = tempStr
                                        }else{
                                            self.balanceLabel.text = String(format: "₹ %@", arguments: [tempStr])
                                            
                                        }
                                        
                                }
                                
                            }
                            
                            if let resultList = result.object(forKey: "transction_data") as? NSArray {
                                //                        self.ProductListResul = NSMutableArray(array: resultList)
                                
                                for tempData in resultList {
                                    self.walletResul.add(tempData)
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                        }else{
                                            self.tableview.reloadData()
                                        }
                                }
                                
                                self.loadingStatus = .haveMore
                                
                            }else{
                                self.loadingStatus = .finished
                                DispatchQueue.main.async
                                    {
                                        self.tableview.reloadData()
                                        
                                }
                            }
                        }else{
                            self.loadingStatus = .finished
                            DispatchQueue.main.async
                                {
                                    self.tableview.reloadData()
                                    
                            }
                        }
                    }
                    
                    
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
