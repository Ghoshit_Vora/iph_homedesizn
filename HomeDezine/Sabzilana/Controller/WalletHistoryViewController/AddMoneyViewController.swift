//
//  AddMoneyViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/7/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

var isUpdateWalletTransation : Bool = false

class AddMoneyViewController: PUUIBaseVC,BSKeyboardControlsDelegate {

    //PayU
    var defaultActivityIndicator: iOSDefaultActivityIndicator = iOSDefaultActivityIndicator()
    var webServiceResponse: PayUWebServiceResponse = PayUWebServiceResponse()
    
    var payu_order_fail : String = "Online Payment Failed_Your Wallet is not Updated"
    var payu_order_success : String = "Wallet Payment Done_Your Wallet Updated Successfully."
    
    //PayU
    
    var keyboard: BSKeyboardControls!
    
    var walletAvlBalance : String = ""
    
    @IBOutlet var walletBalance : UILabel!
    
    @IBOutlet var textFieldAmount : UITextField!
    
    @IBAction func Amount100Click(){
        self.textFieldAmount.text = "100"
    }
    @IBAction func Amount200Click(){
        self.textFieldAmount.text = "200"
    }
    @IBAction func Amount500Click(){
        self.textFieldAmount.text = "500"
    }
    @IBAction func Amount1000Click(){
        self.textFieldAmount.text = "1000"
    }
    
    @IBAction func PayUClick(){
        self.GetAddMoneyDetails(type: "payu")
    }
    @IBAction func PaytmClick(){
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        var amount = ""
        if let tempStr = self.textFieldAmount.text {
            amount = tempStr
        }
        if amount.isEmpty == true {
            return
        }
        
        //        https://www.sabzilana.com/sabzi/index.php?view=add_money&userID=Mzk0&page=add_money&userPhone=9510069162&amount=100&type=payu
        let paytmRequestUrl = String(format: "%@?view=add_money&page=add_money&userID=%@&userPhone=%@&amount=%@&type=paytm", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,amount])
        
        let objRoot = PayTMViewController(nibName: "PayTMViewController", bundle: nil)
        objRoot.paytmRequestUrl = paytmRequestUrl
        objRoot.statusTypeGateway = "wallet"
        self.navigationController?.pushViewController(objRoot, animated: true)
        
        print(paytmRequestUrl)
    }
    @IBAction func shopingClick(){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.walletBalance.text = String(format: "Wallet Balance: ₹ %@", self.walletAvlBalance)
        }
        
        self.keyboard = BSKeyboardControls(fields: [self.textFieldAmount])
        
        self.keyboard.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Add Money"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    //MARK:- Get Payment Type Details -
    func GetAddMoneyDetails(type : String){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        var amount = ""
        if let tempStr = self.textFieldAmount.text {
            amount = tempStr
        }
        if amount.isEmpty == true {
            return
        }
        
//        https://www.sabzilana.com/sabzi/index.php?view=add_money&userID=Mzk0&page=add_money&userPhone=9510069162&amount=100&type=payu
        let urlStr = String(format: "%@?view=add_money&page=add_money&userID=%@&userPhone=%@&amount=%@&type=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,amount,type])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            DispatchQueue.main.async {
                                self.CheckOutAddMoneyAPI(data: result)
                            }
                        }else {
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    //MARK:- Add Money API Called -
    func CheckOutAddMoneyAPI(data : NSDictionary){
        self.initialPayUSetup(data: data)
        self.MakePayUPayment()
    }
    
    //MARK:- PayUBiz Make Payment -
    func initialPayUSetup(data : NSDictionary) {
        defaultActivityIndicator = iOSDefaultActivityIndicator()
        paymentParam = PayUModelPaymentParams()
        
        //Field and Sucess Message
        if let tempStr = data.object(forKey: "payu_wallet_fail") as? String {
            payu_order_fail = tempStr
        }
        if let tempStr = data.object(forKey: "payu_wallet_success") as? String {
            payu_order_success = tempStr
        }
        
        var payConfigure = NSDictionary()
        
        if let tempList = data.object(forKey: "add_money_data") as? NSArray {
            if tempList.count > 0 {
                if let paymentData = tempList.object(at: 0) as? NSDictionary {
                    payConfigure = NSDictionary(dictionary: paymentData)
                }
            }
            
        }
        var amount = ""
        if let tempStr = payConfigure.object(forKey: "amount") as? String {
            amount = tempStr
        }
        if let tempStr = payConfigure.object(forKey: "amount") as? NSNumber {
            amount = tempStr.stringValue
        }
        var product_info = ""
        if let tempStr = payConfigure.object(forKey: "product_info") as? String {
            product_info = tempStr
        }
        var first_name = ""
        if let tempStr = payConfigure.object(forKey: "first_name") as? String {
            first_name = tempStr
        }
        var trans_id = ""
        if let tempStr = payConfigure.object(forKey: "trans_id") as? String {
            trans_id = tempStr
        }
        var email = ""
        if let tempStr = payConfigure.object(forKey: "email") as? String {
            email = tempStr
        }
        
        var udf1 = ""
        if let tempStr = payConfigure.object(forKey: "udf1") as? String {
            udf1 = tempStr
        }
        
        
        
        var phone = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            phone = tempStr
        }
        
        
        paymentParam.amount = amount
        paymentParam.productInfo = product_info
        paymentParam.firstName = first_name
        paymentParam.transactionID = trans_id
        paymentParam.email = email
        paymentParam.userCredentials = "ra:ra"
        paymentParam.phoneNumber = phone
        paymentParam.surl = kPayUWalletsurl
        paymentParam.furl = kPayUWalletfurl
        paymentParam.udf1 = udf1
        paymentParam.udf2 = "u2"
        paymentParam.udf3 = "u3"
        paymentParam.udf4 = "u4"
        paymentParam.udf5 = "u5"
        //    self.paymentParam.environment = ENVIRONMENT_PRODUCTION;
        self.setEnvironment(kPayUMode)
        
        
        addPaymentResponseNotofication()
    }
    
    //Make Payment
    func MakePayUPayment(){
        
        let obj = PayUDontUseThisClass()
        obj.getPayUHashes(withPaymentParam: paymentParam, merchantSalt: kPayUSalt) { (allHashes, hashString, errorMessage) in
            self.callSDKWithHashes(with: allHashes!, withError: errorMessage)
        }
        
    }
    
    func setEnvironment(_ env: String) {
        paymentParam.environment = env
        if (env == ENVIRONMENT_PRODUCTION) {
            paymentParam.key = kPayUKEY
        }
        else {
            paymentParam.key = kPayUKEY
        }
    }
    func addPaymentResponseNotofication() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(PaymentOptionViewController.responseReceived(_:)), name: NSNotification.Name(rawValue: kPUUINotiPaymentResponse), object: nil)
    }
    
    
    
    func callSDKWithHashes(with allHashes: PayUModelHashes, withError errorMessage: String?) {
        if errorMessage == nil {
            self.paymentParam.hashes = allHashes
            self.callSDK()
            
        }
        else {
            print(errorMessage ?? "No Error Found")
        }
    }
    func callSDK(){
        paymentParam.oneTapTokenDictionary = nil
        let respo = PayUWebServiceResponse()
        self.webServiceResponse.getPayUPaymentRelatedDetail(forMobileSDK: self.paymentParam) { (paymentRelatedDetails, errorMessage, extraParam) in
            
            
            respo.callVASForMobileSDK(withPaymentParam: self.paymentParam)
            //FORVAS
            let stryBrd = UIStoryboard(name: pUUIStoryBoard, bundle: nil)
            let paymentOptionVC: PUUIPaymentOptionVC = stryBrd.instantiateViewController(withIdentifier: VC_IDENTIFIER_PAYMENT_OPTION) as! PUUIPaymentOptionVC
            paymentOptionVC.paymentParam = self.paymentParam
            paymentOptionVC.paymentRelatedDetail = paymentRelatedDetails
            
            self.navigationController?.pushViewController(paymentOptionVC, animated: true)
        }
        
    }
    func responseReceived(_ notification: Notification) {
        DispatchQueue.main.async {
            self.PayUResponse(notification)
        }
    }

    func PayUResponse(_ notification: Notification){
        _ = navigationController?.popViewController(animated: false)
        
        //    self.textFieldTransactionID.text = [PUSAHelperClass getTransactionIDWithLength:15];
        let strConvertedRespone: String = "\(notification.object)"
        print("Response Received \(strConvertedRespone)")
        
        if let jsonString = notification.object as? String {
            let jsonData = jsonString.data(using: String.Encoding.utf8)
            let JsonDicationary = try? JSONSerialization.jsonObject(with: jsonData!, options: .allowFragments)
            
            if let response = JsonDicationary as? NSDictionary {
                if let status = response.object(forKey: "status") as? String {
                    print(status)
                    if status.caseInsensitiveCompare("success") == .orderedSame {
                        let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                        objRoot.paymentTitle = "PayU"
                        objRoot.paymentStatusMessage = payu_order_success
                        objRoot.statusTypeGateway = "wallet"
                        let StringArr = payu_order_success.components(separatedBy: "_")
                        if StringArr.count > 0 {
                            objRoot.paymentTitle = StringArr[0]
                        }
                        if StringArr.count > 1 {
                            objRoot.paymentStatusMessage = StringArr[1]
                        }
                        
                        objRoot.paymentStatusImage = "payment_sucess"
                        
                        self.navigationController?.pushViewController(objRoot, animated: true)
                    }else{
                        let objRoot = PaymentStatusViewController(nibName: "PaymentStatusViewController", bundle: nil)
                        objRoot.paymentTitle = "PayU"
                        objRoot.paymentStatusMessage = payu_order_fail
                        objRoot.statusTypeGateway = "wallet"
                        let StringArr = payu_order_fail.components(separatedBy: "_")
                        if StringArr.count > 0 {
                            objRoot.paymentTitle = StringArr[0]
                        }
                        if StringArr.count > 1 {
                            objRoot.paymentStatusMessage = StringArr[1]
                        }
                        
                        objRoot.paymentStatusImage = "payment_field"
                        
                        self.navigationController?.pushViewController(objRoot, animated: true)
                    }
                }
            }
            
        }

    }
    
    
    //MARK: -Update Wallet Balance-
    func UpdateWalletBalance(){
        
        isUpdateWalletTransation = true
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=wallet_transction&userID=%@&userPhone=%@&pagecode=0", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo])
        print(urlStr)
        
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            //Set Balance
                            if let tempStr = result.object(forKey: "walletBal") as? String {
                                DispatchQueue.main.async
                                    {
                                        if tempStr.caseInsensitiveCompare("N/A") == .orderedSame {
                                            self.walletBalance.text = String(format: "Wallet Balance: ₹ 0.00")
                                        }else{
                                            self.walletBalance.text = String(format: "Wallet Balance: ₹ %@", tempStr)
                                            
                                        }
                                }
                                
                            }
                            
                            
                        }else{
                           
                        }
                    }
                    
                }
            }
        }
        
    }

    
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
