//
//  WalletHistoryCell.swift
//  Sabzilana
//
//  Created by Jeevan on 06/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class WalletHistoryCell: UITableViewCell {

    @IBOutlet var dateLabel : UILabel!
    @IBOutlet var detailLabel : UILabel!
    @IBOutlet var amountLabel : UILabel!
    @IBOutlet var dotView : UIView!
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        
        if self.dateLabel != nil  {
            
            if let tempStr = resultData.object(forKey: "TransactionDate") as? String {
                self.dateLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "Remark") as? String {
                self.detailLabel.text = tempStr
            }
            var symbol = ""
            if let tempStr = resultData.object(forKey: "symbol") as? String {
                symbol = tempStr
            }
            if let tempStr = resultData.object(forKey: "Amount") as? String {
                self.amountLabel.text = String(format: "%@ ₹ %@", arguments: [symbol,tempStr])
            }
            if let tempStr = resultData.object(forKey: "type") as? String {
                if tempStr.caseInsensitiveCompare("blue") == .orderedSame {
                    dotView.backgroundColor = UIColor(red: 40/255, green: 147/255, blue: 243/255, alpha: 1.0)
                }else{
                    dotView.backgroundColor = UIColor.red
                }
            }
            
        }else{
            self.perform(#selector(WalletHistoryCell.SetResultUI))
        }
        
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
