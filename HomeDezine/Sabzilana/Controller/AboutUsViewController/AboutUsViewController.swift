//
//  AboutUsViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 05/04/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet var descLabel : UILabel!
    @IBOutlet var imageview : UIImageView!
    var aboutResult = NSDictionary()
//    @IBOutlet var tableview : UITableView!
//    @IBOutlet var headerView : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//            self.tableview.tableHeaderView = self.headerView
//        })
        
        
        self.descLabel.text = ""
        self.perform(#selector(AboutUsViewController.GetAboutUs))
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "About Us"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func facebookClick(){
        if let tempStr = self.aboutResult.object(forKey: "facebook_link") as? String {
            self.openUrl(urlStr: tempStr)
        }
    }
    @IBAction func googleClick(){
        if let tempStr = self.aboutResult.object(forKey: "google_link") as? String {
            self.openUrl(urlStr: tempStr)
        }
    }
    @IBAction func linkedInClick(){
        if let tempStr = self.aboutResult.object(forKey: "linkdin_link") as? String {
            self.openUrl(urlStr: tempStr)
        }
    }
    @IBAction func instagramClick(){
        if let tempStr = self.aboutResult.object(forKey: "insta_link") as? String {
            self.openUrl(urlStr: tempStr)
        }
    }
    @IBAction func twitterClick(){
        if let tempStr = self.aboutResult.object(forKey: "twitter_link") as? String {
            self.openUrl(urlStr: tempStr)
        }
    }

    func GetAboutUs(){
        
        let urlStr = String(format: "%@?view=about&userID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            
                            if let resultList = result.object(forKey: "about") as? NSArray {
                                print(resultList)
                                if resultList.count > 0 {
                                    if let data = resultList.object(at: 0) as? NSDictionary {
                                        self.aboutResult = NSDictionary(dictionary: data)
                                        
                                        DispatchQueue.main.async
                                            {
                                              self.SetAboutUsData()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func SetAboutUsData(){
        if let tempStr = self.aboutResult.object(forKey: "text") as? String {
            self.descLabel.attributedText = tempStr.html2AttributedString

            
        }
        if let tempStr = self.aboutResult.object(forKey: "image") as? String {
            if tempStr.isEmpty == false {
                let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                    if image != nil {
                        self.imageview.image = image
                    }
                })
                
            }
        }

    }
    func openUrl(urlStr : String){
        if urlStr.isEmpty == false {
            if let url = URL(string: urlStr) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
