//
//  RegisterViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/25/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class AddInquiryViewController: UIViewController, BSKeyboardControlsDelegate,DropDownDelegate {

    var productDetailsResult = NSDictionary() {
        didSet{
            // Delay 2 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.SetResultUI()
            }
           
            
        }
    }
    @IBOutlet var imageview : UIImageView?
    @IBOutlet var productLabel : UILabel?
    
    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    
    
    @IBOutlet var textFieldName : ACFloatingTextfield!
    @IBOutlet var textFieldEmail : ACFloatingTextfield!
    @IBOutlet var textFieldMobile : ACFloatingTextfield!
    @IBOutlet var textFieldPasswor : ACFloatingTextfield!
    @IBOutlet var textFieldState : ACFloatingTextfield!
    @IBOutlet var textFieldPincode : ACFloatingTextfield!
    @IBOutlet var textFieldMessage : ACFloatingTextfield!
    
    @IBOutlet var textFieldCity : ACFloatingTextfield!
   
    var stateList = NSArray()
    var cityList = NSArray()
    
    var keyboard: BSKeyboardControls!
    
    var stateID : String = ""
    var cityID : String = ""
    
    
    func SetResultUI(){
        
        if self.imageview != nil && self.productDetailsResult.object(forKey: "productID") != nil {
           
            
            if let tempStr = productDetailsResult.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview?.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview?.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = productDetailsResult.object(forKey: "name") as? String {
                self.productLabel?.text = tempStr
            }
            
            //Set Default Field
            let user = getLoginUser()
            if let tempStr = user.object(forKey: "name") as? String {
                self.textFieldName.text = tempStr
            }
            if let tempStr = user.object(forKey: "email") as? String {
                self.textFieldEmail.text = tempStr
            }
            if let tempStr = user.object(forKey: "phone") as? String {
                self.textFieldMobile.text = tempStr
            }
            if let tempStr = user.object(forKey: "phone") as? String {
                self.textFieldMobile.text = tempStr
            }
            if let tempStr = user.object(forKey: "state") as? String {
                self.textFieldState.text = tempStr
                if let idStr = user.object(forKey: "state_id") as? String {
                    self.stateID = idStr
                }
            }
            if let tempStr = user.object(forKey: "city") as? String {
                self.textFieldCity.text = tempStr
                if let idStr = user.object(forKey: "city_id") as? String {
                    self.cityID = idStr
                }
            }
           
            
        }else{
            self.perform(#selector(AddInquiryViewController.SetResultUI), with: nil, afterDelay: 0.1)
        }
        
    }
    
    @IBAction func StateClick(){
        self.OpenAreaPopups(isSate: true)
    }
    @IBAction func CityClick(){
        self.OpenAreaPopups(isSate: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view.
        self.tableview.tableHeaderView = self.headerView
        self.navigationController?.navigationBar.isHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [textFieldName,textFieldEmail,textFieldMobile,textFieldPasswor,textFieldPincode,textFieldMessage])
        
        keyboard.delegate = self
        
        
        
        self.GetStateList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        DispatchQueue.main.async {
            
            self.navigationController?.navigationBar.topItem?.title = "Inquiry"
            self.navigationItem.title = "Inquiry"
            self.navigationController?.navigationBar.isHidden = false
        }
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func OpenAreaPopups(isSate : Bool){
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }

        let container = AreaViewController.instance()
        container.delegate = self
        
        if isSate == true {
            container.navigationTitleString = "Choose State"
            container.responseResult = NSArray(array: self.stateList)
        }else{
            container.navigationTitleString = "Choose City"
            container.responseResult = NSArray(array: self.cityList)
        }
        
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        view.endEditing(true)
    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    @IBAction func submitClick(){
        self.InsertInquiry()
    }
    //MARK:- API CALL -
    func InsertInquiry(){
        
        
        if textFieldName.text!.isEmpty {
            ShowMessage("", message: "Please enter name")
            return
        }
       if textFieldEmail.text!.isEmpty {
            ShowMessage("", message: "Please enter email")
            return
        }
        if textFieldMobile.text!.isEmpty {
            ShowMessage("", message: "Please enter mobile number")
            return
        }
        if textFieldPasswor.text!.isEmpty {
            ShowMessage("", message: "Please enter quantity")
            return
        }
        if textFieldState.text!.isEmpty {
            ShowMessage("", message: "Please select state")
            return
        }
        if textFieldCity.text!.isEmpty {
            ShowMessage("", message: "Please select city")
            return
        }
        if textFieldPincode.text!.isEmpty {
            ShowMessage("", message: "Please enter postcode")
            return
        }
        if let pincode = textFieldPincode.text {
            if pincode.count != 6 {
                ShowMessage("", message: "Postcode must be a 6 digit number")
                return
            }
        }
        if textFieldMessage.text!.isEmpty {
            ShowMessage("", message: "Please enter message")
            return
        }
        
        self.view.endEditing(true)
        
        
        var productId = ""
        if let product = self.productDetailsResult.object(forKey: "productID") as? String {
            productId = product
        }
        
//        http://homedezin.com/mapp/index.php?view=product_inq&userID=MQ==&productID=MQ==&name=Mehul&email=mehul@thedezine.in&phone=7874806162&quantity=10&msg=Thisistest&city_name=a&state_name=a&pincode=370645
        let urlStr = String(format: "%@?view=product_inq&name=%@&email=%@&phone=%@&quantity=%@&city_name=%@&state_name=%@&msg=%@&pincode=%@&userID=%@&productID=%@&userType=%@", arguments: [kMainDomainUrl,self.textFieldName.text!, self.textFieldEmail.text!,self.textFieldMobile.text!,self.textFieldPasswor.text!,self.textFieldCity.text!,self.textFieldState.text!,self.textFieldMessage.text!,self.textFieldPincode.text!,appDelegate.SabzilanaLoginUserId,productId,kPlatformName])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
            print(data)
            
            if succeeded == true {
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            DispatchQueue.main.async {
                                let controller = InquiryFinishViewController(nibName: "InquiryFinishViewController", bundle: nil)
                                self.navigationController?.pushViewController(controller, animated: true)
                            }
                        }else{
                            if let message = result.object(forKey: kMessage) as? String {
                                ShowMessage("", message: message)
                            }
                            
                        }
                    }
                }
            }
            
        }
        
    }

    
    func GetStateList(){
        
        let urlStr = String(format: "%@?view=state_list", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "state") as? NSArray {
                                self.stateList = NSArray(array: tempSate)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    func GetCityList(stateId : String){
        
        let urlStr = String(format: "%@?view=city_list&state_id=%@", arguments: [kMainDomainUrl,stateId])
        print(urlStr)
//        http://www.homedezin.com/mapp/index.php?view=city_list&state_id=4
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "City") as? NSArray {
                                self.cityList = NSArray(array: tempSate)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    
    //MARK:- DropDown Delegate -
    func didSelectDropDownResult(data : AnyObject, screenName : String) {
         print(data)
        if let result = data as? NSDictionary {
            if screenName == "Choose State" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.stateID = tempStr
                    self.GetCityList(stateId: tempStr)
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textFieldState.text = nameStr
                    }
                    self.cityID = ""
                    self.textFieldCity.text = ""
                }
            }else if screenName == "Choose City" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.cityID = tempStr
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textFieldCity.text = nameStr
                    }
                }
            }
            
        }
    }
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        /*
        if let result = data as? NSDictionary {
            if let tempStr = result.object(forKey: "ID") as? String {
                self.areaID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textFieldArea.text = nameStr
                }
            }else if let tempStr = result.object(forKey: "pincodeID") as? String {
                self.pincodeID = tempStr
                if let nameStr = result.object(forKey: "name") as? String {
                    self.textFieldPinCode.text = nameStr
                }
            }
            
            
        }
        */
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
