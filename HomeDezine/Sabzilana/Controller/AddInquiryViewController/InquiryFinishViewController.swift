//
//  InquiryFinishViewController.swift
//  Sabzilana
//
//  Created by Desap Team on 13/02/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class InquiryFinishViewController: UIViewController {

    var message = ""
    @IBOutlet var messageLabel : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backToHomeClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        DispatchQueue.main.async {
            
            self.navigationController?.navigationBar.topItem?.title = "Success"
            self.navigationItem.title = "Success"
            self.navigationController?.navigationBar.isHidden = false
            
            if self.message.isEmpty == false {
                self.messageLabel?.text = self.message
            }
        }
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
