//
//  FullscreenImageViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 5/5/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class FullscreenImageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var tempImageView : UIImageView!
    
    @IBOutlet weak var imageView: ZoomImageView!
    @IBOutlet var collectionview : UICollectionView!
    var imageList : NSArray = NSArray() {
        didSet{
            self.LoadResult()
        }
    }
    
    var productResult = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if self.imageList.count > 0 {
            if let resultData = imageList.object(at: 0) as? NSDictionary {
                if let tempStr = resultData.object(forKey: "large_image") as? String {
                    if tempStr.isEmpty == false {
                        let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                        //let imgUrl2 = URL(string:tempStr)
                       // SDImageCache.shared().cleanDisk()
                        //SDImageCache.shared().clearMemory()
                        
                        self.tempImageView.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                            if image != nil {
                                self.imageView.zoomMode = .fit
                                self.imageView.image = image
                                
                                
                            }
                        })
                        
                    }
                }
            }
        }
        

        let nibName5 = UINib(nibName: "ImageViewCell", bundle:nil)
        self.collectionview.register(nibName5, forCellWithReuseIdentifier: "ImageViewCell")
        
        self.setNavigationTitle()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationTitle()
    }
    func  setNavigationTitle(){
        if self.navigationController != nil {
            if let tempStr = self.productResult.object(forKey: "name") as? String {
                if tempStr.isEmpty == false {
                    self.navigationItem.title = tempStr
                }else{
                    self.navigationItem.title = "Home Dezin"
                }
                
            }else{
                self.navigationItem.title = "Home Dezin"
            }
        }else {
            self.perform(#selector(FullscreenImageViewController.setNavigationTitle), with: nil, afterDelay: 0.1)
        }
        
    }
    func LoadResult(){
        if self.collectionview != nil {
            DispatchQueue.main.async
                {
                    self.collectionview.delegate = self
                    self.collectionview.dataSource = self
                    self.collectionview.ReloadWithAnimation()
                    
            }
        }else{
            self.perform(#selector(HeaderHomeCell.LoadResult), with: nil, afterDelay: 0.2)
        }
        
    }
    
    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: 100, height: 100);
        return CGSize(width: 70, height: 70);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageList.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.imageList.count > indexPath.row {
            if let result = self.imageList.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewCell", for: indexPath) as? ImageViewCell {
                    cell.resultData = result
                    
                    if let tempStr = result.object(forKey: "large_image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                            cell.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                                if image != nil {
                                    
                                    cell.imageview.image = image
                                }
                            })
                            
                        }
                    }
                    
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if let cell = collectionView.cellForItem(at: indexPath) as? ImageViewCell {
            if let tempStr = cell.resultData.object(forKey: "large_image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.tempImageView.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            
                            self.imageView.image = image
                            self.imageView.zoomMode = .fill
                        }
                    })
                    
                }
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
