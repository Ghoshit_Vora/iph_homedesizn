//
//  ChangeAddressViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/26/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

protocol UpdateProfileDelegate {
    func didGetProfileInfo()
}

class UpdateProfileViewController: UIViewController,BSKeyboardControlsDelegate,DropDownDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var infoDelegate : UpdateProfileDelegate?
    
    var delegate : ChangeAddressDelegate?
    
    var datePicker : UIDatePicker = UIDatePicker()
    
    var keyboard: BSKeyboardControls!
    @IBOutlet var tableview : UITableView!
    @IBOutlet var headerView : UIView!
    
    @IBOutlet var imageview : UIImageView!
    
    @IBOutlet var textName : ACFloatingTextfield!
    @IBOutlet var textEmail : ACFloatingTextfield!
    @IBOutlet var textMobile : ACFloatingTextfield!
    @IBOutlet var textAddress : ACFloatingTextfield!
   
    @IBOutlet var textPin : ACFloatingTextfield!
    @IBOutlet var textCity : ACFloatingTextfield!
    @IBOutlet var textState : ACFloatingTextfield!
    @IBOutlet var textDOB : ACFloatingTextfield!
    @IBOutlet var textMarrage : ACFloatingTextfield!
    
    
    var AreaNPinCodeResult = NSDictionary()
    var pincodeID : String = ""
    
    var stateList = NSArray()
    var cityList = NSArray()
    
    var stateID : String = ""
    var cityID : String = ""
    
    var ResponseResultDelivery = NSDictionary() {
        didSet{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.UpdateUI()
            }
            
        }
    }
    
    @IBAction func GetCurrentAddress(){
        
    }
    
    @IBAction func ChangeAddressClick(){
        self.ChangeUserInfo()
    }
    
    @IBAction func StateClick(){
        self.OpenAreaPopups(isSate: true)
    }
    @IBAction func CityClick(){
        
        if self.cityList.count > 0 {
            self.OpenAreaPopups(isSate: false)
        }else{
            self.GetCityList(stateId: self.stateID)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.async {
            self.tableview.tableHeaderView = self.headerView
            var frame = self.headerView.bounds
            frame.size.height = 667
            self.headerView.frame = frame
            self.tableview.tableHeaderView = self.headerView
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeAddressViewController.onKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeAddressViewController.onKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
        
        keyboard = BSKeyboardControls(fields: [textName,textAddress,textPin])
        keyboard.delegate = self
        
        
       
    }
    func UpdateUI(){
        
        if self.textName != nil {
            print(self.ResponseResultDelivery)
            var deliveryData = NSDictionary()
            if let dataList = self.ResponseResultDelivery.object(forKey: "getdetail") as? NSArray {
                if dataList.count > 0 {
                    if let tempData = dataList.object(at: 0) as? NSDictionary {
                        deliveryData = NSDictionary(dictionary: tempData)
                    }
                }
            }
            if let tempStr = deliveryData.object(forKey: "name") as? String {
                self.textName.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "email") as? String {
                self.textEmail.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "phone") as? String {
                self.textMobile.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "address") as? String {
                self.textAddress.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "city") as? String {
                self.textCity.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "city_id") as? String {
                self.cityID = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "city_id") as? NSNumber {
                self.cityID = tempStr.stringValue
            }
            
            if let tempStr = deliveryData.object(forKey: "state") as? String {
                self.textState.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "state_id") as? String {
                self.stateID = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "state_id") as? NSNumber {
                self.stateID = tempStr.stringValue
            }
//            if let tempStr = deliveryData.object(forKey: "city") as? String {
//                self.textCity.text = tempStr
//            }
            
            if let tempStr = deliveryData.object(forKey: "pincode") as? String {
                self.textPin.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "birthdate") as? String {
                self.textDOB.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "mrgdate") as? String {
                self.textMarrage.text = tempStr
            }
            if let tempStr = deliveryData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "user"))
                    
                    
                }
            }

            self.GetStateList()
        }else{
            self.perform(#selector(UpdateProfileViewController.UpdateUI), with: nil, afterDelay: 0.2)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Update Profile"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    func OpenAreaPopups(isSate : Bool){
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = AreaViewController.instance()
        container.delegate = self
        
        if isSate == true {
            container.navigationTitleString = "Choose State"
            container.responseResult = NSArray(array: self.stateList)
        }else{
            container.navigationTitleString = "Choose City"
            container.responseResult = NSArray(array: self.cityList)
        }
        
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    // MARK: - Keyboard Event -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        keyboard.activeField = textField
    }
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, inDirection direction: BSKeyboardControlsDirection) {
        self.tableview.scrollRectToVisible(field.frame, animated: true)
    }

    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.view.endEditing(true)
    }
//    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
//        self.view.endEditing(true)
//    }
    func onKeyboardHide(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let duration = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsets.zero;
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
        })
    }
    func onKeyboardShow(_ notification: Notification)
    {
        let userInfo : NSDictionary = notification.userInfo! as NSDictionary
        let kbMain  = (userInfo.object(forKey: UIKeyboardFrameEndUserInfoKey)! as AnyObject).cgRectValue
        let kbSize = kbMain?.size
        let duration  = userInfo.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as! Double;
        UIView.animate(withDuration: duration, animations: { () -> Void in
            let edgeInsets  = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
            self.tableview.contentInset = edgeInsets
            self.tableview.scrollIndicatorInsets = edgeInsets
            if (self.keyboard.activeField) != nil
            {
                self.tableview.scrollRectToVisible(self.keyboard.activeField!.frame, animated: true)
            }
        })
    }
    
    
    //MARK:- API CALL -
    
    
    func ChangeUserInfo(){
        
        if textName.text!.isEmpty {
            ShowMessage("", message: "Please enter name")
            return
        }
        else if textEmail.text!.isEmpty {
            ShowMessage("", message: "Please enter email")
            return
        }
        else if textMobile.text!.isEmpty {
            ShowMessage("", message: "Please enter mobile number")
            return
        }
        else if textAddress.text!.isEmpty {
            ShowMessage("", message: "Please enter address")
            return
        }
        else if textState.text!.isEmpty {
            ShowMessage("", message: "Please select state")
            return
        }else if textCity.text!.isEmpty {
            ShowMessage("", message: "Please select city")
            return
        }
        else if textPin.text!.isEmpty {
            ShowMessage("", message: "Please enter pincode")
            return
        }
//        else if textDOB.text!.isEmpty {
//            ShowMessage("", message: "Please enter Birth Date")
//            return
//        }else if textMarrage.text!.isEmpty {
//            ShowMessage("", message: "Please select Marriage Anniversary Date")
//            return
//        }
//        userID,page=update_info,date_of_birth,mrg_anniversary,whatsapp_no,file
        
        let requestBody = ["userID":appDelegate.SabzilanaLoginUserId,"cname":self.textName.text!,"cemail":self.textEmail.text!,"cphone":self.textMobile.text!,"cityID":self.cityID,"stateID":self.stateID,"caddress":self.textAddress.text!,"cpincode":self.textPin.text!,"cbirthdate":self.textDOB.text!,"cmrgdate":self.textMarrage.text!]
       
    
        
//    userID,cname,cemail,cphone,cityID,stateID,caddress,cpincode,cbirthdate,cmrgdate
        
        let baseUrl = String(format: "%@?view=changeinfo", arguments: [kMainDomainUrl])
        
//        print(requestBody)
        
        
        self.view.endEditing(true)
        
        var image = UIImage(named: "ic_splashbg")
        var imgData : Data?
        
        if let tempImage = self.imageview.image {
            image = tempImage
            imgData = UIImageJPEGRepresentation(image!, 0.7)
            
        }
        
//        DispatchQueue.main.async {
//            MOHUD.showWithStatus("Loading")
//
//        }

        upload(multipartFormData: { (multipartFormData) in
//            UIImagePNGRepresentation(image!)!
            
            if let imageData = imgData {
                multipartFormData.append(imageData, withName: "file", fileName: "test1.jpg", mimeType: "image/jpg")
            }
            
          
            
            for (key, value) in requestBody {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:baseUrl)
        { (result) in
            switch result {
                
                
            case .success(let upload, _, _):
                
                
                upload.responseJSON { response in
                    
//                    DispatchQueue.main.async {
//                        MOHUD.dismiss()
//                    }
                    
                    //print response.result
//                    debugPrint(response.result)
                    
                    if let result = response.result.value as? NSDictionary {
                        if let statusCode = result.object(forKey: kStatusCode) as? String {
                            if statusCode == "0" {
                                
                                
                                if self.infoDelegate != nil {
                                    self.infoDelegate?.didGetProfileInfo()
                                    DispatchQueue.main.async {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                }
//                                DispatchQueue.main.async {
//                                    if let message = result.object(forKey: kMessage) as? String {
//                                        self.ShowActionMessage(message: message)
//                                    }
//                                }
//
                                
                            }else{
                                if let message = result.object(forKey: kMessage) as? String {
                                    self.ShowActionMessage(message: message)
                                }
                                
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                
                DispatchQueue.main.async {
                    MOHUD.dismiss()
                }
                print(encodingError)
            }
        }
      
        
    }
    //MARK: - TNM Error Alert -
    func ShowActionMessage(message : String){
        
        let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        //        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
        //
        //        }))
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            DispatchQueue.main.async {
                self.RedirectToRoot()
            }
        }))
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
    func RedirectToRoot(){
        
        let viewContollers = self.navigationController?.viewControllers
        
        if let conrollerList = viewContollers {
            for controler in conrollerList {
                if let tempVC = controler as? MyProfileViewController {
                    
                    tempVC.GetUserInfo()
                    _ = navigationController?.popToViewController(tempVC, animated: true)
                    break
                }
            }
        }
        
        
    }
   
    
    //MARK:- DropDown Delegate -
    //MARK:- DropDown Delegate -
    func didSelectDropDownResult(data : AnyObject, screenName : String) {
        print(data)
        if let result = data as? NSDictionary {
            if screenName == "Choose State" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.stateID = tempStr
                    self.GetCityList(stateId: tempStr)
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textState.text = nameStr
                    }
                    self.cityID = ""
                    self.textCity.text = ""
                }
            }else if screenName == "Choose City" {
                if let tempStr = result.object(forKey: "ID") as? String {
                    self.cityID = tempStr
                    if let nameStr = result.object(forKey: "name") as? String {
                        self.textCity.text = nameStr
                    }
                }
            }
            
        }
    }
   
    //MARK:- Get City And State -
    func GetStateList(){
        
        let urlStr = String(format: "%@?view=state_list", arguments: [kMainDomainUrl])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "state") as? NSArray {
                                self.stateList = NSArray(array: tempSate)
                                self.GetCityList(stateId: self.stateID)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    func GetCityList(stateId : String){
        
        let urlStr = String(format: "%@?view=city_list&state_id=%@", arguments: [kMainDomainUrl,stateId])
        print(urlStr)
        //        http://www.homedezin.com/mapp/index.php?view=city_list&state_id=4
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        
                        if statusCode == "0" {
                            if let tempSate = result.object(forKey: "City") as? NSArray {
                                self.cityList = NSArray(array: tempSate)
                            }
                            
                        }
                    }
                }
            }
        }
        
    }
    //MARK: - DATE PICKER
    @IBAction func MarrageClick(_ sender : AnyObject){
        self.CreateDatePickerViewWithAlertController(tag: 2)
    }
    @IBAction func DOBClick(_ sender : AnyObject){
        self.CreateDatePickerViewWithAlertController(tag: 1)
    }
    func CreateDatePickerViewWithAlertController(tag : Int)
    {
        let label = UILabel(frame: CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 30))
        label.textAlignment = .center
        label.text = "Set delivery date:"
        label.font = UIFont.systemFont(ofSize: 20)
        
        let viewDatePicker: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250))
        viewDatePicker.backgroundColor = UIColor.clear
        
        
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 200))
        
        self.datePicker.tag = tag
        
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.locale = Locale(identifier: "en_GB")
        //        self.datePicker.addTarget(self, action: "datePickerSelected", forControlEvents: UIControlEvents.ValueChanged)
        self.datePicker.addTarget(self, action: #selector(DeliveryAddressViewController.handleDatePicker(_:)), for: UIControlEvents.valueChanged)
        
        viewDatePicker.addSubview(label)
        
        viewDatePicker.addSubview(self.datePicker)
        
        let alertController = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alertController.view.addSubview(viewDatePicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        { (action) in
            // ...
        }
        
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Done", style: .default)
        { (action) in
            
            self.setDate()
        }
        
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true)
        {
            // ...
        }
        
        
        
        
        
    }
    func setDate(){
        self.GetSelectedDate(date: self.datePicker.date)
    }
    func handleDatePicker(_ sender: UIDatePicker) {
        self.GetSelectedDate(date: self.datePicker.date)
        
    }
    func GetSelectedDate(date : Date) {
        let formatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        
        formatter.dateFormat = "dd-MM-yyyy"
        let dateStr = formatter.string(from: date)
        
        if self.datePicker.tag == 1 {
            self.textDOB.text = dateStr
        }else{
            self.textMarrage.text = dateStr
        }
    }

    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Photo Picker -
    @IBAction func openPicker()
    {
        self.openSheet()
    }
    
    func openSheet()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        //Create and add a second option action
        
        let option2: UIAlertAction = UIAlertAction(title: "Take Photo", style: .destructive) { action -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
                
            }
            
        }
        actionSheetController.addAction(option2)
        
        let option1: UIAlertAction = UIAlertAction(title: "Choose from Library", style: .default) { action -> Void in
            //Code for picking from camera roll goes here
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        actionSheetController.addAction(option1)
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [AnyHashable: Any]!) {
        self.imageview.image = image
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil);
        }
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil);
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
