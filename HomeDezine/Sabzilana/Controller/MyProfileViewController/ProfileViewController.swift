//
//  ProfileViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 5/16/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UpdateProfileDelegate {

    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblMobile : UILabel!
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblAddress : UILabel!
    @IBOutlet var imageview : UIImageView!
    
    var ResponseResult = NSDictionary()
    
    
    @IBAction func editProfileClick(){
        DispatchQueue.main.async {
            let objRoot = UpdateProfileViewController(nibName: "UpdateProfileViewController", bundle: nil)
            objRoot.infoDelegate = self
            objRoot.ResponseResultDelivery = NSDictionary(dictionary: self.ResponseResult)
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
        
    }

    @IBAction func signOutClick(){
        let refreshAlert = UIAlertController(title: kAppName, message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        
                refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
        
                }))
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            removeLoginUser()
            appDelegate.SabzilanaLoginUserId = ""
            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
            appDelegate.navigationController = UINavigationController(rootViewController: viewController)
            appDelegate.styleNavigationController(appDelegate.navigationController)
            appDelegate.window?.rootViewController = appDelegate.navigationController
        }))
        DispatchQueue.main.async {
            appDelegate.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        DispatchQueue.main.async {
            self.GetUserInfo()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "My Profile"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func UpdateUI(){
        
        if let userDetailsList = self.ResponseResult.object(forKey: "getdetail") as? NSArray {
            if userDetailsList.count > 0 {
                
                if let userResult = userDetailsList.object(at: 0) as? NSDictionary {
                    setLoginUser(userResult)
                    if let tempStr = userResult.object(forKey: "name") as? String {
                        self.lblName.text = tempStr
                    }
                    if let tempStr = userResult.object(forKey: "phone") as? String {
                        self.lblMobile.text = tempStr
                    }
                    if let tempStr = userResult.object(forKey: "email") as? String {
                        self.lblEmail.text = tempStr
                    }
                    if let tempStr = userResult.object(forKey: "city") as? String {
                        self.lblAddress.text = tempStr
                    }
                    if let tempStr = userResult.object(forKey: "image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                            self.imageview.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "user"))
                            
                            
                        }
                    }
                }
            }
        }
        
        
    }
    
    func GetUserInfo(){
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        let urlStr = String(format: "%@?view=getinfo&userID=%@&phone=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo])
        
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            
            if succeeded == true {
                print(data)
                
                if let result = data as? NSDictionary {
                    
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            self.ResponseResult = NSDictionary(dictionary: result)
                            DispatchQueue.main.async {
                                self.UpdateUI()
                                
                            }
                        }
                    }
                    
                }
                
            }
            
            
        }
        
    }
    
    
    //MARK:- Delegate -
    func didGetProfileInfo() {
        self.GetUserInfo()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
