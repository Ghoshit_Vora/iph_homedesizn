//
//  AreaViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 4/25/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ItemPackSizeController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {
    
    var delegate : DropDownMenuDelegate?
    @IBOutlet var titleLabel : UILabel!
    var dropDownprice_ID = ""
    
    @IBOutlet var tableview : UITableView!
    var responseResult = NSArray() {
        didSet{
            self.UpdateUI()
        }
    }
    var closeHandler: (() -> Void)?
    
    class func instance() -> ItemPackSizeController {
        let storyboard = ItemPackSizeController(nibName: "ItemPackSizeController", bundle: nil)
        return storyboard
    }
    func UpdateUI(){
        if self.tableview != nil  {
            DispatchQueue.main.async
                {
                    self.tableview.dataSource = self
                    self.tableview.delegate = self
                    self.tableview.ReloadWithAnimation()
            }
            
        }else{
            self.perform(#selector(ItemPackSizeController.UpdateUI), with: nil, afterDelay: 0.2)
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "ItemSizewCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "ItemSizewCell")
    }

    @IBAction func closeClick(){
        closeHandler?()
    }
    
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let height = CGFloat(((self.responseResult.count + 1) * 50))
        return CGSize(width: UIScreen.main.bounds.width - 60, height: height)
    }
    
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseResult.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.responseResult.count > indexPath.row {
            if let data = self.responseResult.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ItemSizewCell") as? ItemSizewCell {
                    cell.resultData = data
                    if let princeId = data.object(forKey: "price_ID") as? String {
                        if princeId.caseInsensitiveCompare(self.dropDownprice_ID) == .orderedSame {
                            cell.titleLabel.textColor = UIColor.white
                            cell.headerView.backgroundColor = UIColor.darkGray
                        }else{
                            cell.headerView.backgroundColor = UIColor.white
                            cell.titleLabel.textColor = UIColor.black
                        }
                    }
                    
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? ItemSizewCell {
            if self.delegate != nil {
                self.delegate?.didSelectDropDownResult(data: cell.resultData)
                closeHandler?()
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
