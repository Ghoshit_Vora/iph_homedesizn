//
//  MultipleImageHeader.swift
//  Sabzilana
//
//  Created by Desap Team on 22/03/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

protocol MultipleImageHeaderDelegate {
    func didTapFullScreenImage()
    func getSahreImage(image : UIImage)
}
class MultipleImageHeader: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var delegate : MultipleImageHeaderDelegate?
    
    @IBOutlet var collectionLargeImage : UICollectionView!
    @IBOutlet var collectionSmallImage : UICollectionView!
    
    var imageList : NSArray = NSArray() {
        didSet{
            self.LoadResult()
        }
    }
    
    class func instanceFromNib() -> MultipleImageHeader {
        return UINib(nibName: "MultipleImageHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MultipleImageHeader
    }

    func LoadResult(){
        if self.collectionLargeImage != nil {
            
            let nibName5 = UINib(nibName: "ImageViewCell", bundle:nil)
            self.collectionLargeImage.register(nibName5, forCellWithReuseIdentifier: "ImageViewCell")
            
            self.collectionSmallImage.register(nibName5, forCellWithReuseIdentifier: "ImageViewCell")
            
            DispatchQueue.main.async
                {
                    self.collectionLargeImage.delegate = self
                    self.collectionLargeImage.dataSource = self
                    self.collectionLargeImage.ReloadWithAnimation()
                    
                    self.collectionSmallImage.delegate = self
                    self.collectionSmallImage.dataSource = self
                    self.collectionSmallImage.ReloadWithAnimation()
                    
            }
        }else{
            self.perform(#selector(HeaderHomeCell.LoadResult), with: nil, afterDelay: 0.2)
        }
        
    }
    
    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: 100, height: 100);
        
        if collectionView == self.collectionLargeImage {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height);
        }
        
        let height = collectionView.frame.size.height
        
        return CGSize(width: height, height: height);
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.imageList.count > indexPath.row {
            if let result = self.imageList.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewCell", for: indexPath) as? ImageViewCell {
                    cell.resultData = result
                    
                    if let tempStr = result.object(forKey: "large_image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                            cell.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                                if image != nil {
                                    if indexPath.row == 0 {
                                        self.delegate?.getSahreImage(image: image!)
                                    }
                                    cell.imageview.image = image
                                }
                            })
                            
                        }
                    }
                    
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if collectionView ==  self.collectionSmallImage {
            self.collectionLargeImage.scrollToItem(at: IndexPath(row: indexPath.row, section: indexPath.section), at: .centeredHorizontally, animated: true)
        }else{
            if self.delegate != nil {
                self.delegate?.didTapFullScreenImage()
            }
        }

        
    }
    
}
