//
//  ProductDetailsCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/31/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol ProductDetailsCellDelegate {
    func didTapImageView(data : NSDictionary)
    func viewReviewProductDetails(cell : ProductDetailsCell)
    func shareProduct(cell : ProductDetailsCell)
}
class ProductDetailsCell: UITableViewCell,DropDownMenuDelegate,MultipleImageHeaderDelegate {

    
    @IBOutlet var scrollview : UIScrollView?
    
    var delegateSelf : ProductDetailsCellDelegate?
    
    var isWishListEnable : Bool = false
    
    @IBOutlet var descLabel : UILabel!
    
    var delegate : ProductCellBasketUpdateDelegate?
    
    var shareImage = UIImage()
    @IBOutlet var nameLabel : UILabel!
    
   @IBOutlet var brandLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
   
    @IBOutlet var availibilityLabel : UILabel?
    @IBOutlet var reviewCountLabel : UILabel?
    @IBOutlet var rateView : UIView?
    
    var Qty_Price_ID : String = ""
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    @IBAction func reviewsClick(){
        if self.delegateSelf != nil {
            self.delegateSelf?.viewReviewProductDetails(cell: self)
            
        }
    }
    @IBAction func shareClick(){
        if self.delegateSelf != nil {
            self.delegateSelf?.shareProduct(cell: self)
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProductDetailsCell.handleTap))
//        self.imageview.isUserInteractionEnabled = true
//        self.imageview.addGestureRecognizer(tapGestureRecognizer)
       
       
        
    }
    func didTapFullScreenImage() {
        self.handleTap()
    }
    func getSahreImage(image: UIImage) {
        self.shareImage = image
    }
    func handleTap(){
        if self.delegateSelf != nil {
            self.delegateSelf?.didTapImageView(data: self.resultData)
        }
    }
    @IBAction func addQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyAddedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
        
    }
    @IBAction func plusQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
    }
    @IBAction func minusQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
    }
    func UpdateNCheckQty(){
//        if self.selectedQty <= 0 {
//            self.selectedQty = 0
////            self.buttonAddQty.isHidden = false
////            self.qtyView.isHidden = true
//        }else{
////            self.buttonAddQty.isHidden = true
////            self.qtyView.isHidden = false
//
//        }
//        self.qtyLabel.text = String(format: "%d", self.selectedQty)
        
    }
    
    func SetResultUI(){
        
        if self.nameLabel != nil && self.resultData.object(forKey: "productID") != nil {
            
            if let scroll = self.scrollview {
                let multipleImageView = MultipleImageHeader.instanceFromNib()
                multipleImageView.delegate = self
                multipleImageView.frame = scroll.bounds
                multipleImageView.contentMode = .scaleAspectFit
                scroll.addSubview(multipleImageView)
                
                if let image_list = resultData.object(forKey: "image_list") as? NSArray {
                    multipleImageView.imageList = image_list
                }
            }
            
            
            //Qty View
//            self.buttonAddQty.isHidden = false
//            self.qtyView.isHidden = true
            
            //
            /*
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            */
            var nameStr = ""
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                nameStr = tempStr
            }
            if let tempStr = resultData.object(forKey: "caption") as? String {
                nameStr = String(format: "%@\n%@", arguments: [nameStr,tempStr])
            }
            self.nameLabel.text = nameStr
           
            if let tempStr = resultData.object(forKey: "price") as? String {
                let priceStr = String(format: "₹ %@", arguments: [tempStr])
                self.priceLabel.text = priceStr
                
            }
            if let tempStr = resultData.object(forKey: "description") as? String {
                self.descLabel.text = tempStr
            }
            
            if let tempStr = resultData.object(forKey: "review_count") as? NSNumber {
                self.reviewCountLabel?.text = "(\(tempStr.stringValue))"
            }
            if let tempStr = resultData.object(forKey: "availibility") as? String {
                self.availibilityLabel?.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "brand") as? String {
                self.brandLabel?.text = tempStr
            }
            
            //Update and Check Qty
            
            let rate = EZRatingView(frame: (self.rateView?.bounds)!)
            rate.isUserInteractionEnabled = false
//            rate.stepInterval = 0.5
            if let tempStr = resultData.object(forKey: "star") as? NSNumber {
                rate.value = CGFloat(tempStr.floatValue)
            }
            if let tempStr = resultData.object(forKey: "star") as? String {
                if let rateValue = Float(tempStr) {
                    rate.value = CGFloat(rateValue)
                }
                
            }
            self.rateView?.addSubview(rate)
            
            
            //Update Favourites
            if self.isWishListEnable == true {
//                self.btnFavourite.isHidden = true
            }else{
//                self.btnFavourite.isHidden = false
                self.FavouriteUpdate()
            }
            
        }else{
            self.perform(#selector(ProductDetailsCell.SetResultUI))
        }
        
    }
    
    //Update GM or PCS
    func UpdateGMOrPcsQty(priceData : NSDictionary){
        if let tempStr = priceData.object(forKey: "price_ID") as? String {
            self.Qty_Price_ID = tempStr
        }
        
        
        if let tempStr = priceData.object(forKey: "price") as? String {
            let priceStr = String(format: "₹ %@", arguments: [tempStr])
            self.priceLabel.text = priceStr
            
        }
        if let tempStr = priceData.object(forKey: "weight") as? String {
            
            // alternative: not case sensitive
//            if tempStr.lowercased().range(of:"pcs") != nil {
//                self.weightLabel.text = tempStr
//                self.weightView.layer.borderColor = UIColor.clear.cgColor
//                self.weightArrow.isHidden = true
//            }else{
//                self.weightLabel.text = tempStr
//                self.weightView.layer.borderColor = UIColor.gray.cgColor
//                self.weightArrow.isHidden = false
//            }
        }
        
//        let tempQty = self.QtyProductInBasket()
        
//        if let intQty = Int(tempQty) {
//            self.selectedQty = intQty
//        }else {
//            self.selectedQty = 0
//        }
        
        self.UpdateNCheckQty()
        
    }
    
    //MARK:- Database Query
    /*
    func AddProductInBasket(){
        
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        
        //Mutable Basket Object and Update Qty
        let liveDataObject = NSMutableDictionary(dictionary: self.resultData)
        let liveQty = String(format: "%d", self.selectedQty)
        print(liveQty)
        liveDataObject.setValue(liveQty, forKey: kBasketQtyKey)
        liveDataObject.setValue(livePriceId, forKey: kProductItemID)
        
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            if self.selectedQty > 0 {
                                liveMutableBasket.add(liveDataObject)
                            }
                        }else{
                            if self.selectedQty > 0 {
                                if liveMutableBasket.count > index {
                                    liveMutableBasket.replaceObject(at: index, with: liveDataObject)
                                }
                                
                            }else{
                                if liveMutableBasket.count > index {
                                    liveMutableBasket.removeObject(at: index)
                                }
                            }
                        }
                    }else{
                        if self.selectedQty > 0 {
                            liveMutableBasket.add(liveDataObject)
                        }
                    }
                }else{
                    if self.selectedQty > 0 {
                        liveMutableBasket.add(liveDataObject)
                    }
                }
                
            }else{
                if self.selectedQty > 0 {
                    liveMutableBasket.add(liveDataObject)
                }
            }
            
            //Update Basket Result in Local DB
            SetBasketLocalResult(liveMutableBasket)
        }
        
        
        print(GetBasketLocalResult())
        
        //Update Basket QTY
        DispatchQueue.main.async {
            if self.delegate != nil {
                self.delegate?.didUpdateBasket()
            }
        }
        
    }
    */
    
    func QtyProductInBasket() -> String {
        
        var LiveItemQty = "0"
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            LiveItemQty = "0"
                        }else{
                            if liveMutableBasket.count > index {
                                if let product = liveMutableBasket.object(at: index) as? NSDictionary {
                                    if let tempQty = product.object(forKey: kBasketQtyKey) as? String {
                                        LiveItemQty = tempQty
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
        }
        return LiveItemQty
        
    }
    
    //MARK:- Item Pack Size -
    @IBAction func ItemsPackSizeClick(){
        
        var viewController = UIViewController()
        
        if let tempController = appDelegate.window?.visibleViewController {
            viewController = tempController
        }
        let popup = PopupController
            .create(viewController)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = ItemPackSizeController.instance()
        container.delegate = self
        
        if let priceList = resultData.object(forKey: "price_list") as? NSArray {
            container.responseResult = NSArray(array: priceList)
        }
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    
    //MAR:- Dropdown delegate -
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        if let priceData = data as? NSDictionary {
            DispatchQueue.main.async {
                self.UpdateGMOrPcsQty(priceData: priceData)
            }
        }
        
    }
    
    //MARK:- Manage Favourite -
    @IBAction func FavouriteClick(){
        
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to be signed in to Create and Save your own wishlist.")
            return
        }
        
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    var productId = ""
                    
                    if let dbData = wishListResult.object(at: 0) as? NSDictionary {
                        //DB Data
                        if let tempStr = dbData.object(forKey: kWishListProductID) as? String {
                            productId = tempStr
                        }
                        
                        //Delete Query
                        let queryDelete = String(format: "DELETE FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,productId)
                        MyDbManager.sharedClass().exicuteSQLiteQuery(queryDelete)
                    }
                }else{
                    if tempStr.isEmpty == false {
                        let insertData = [kWishListProductID : tempStr]
                        MyDbManager.sharedClass().insert(into: kWishListTableName, field: insertData )
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.FavouriteUpdate()
            }
        }
    }
    func FavouriteUpdate(){
        //        favourites
        /*
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    self.btnFavourite.setImage(#imageLiteral(resourceName: "favouriteSel"), for: .normal)
                }else{
                    if tempStr.isEmpty == false {
                        self.btnFavourite.setImage(#imageLiteral(resourceName: "favourites"), for: .normal)
                    }
                }
            }
        }
         */
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
