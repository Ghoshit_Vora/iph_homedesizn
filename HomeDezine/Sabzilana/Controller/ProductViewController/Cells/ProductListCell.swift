//
//  ProductListCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

protocol ProductCellBasketUpdateDelegate {
    func didUpdateBasket()
    func wishlistDidDeleteRecord(data : NSDictionary)
}
class ProductListCell: UITableViewCell,DropDownMenuDelegate {

    var isWishListEnable : Bool = false
    @IBOutlet var btnDelete : UIButton!
    @IBOutlet var btnArror : UIButton!
    
    var delegate : ProductCellBasketUpdateDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var captionLabel : UILabel!
    @IBOutlet var discountLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var mrpLabel : UILabel!
    
    @IBOutlet var weightLabel : UILabel!
    
    @IBOutlet var weightView : UIView!
    @IBOutlet var weightArrow : UIButton!
    
    
    @IBOutlet var btnFavourite : UIButton!
    
    @IBOutlet var buttonAddQty : UIButton!
    @IBOutlet var qtyView : UIView!
    @IBOutlet var qtyLabel : UILabel!
    
    
    @IBOutlet var rateLabel : UILabel?
    @IBOutlet var brandNameLabel : UILabel?
    
    var Qty_Price_ID : String = ""
    var selectedQty : Int = 0
    var dropDownprice_ID = ""
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func deleteItemInList(){
        if self.delegate != nil {
            self.delegate?.wishlistDidDeleteRecord(data: self.resultData)
        }
    }
    @IBAction func addQtyClick(){
        self.selectedQty = 1
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyAddedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
    }
    @IBAction func plusQtyClick(){
        self.selectedQty+=1
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
    }
    @IBAction func minusQtyClick(){
        self.selectedQty-=1
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
        self.AddProductInBasket()
        
        if let visibleView = appDelegate.window?.visibleViewController?.view {
            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
        }
    }
    func UpdateNCheckQty(){
        if self.selectedQty <= 0 {
            self.selectedQty = 0
//            self.buttonAddQty.isHidden = false
//            self.qtyView.isHidden = true
        }else{
//            self.buttonAddQty.isHidden = true
//            self.qtyView.isHidden = false
            
        }
        self.qtyLabel.text = String(format: "%d", self.selectedQty)
        
    }
    
    func SetResultUI(){
        
        if self.imageview != nil && self.resultData.object(forKey: "productID") != nil {
            
            //Qty View
//            self.buttonAddQty.isHidden = false
//            self.qtyView.isHidden = true
            
            //
            
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "brand") as? String {
                self.captionLabel.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "price") as? String {
                let priceStr = String(format: "₹ %@", arguments: [tempStr])
                self.priceLabel.text = priceStr
                
            }
            
            if let tempStr = resultData.object(forKey: "total_review") as? String {
                self.rateLabel?.text = tempStr
            }
            if let tempStr = resultData.object(forKey: "brand") as? String {
                self.brandNameLabel?.text = tempStr
            }
            //Get Prices Data
            if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                if priceList.count > 0 {
                    if let priceData = priceList.object(at: 0) as? NSDictionary {
                        self.UpdateGMOrPcsQty(priceData: priceData)
                    }
                }
                
            }
            //Update and Check Qty
            self.UpdateNCheckQty()
            
            //Qty Sold Out
            if let tempStr = resultData.object(forKey: "sold_out") as? String {
                if tempStr.caseInsensitiveCompare("yes") == .orderedSame {
//                    self.buttonAddQty.isHidden = true
//                    self.qtyView.isHidden = true
                }
            }
            
            //Update Favourites
            if self.isWishListEnable == true {
                self.btnFavourite.isHidden = true
                self.btnDelete.isHidden = false
                self.btnArror.isHidden = true
            }else{
                self.btnFavourite.isHidden = false
                self.btnDelete.isHidden = true
                self.btnArror.isHidden = false
                self.FavouriteUpdate()
            }
            
            
        }else{
            self.perform(#selector(ProductListCell.SetResultUI))
        }
        
    }
    
    //Update GM or PCS
    func UpdateGMOrPcsQty(priceData : NSDictionary){
        if let tempStr = priceData.object(forKey: "price_ID") as? String {
            self.Qty_Price_ID = tempStr
        }
        if let tempStr = priceData.object(forKey: "dis") as? String {
            self.discountLabel.text = String(format: "%@%% off", arguments: [tempStr])
        }
        if let tempStr = priceData.object(forKey: "mrp") as? String {
//            let strokeStr = String(format: "₹ %@", arguments: [tempStr])
//            self.mrpLabel.StrikeText(text: strokeStr)
            
        }
        if let tempStr = priceData.object(forKey: "price") as? String {
            let priceStr = String(format: "₹ %@", arguments: [tempStr])
            self.priceLabel.text = priceStr
            
        }
        if let tempStr = priceData.object(forKey: "weight") as? String {
            
            if let princeId = priceData.object(forKey: "price_ID") as? String {
                self.dropDownprice_ID = princeId
            }
            // alternative: not case sensitive
            if tempStr.lowercased().range(of:"pcs") != nil {
                self.weightLabel.text = tempStr
                self.weightView.layer.borderColor = UIColor.clear.cgColor
                self.weightArrow.isHidden = true
            }else{
                self.weightLabel.text = tempStr
                self.weightView.layer.borderColor = UIColor.gray.cgColor
                self.weightArrow.isHidden = false
                
            }
        }
        
        let tempQty = self.QtyProductInBasket()
        
        if let intQty = Int(tempQty) {
            self.selectedQty = intQty
        }else {
            self.selectedQty = 0
        }
        
        self.UpdateNCheckQty()
        
    }
    
    //MARK:- Database Query
    func AddProductInBasket(){
        
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        
        //Mutable Basket Object and Update Qty
        let liveDataObject = NSMutableDictionary(dictionary: self.resultData)
        let liveQty = String(format: "%d", self.selectedQty)
        print(liveQty)
        liveDataObject.setValue(liveQty, forKey: kBasketQtyKey)
        liveDataObject.setValue(livePriceId, forKey: kProductItemID)
        
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            if self.selectedQty > 0 {
                                liveMutableBasket.add(liveDataObject)
                            }
                        }else{
                            if self.selectedQty > 0 {
                                if liveMutableBasket.count > index {
                                   liveMutableBasket.replaceObject(at: index, with: liveDataObject)
                                }
                                
                            }else{
                                if liveMutableBasket.count > index {
                                    liveMutableBasket.removeObject(at: index)
                                }
                            }
                        }
                    }else{
                        if self.selectedQty > 0 {
                            liveMutableBasket.add(liveDataObject)
                        }
                    }
                }else{
                    if self.selectedQty > 0 {
                        liveMutableBasket.add(liveDataObject)
                    }
                }
                
            }else{
                if self.selectedQty > 0 {
                    liveMutableBasket.add(liveDataObject)
                }
            }
            
            //Update Basket Result in Local DB
            SetBasketLocalResult(liveMutableBasket)
        }
        
        
        print(GetBasketLocalResult())
       
        //Update Basket QTY
        DispatchQueue.main.async {
            if self.delegate != nil {
                self.delegate?.didUpdateBasket()
            }
        }
        
    }

    func QtyProductInBasket() -> String {
        
        var LiveItemQty = "0"
        //Get Current Product ID
        var liveProductId = ""
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            liveProductId = tempStr
        }
        
        let livePriceId = self.Qty_Price_ID
        
        //Check Replace Or Add New Value in Basket
        let liveMutableBasket = NSMutableArray(array: GetBasketLocalResult())
        
        if let basketResult = GetBasketLocalResult().value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(liveProductId) == true {
                
                if let basketItemsId = GetBasketLocalResult().value(forKeyPath: kProductItemID) as? NSArray {
                    if basketItemsId.contains(livePriceId) == true {
                        let index = basketItemsId.index(of: livePriceId)
                        
                        if index == NSNotFound {
                            LiveItemQty = "0"
                        }else{
                            if liveMutableBasket.count > index {
                                if let product = liveMutableBasket.object(at: index) as? NSDictionary {
                                    if let tempQty = product.object(forKey: kBasketQtyKey) as? String {
                                        LiveItemQty = tempQty
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
            
        }
        return LiveItemQty
        
    }
    
    //MARK:- Item Pack Size - 
    @IBAction func ItemsPackSizeClick(){
        
        var viewController = UIViewController()
        
        if let tempController = appDelegate.window?.visibleViewController {
            viewController = tempController
        }
        let popup = PopupController
            .create(viewController)
            .customize(
                [
                    .layout(.center),
                    .animation(.fadeIn),
                    .backgroundStyle(.blackFilter(alpha: 0.8)),
                    .dismissWhenTaps(true),
                    .scrollable(false)
                ]
            )
            .didShowHandler { popup in
                print("showed popup!")
            }
            .didCloseHandler { popup in
                print("closed popup!")
        }
        
        let container = ItemPackSizeController.instance()
        container.delegate = self
        container.dropDownprice_ID = self.dropDownprice_ID
        if let priceList = resultData.object(forKey: "price_list") as? NSArray {
            print(priceList)
            container.responseResult = NSArray(array: priceList)
        }
        
        container.closeHandler = { data in
            popup.dismiss()
        }
        
        popup.show(container)
    }
    
   //MAR:- Dropdown delegate -
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        if let priceData = data as? NSDictionary {
            DispatchQueue.main.async {
               self.UpdateGMOrPcsQty(priceData: priceData)
            }
        }
        
    }
    
    //MARK:- Manage Favourite -
    @IBAction func FavouriteClick(){
        
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to be signed in to Create and Save your own wishlist.")
            return
        }
        
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    var productId = ""
                    
                    if let dbData = wishListResult.object(at: 0) as? NSDictionary {
                        //DB Data
                        if let tempStr = dbData.object(forKey: kWishListProductID) as? String {
                            productId = tempStr
                        }
                        
                        //Delete Query
                        let queryDelete = String(format: "DELETE FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,productId)
                        MyDbManager.sharedClass().exicuteSQLiteQuery(queryDelete)
                    }
                }else{
                    if tempStr.isEmpty == false {
                        let insertData = [kWishListProductID : tempStr]
                        MyDbManager.sharedClass().insert(into: kWishListTableName, field: insertData )
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.FavouriteUpdate()
            }
        }
    }
    func FavouriteUpdate(){
//        favourites
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    self.btnFavourite.setImage(#imageLiteral(resourceName: "favouriteSel"), for: .normal)
                }else{
                    if tempStr.isEmpty == false {
                        self.btnFavourite.setImage(#imageLiteral(resourceName: "favourites"), for: .normal)
                    }
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
