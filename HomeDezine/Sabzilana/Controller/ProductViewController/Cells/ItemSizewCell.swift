//
//  ItemSizewCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/27/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ItemSizewCell: UITableViewCell {

    @IBOutlet var headerView : UIView!
    @IBOutlet var titleLabel : UILabel!
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        if self.titleLabel != nil  {
            var priceStr = ""
            if let tempStr = resultData.object(forKey: "price") as? String {
                priceStr = String(format: "₹ %@", arguments: [tempStr])
            }
            if let tempStr = resultData.object(forKey: "weight") as? String {
                self.titleLabel.text = String(format: "%@ / %@", arguments: [priceStr,tempStr])
            }
            
        }else{
            self.perform(#selector(ItemSizewCell.SetResultUI), with: nil, afterDelay: 0.2)
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
