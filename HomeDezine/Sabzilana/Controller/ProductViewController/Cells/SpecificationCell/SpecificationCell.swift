//
//  SpecificationCell.swift
//  Sabzilana
//
//  Created by Desap Team on 27/01/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit
protocol SpecificationCellDelegate {
    func writeReview(cell : SpecificationCell)
    func viewReviews(cell : SpecificationCell)
}
class SpecificationCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    var delegate : SpecificationCellDelegate?
    
    @IBOutlet var tableview : UITableView?
    
    @IBOutlet var specificLabel : UILabel?
    @IBOutlet var reviewLabel : UILabel?
    @IBOutlet var specificLine : UIView?
    @IBOutlet var reviewLine : UIView?
    @IBOutlet var menuView : UIView?
    
    @IBOutlet var reviewContainerView : UIView?
    
    
    @IBOutlet var reviewText : UITextView?
    
    @IBOutlet var rateView : UIView?
    var rateEZ = EZRatingView()
    
    var resultData = NSDictionary()
    var specificationDetailList = NSArray() {
        didSet{
            self.UpdateUI()
        }
    }
    
    @IBAction func submitReview(){
        if self.delegate != nil {
            self.delegate?.writeReview(cell: self)
        }
    }
    @IBAction func viewAllReview(){
        if self.delegate != nil {
            self.delegate?.viewReviews(cell: self)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let nibName = UINib(nibName: "SpecificationDetailsCell", bundle: nil)
        self.tableview?.register(nibName, forCellReuseIdentifier: "SpecificationDetailsCell")
        
        
        
//        self.UpdateUI()
        self.shadowView()
        self.setMenuSelect(index: 0)
        
        self.rateViewConfigure()
    }
    func rateViewConfigure(){
        rateEZ = EZRatingView(frame: (self.rateView?.bounds)!)
        rateEZ.isUserInteractionEnabled = true
        rateEZ.stepInterval = 0.5
        
        rateEZ.addTarget(self, action: #selector(changeRate(sender:)), for: .valueChanged)
        self.rateView?.addSubview(rateEZ)
    }
    @objc func changeRate(sender : EZRatingView) {
//        _ratingNumbers[sender.tag] = @(sender.value);
    }
    func shadowView(){
        self.reviewContainerView?.layer.masksToBounds = false
        self.reviewContainerView?.layer.shadowColor = UIColor.darkGray.cgColor
        self.reviewContainerView?.layer.shadowOpacity = 0.5
        self.reviewContainerView?.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.reviewContainerView?.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.reviewText?.placeholder = "Write Your Review"
       
    }
    func setMenuSelect(index : Int){
        if index == 0 {
            self.specificLabel?.textColor = UIColor.black
            self.specificLine?.backgroundColor = UIColor(hexString: "B90042")
            self.reviewLabel?.textColor = UIColor.darkGray
            self.reviewLine?.backgroundColor = UIColor.gray
            self.reviewContainerView?.isHidden = true
            self.tableview?.isHidden = false
        }else{
            self.specificLabel?.textColor = UIColor.darkGray
            self.specificLine?.backgroundColor = UIColor.gray
            self.reviewLabel?.textColor = UIColor.black
            self.reviewLine?.backgroundColor = UIColor(hexString: "B90042")
            self.reviewContainerView?.isHidden = false
            self.tableview?.isHidden = true
        }
    }
    @IBAction func specificationClick(){
        self.setMenuSelect(index: 0)
    }
    @IBAction func reviewClick(){
        self.setMenuSelect(index: 1)
    }
    func UpdateUI(){
        if self.tableview != nil {
            self.UpdateTableView()
        }else{
            self.perform(#selector(self.UpdateUI), with: nil, afterDelay: 0.2)
        }
    }
    func UpdateTableView(){
        self.tableview?.dataSource = self
        self.tableview?.delegate = self
        self.tableview?.reloadData()
    }

    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.specificationDetailList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = self.tableview?.dequeueReusableCell(withIdentifier: "SpecificationDetailsCell") as? SpecificationDetailsCell {
            if self.specificationDetailList.count > indexPath.row {
                if let specification = self.specificationDetailList.object(at: indexPath.row) as? NSDictionary {
                    if let valueLabel = specification.object(forKey: "attribute") as? String {
                        cell.attributeLabel?.text = valueLabel
                    }
                    if let valueLabel = specification.object(forKey: "value") as? String {
                        cell.valueLabel?.text = valueLabel
                    }
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
