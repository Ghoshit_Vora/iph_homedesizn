//
//  ProductViewController.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

enum LoadMoreStatus{
    case loading
    case finished
    case haveMore
}


class ProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, ProductCellBasketUpdateDelegate,FilterProductDelegate {

    var totalNumberOfProduct : String = "0"
    
    var barButtonBasket : MJBadgeBarButton = MJBadgeBarButton()
    @IBOutlet var clearFilterButton : UIButton?
    
    @IBOutlet var tableview : UITableView!
    @IBOutlet var collectionView : UICollectionView!
    var ProductListResul = NSMutableArray()
    var categoryResultData = NSDictionary(){
        didSet{
            self.GetProductResult(sortType: SortingEnableValue, pageCode: currentPageCode, filterCategory: "")
            self.setNavigationTitle()
        }
    }
    
    var isLastChoiceEnable : Bool = false {
        didSet{
            self.GetLastChoiceResult(sortType: SortingEnableValue, pageCode: currentPageCode)
            self.setNavigationTitle()
        }
    }
    
    var isWishListEnable : Bool = false {
        didSet{
            self.GetWishListResult(sortType: SortingEnableValue, pageCode: currentPageCode)
            self.setNavigationTitle()
        }
    }
    
    
    var isListViewEnable : Bool = false
    var SortingEnableValue : String = ""
    var currentPageCode : Int = 0
    
    var loadingStatus = LoadMoreStatus.haveMore
    @IBOutlet var footerView : UIView!
    let loadMoreSpinner = UIActivityIndicatorView()
    
    func loadMore() {
        if loadingStatus != .finished {
            self.currentPageCode = self.currentPageCode + 1
            
            if self.isLastChoiceEnable == true {
               self.GetLastChoiceResult(sortType: SortingEnableValue, pageCode: currentPageCode)
            }else if self.isWishListEnable == true {
                self.GetWishListResult(sortType: SortingEnableValue, pageCode: currentPageCode)
            }
            else{
                self.GetProductResult(sortType: self.SortingEnableValue, pageCode: self.currentPageCode, filterCategory: "" )
            }
            
            
        }else{
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }
    
    func  setNavigationTitle(){
        if self.navigationController != nil {
            
            if self.isWishListEnable == true {
                self.navigationController?.navigationBar.topItem?.title = "Wish List"
                return
            }else if self.isLastChoiceEnable == true {
                self.navigationController?.navigationBar.topItem?.title = "Last Choice"
                return
            }
            
            if let tempStr = categoryResultData.object(forKey: "name") as? String {
                if tempStr.isEmpty == false {
                    self.navigationController?.navigationBar.topItem?.title = tempStr
                }else{
                    self.navigationController?.navigationBar.topItem?.title = "Home Dezin"
                }
                
            }else{
                self.navigationController?.navigationBar.topItem?.title = "Home Dezin"
            }
        }else {
            self.perform(#selector(ProductViewController.setNavigationTitle), with: nil, afterDelay: 0.1)
        }
        
    }
    
    @IBAction func ViewStyleClick(_sender : UIButton){
        /*
        if self.isListViewEnable == true {
           self.isListViewEnable = false
            self.tableview.isHidden = true
            self.collectionView.isHidden = false
            DispatchQueue.main.async {
                self.collectionView.ReloadWithAnimation()
            }
        }else{
            self.isListViewEnable = true
            self.tableview.isHidden = false
            self.collectionView.isHidden = true
            DispatchQueue.main.async {
                self.tableview.ReloadWithAnimation()
            }
        }
 */
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loadMoreSpinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        loadMoreSpinner.color = NSTheme().GetNavigationBGColor()
        loadMoreSpinner.startAnimating()
        
        self.clearFilterButton?.isHidden = true
        
        
        let nibName = UINib(nibName: "ProductListCell", bundle: nil)
        self.tableview .register(nibName, forCellReuseIdentifier: "ProductListCell")
        
        let nibName5 = UINib(nibName: "ProducGridCell", bundle:nil)
        self.collectionView.register(nibName5, forCellWithReuseIdentifier: "ProducGridCell")
        
        self.collectionView.register(LoadMoreReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter , withReuseIdentifier: "LoadMoreVerticalCollectionFooterViewCellIdentifier")
        
        
        self.collectionView.isHidden = false
        self.tableview.isHidden = true
        self.tableview.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "newcart"), style: .plain, target: self, action: #selector(BasketClic))
        
        self.BarButtonBasket()
    }
    
    func BasketClic(){
        let objRoot = BasketViewController(nibName: "BasketViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d", arguments: [totalItems])
        self.barButtonBasket.badgeValue = priceStr
        
        if self.ProductListResul.count > 0 {
            DispatchQueue.main.async {
                self.tableview.ReloadWithAnimation()
                self.collectionView.ReloadWithAnimation()
            }
        }
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductViewController.SearchClick))
        
        
        
        self.navigationItem.rightBarButtonItems = [homeButton,search]
    }
    @objc func SearchClick(){
        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func BarButtonBasket(){
        
        let rightButtonActivity = UIButton(frame: CGRect(x: 0,y: 0,width: 40,height: 40))
        rightButtonActivity.addTarget(self, action: #selector(BasketClic), for: UIControlEvents.touchUpInside)
        rightButtonActivity.setImage(UIImage(named: "newcart"), for: UIControlState())
        rightButtonActivity.setTitleColor(UIColor.blue, for: UIControlState())
        rightButtonActivity.sizeToFit()
        
        self.barButtonBasket = MJBadgeBarButton()
        self.barButtonBasket.setup(rightButtonActivity)
        self.barButtonBasket.badgeValue = ""
        self.barButtonBasket.badgeOriginX = 10.0
        self.barButtonBasket.badgeOriginY = -6
        self.navigationItem.rightBarButtonItem = self.barButtonBasket
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 0.0
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductViewController.SearchClick))
        
        self.navigationItem.rightBarButtonItems = [search]
        //        self.navigationItem.rightBarButtonItems = [self.barButtonBasket,fixedSpace,search]
//        self.navigationItem.rightBarButtonItems = [self.barButtonBasket,fixedSpace,search]
    }
//    func SearchClick(){
//        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
//        self.navigationController?.pushViewController(objRoot, animated: true)
//    }
    

    
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ProductListResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if loadingStatus == .haveMore {
            return self.footerView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if loadingStatus == .haveMore {
            return 60.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    //MARK:- Show Number Of Record Toast-
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.isListViewEnable == true {
            let firstVisibleIndexPath = self.tableview.indexPathsForVisibleRows?[0]
            if firstVisibleIndexPath != nil {
                if let rows = firstVisibleIndexPath?.row {
                    let string = String(format: "Showing %d/%@ items", arguments: [rows + 1,self.totalNumberOfProduct])
//                    self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
                }
                
            }
        }else{
            
            let firstVisibleIndexPath = self.collectionView.indexPathsForVisibleItems[0]
            let string = String(format: "Showing %d/%@ items", arguments: [firstVisibleIndexPath.row + 1,self.totalNumberOfProduct])
//            self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        /*
        if self.isListViewEnable == true {
            let firstVisibleIndexPath = self.tableview.indexPathsForVisibleRows?[0]
            if firstVisibleIndexPath != nil {
                if let rows = firstVisibleIndexPath?.row {
                    let string = String(format: "Showing %d/%@ items", arguments: [rows + 1,self.totalNumberOfProduct])
                    self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
                }
                
            }
        }else{
            let firstVisibleIndexPath = self.collectionView.indexPathsForVisibleItems[0]
            let string = String(format: "Showing %d/%@ items", arguments: [firstVisibleIndexPath.row + 1,self.totalNumberOfProduct])
            self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
        }
        */
       
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int){
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            self.perform(#selector(ProductViewController.loadMore), with: nil, afterDelay: 0.5)
        }
    }
//
//    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print("didEndDisplaying")
//        if(indexPath.row == self.ProductListResul.count-1){
//            if loadingStatus == .haveMore {
//                self.tableview.tableFooterView = self.footerView
//                self.perform(#selector(ProductViewController.loadMore), with: nil, afterDelay: 0.5)
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.ProductListResul.count > indexPath.row {
            if let data = self.ProductListResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductListCell") as? ProductListCell {
                    cell.isWishListEnable = self.isWishListEnable
                    cell.delegate = self
                    cell.resultData = data
                    return cell
                }
            }
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? ProductListCell {
            let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            objRoot.isWishListEnable = self.isWishListEnable
            objRoot.productResult = cell.resultData
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }

    
    //MARK:- CollectionView Delegate - 
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var footerView:LoadMoreReusableView = LoadMoreReusableView()
        if (kind ==  UICollectionElementKindSectionFooter) && (loadingStatus != .finished){
            footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "LoadMoreVerticalCollectionFooterViewCellIdentifier", for: indexPath) as! LoadMoreReusableView
            //footerView.frame = CGRectMake(0, 0, self.view.frame.width, 80)
            footerView.backgroundColor = UIColor.groupTableViewBackground
            
            footerView.addSubview(loadMoreSpinner)
            loadMoreSpinner.center = CGPoint(x: (footerView.frame.size.width  / 2) - (loadMoreSpinner.frame.width / 2),
                                             y: (footerView.frame.size.height / 2) - (loadMoreSpinner.frame.height / 2));
//
        }
        
        return footerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if loadingStatus == .haveMore {
            return (loadingStatus == .finished) ? CGSize.zero : CGSize(width: self.view.frame.width, height: 60)
        }else{
            return CGSize.zero
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            self.perform(#selector(ProductViewController.loadMore), with: nil, afterDelay: 0.5)
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
        
        
//        if(indexPath.row == self.ProductListResul.count-1){
//            if loadingStatus == .haveMore {
//                self.perform(#selector(ProductViewController.loadMore), with: nil, afterDelay: 0.5)
//            }
//        }
        
        
    }
    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: 100, height: 100);
        return CGSize(width: (UIScreen.main.bounds.width / 2) - 1, height: 275);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ProductListResul.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.ProductListResul.count > indexPath.row {
            if let data = self.ProductListResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProducGridCell", for: indexPath) as? ProducGridCell {
                    cell.isWishListEnable = self.isWishListEnable
                    cell.delegate = self
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if let cell = collectionView.cellForItem(at: indexPath) as? ProducGridCell {
            let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            objRoot.isWishListEnable = self.isWishListEnable
            objRoot.productResult = cell.resultData
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
        
    }
    @IBAction func RefineClick(sender: AnyObject)
    {
        self.clearFilterButton?.isHidden = true
        self.SortingEnableValue = ""
        self.currentPageCode = 0
        
        if self.isLastChoiceEnable == true {
            self.GetLastChoiceResult(sortType: SortingEnableValue, pageCode: currentPageCode)
        }else if self.isWishListEnable == true {
            self.GetWishListResult(sortType: SortingEnableValue, pageCode: currentPageCode)
        }
        else{
            self.GetProductResult(sortType: self.SortingEnableValue, pageCode: self.currentPageCode, filterCategory: "" )
        }
        
    }
    
    @IBAction func filterClick(){
        let controller = FilterProductViewController(nibName: "FilterProductViewController", bundle: nil)
        controller.delegate = self
        if let tempStr = categoryResultData.object(forKey: "catID") as? String {
            controller.categoryId = tempStr
            
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func applyProductFilter(filterIds: String) {
        self.clearFilterButton?.isHidden = false
        self.SortingEnableValue = ""
        self.currentPageCode = 0
        
        self.GetProductResult(sortType: self.SortingEnableValue, pageCode: self.currentPageCode, filterCategory: filterIds )
    }
    @IBAction func sortByClick()
    {
        self.currentPageCode = 0
        
        let optionMenu = UIAlertController(title: nil, message: "SORT BY", preferredStyle: .actionSheet)
        
        let newestProduct = UIAlertAction(title: "Newest Product", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if self.isLastChoiceEnable == true {
                self.GetLastChoiceResult(sortType: "", pageCode: self.currentPageCode)
            }else if self.isWishListEnable == true {
                self.GetWishListResult(sortType: "", pageCode: self.currentPageCode)
            }
            else{
                self.GetProductResult(sortType: "", pageCode: self.currentPageCode, filterCategory: "" )
            }
        })
        
        let priceLowToHight = UIAlertAction(title: "Price Low ->Hight", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                if self.isLastChoiceEnable == true {
                    self.GetLastChoiceResult(sortType: "price_asc", pageCode: self.currentPageCode)
                }else if self.isWishListEnable == true {
                    self.GetWishListResult(sortType: "price_asc", pageCode: self.currentPageCode)
                }
                else{
                    self.GetProductResult(sortType: "price_asc", pageCode: self.currentPageCode, filterCategory: "" )
                }
        })
        
        let priceHighToLow = UIAlertAction(title: "Price High->Low", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                if self.isLastChoiceEnable == true {
                    self.GetLastChoiceResult(sortType: "price_desc", pageCode: self.currentPageCode)
                }else if self.isWishListEnable == true {
                    self.GetWishListResult(sortType: "price_desc", pageCode: self.currentPageCode)
                }
                else{
                    self.GetProductResult(sortType: "price_desc", pageCode: self.currentPageCode, filterCategory: "" )
                }
        })
        let nameAtoZ = UIAlertAction(title: "Name A To Z", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                if self.isLastChoiceEnable == true {
                    self.GetLastChoiceResult(sortType: "name_asc", pageCode: self.currentPageCode)
                }else if self.isWishListEnable == true {
                    self.GetWishListResult(sortType: "name_asc", pageCode: self.currentPageCode)
                }
                else{
                    self.GetProductResult(sortType: "name_asc", pageCode: self.currentPageCode, filterCategory: "" )
                }
        })
        let nameZtoA = UIAlertAction(title: "Name Z To A", style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
                if self.isLastChoiceEnable == true {
                    self.GetLastChoiceResult(sortType: "name_desc", pageCode: self.currentPageCode)
                }else if self.isWishListEnable == true {
                    self.GetWishListResult(sortType: "name_desc", pageCode: self.currentPageCode)
                }
                else{
                    self.GetProductResult(sortType: "name_desc", pageCode: self.currentPageCode, filterCategory: "" )
                }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            {
                (alert: UIAlertAction!) -> Void in
                
        })
        
        optionMenu.addAction(newestProduct)
        optionMenu.addAction(priceLowToHight)
        optionMenu.addAction(priceHighToLow)
        optionMenu.addAction(nameAtoZ)
        optionMenu.addAction(nameZtoA)
        optionMenu.addAction(cancelAction)
        
        DispatchQueue.main.async
            {
                self.present(optionMenu, animated: true, completion: nil)
        }
        
    }

    //MARK:- API -
    //GetProduct From Catagory
    func GetProductResult(sortType : String, pageCode : Int, filterCategory : String){
        print(sortType)
        print(pageCode)
        
        var CategoryId = ""
        if let tempStr = categoryResultData.object(forKey: "catID") as? String {
            CategoryId = tempStr
        }
        
        self.SortingEnableValue = sortType
        
        var requestUrl = ""
        if filterCategory.isEmpty == false {
            requestUrl = String(format: "%@?view=product_list&userID=%@&categoryID=%@&page_no=%d&filter_data=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,CategoryId,pageCode,filterCategory])
        }else if sortType.isEmpty == true {
            requestUrl = String(format: "%@?view=product_list&userID=%@&categoryID=%@&page_no=%d&city_id=&order_data=", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,CategoryId,pageCode])
        }else{
//            requestUrl = String(format: "%@?view=product_list&userID=%@&categoryID=%@&page_no=%d&type=%@&city_id=", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,CategoryId,pageCode,sortType])
            requestUrl = String(format: "%@?view=product_list&userID=%@&categoryID=%@&page_no=%d&order_data=%@&city_id=", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,CategoryId,pageCode,sortType])
        }
//        :http://www.homedezin.com/mapp/index.php?view=product_list&categoryID=NDk=&page_no=0&filter_data=&order_data=name_desc
        print(requestUrl)
        
        
        
        if pageCode <= 0 {
            self.ProductListResul = NSMutableArray()
        }
        TNMWSMethod(nil, url: requestUrl, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            self.loadingStatus = .haveMore
            
            if succeeded == true {
               
                print(data)
                //GetProduct List
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            //GetNumberOfProduct 
                            if let tempStr = result.object(forKey: "product_count") as? String {
                                self.totalNumberOfProduct = tempStr
                            }
                            if let tempStr = result.object(forKey: "product_count") as? NSNumber {
                                self.totalNumberOfProduct = tempStr.stringValue
                            }
                            
                            
                            if let resultList = result.object(forKey: "product_list") as? NSArray {
                                //                        self.ProductListResul = NSMutableArray(array: resultList)
                                
                                for tempData in resultList {
                                    self.ProductListResul.add(tempData)
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                        }else{
                                            self.tableview.reloadData()
                                        }
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.collectionView.delegate = self
                                            self.collectionView.dataSource = self
                                        }
                                        self.collectionView.ReloadWithAnimation()
                                        
                                }
                                if filterCategory.isEmpty == false {
                                    self.loadingStatus = .finished
                                }else{
                                    self.loadingStatus = .haveMore
                                }
                                
                                
                            }else{
                                self.loadingStatus = .finished
                                DispatchQueue.main.async
                                    {
                                        self.tableview.reloadData()
                                        self.collectionView.ReloadWithAnimation()
                                }
                            }
                        }else{
                             if let message = result.object(forKey: kMessage) as? String {
//                                ShowMessage("", message: message)
                            }
                            self.loadingStatus = .finished
                            DispatchQueue.main.async
                                {
                                    self.tableview.reloadData()
                                    self.collectionView.ReloadWithAnimation()
                            }
                        }
                    }
                    
                    
                }
                
                
            }else{
                self.loadingStatus = .finished
            }
            
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }
    /*
    //MARK:- Get Customer Personal Details -
    func GetCustomersPersonal(completion: @escaping (_ response: NSDictionary) -> Void) {
        appDelegate.DesapShowLoader()
        let appState = AppAuthState.shared
        
        let headers: HTTPHeaders = [
            "Content-Type" : "application/json",
            "Authorization" :  appState.AppAuthAccessToken
        ]
        
        request(kCustomersGet, headers: headers).responseJSON { response in
            
            appDelegate.DesapHideLoader()
            
            //Successs Response
            response.result.ifSuccess({
                if let json = response.result.value {
                    print("JSON: \(json)") // serialized json response
                    if let jsonRes = json as? NSDictionary {
                        completion(jsonRes)
                    }
                }
            })
            
            //Failure response
            response.result.ifFailure({
                if response.response?.statusCode == kStatusCodeAuthField {
                    appDelegate.applicationSessionWillExpiredHandler()
                }else{
                    if response.response?.statusCode == kStatusCodeOffline {
                        CpMessageWithOptions(message: kNoInternetConnection, perform: "Retry", cancel: "Cancel", completion: { (success) in
                            
                        })
                    }else{
                        appDelegate.TNMShowMessage("", message: kErrorMessage)
                    }
                    
                }
                
            })
        }
        
        
    }
*/
    //Get Last Choice 
    func GetLastChoiceResult(sortType : String, pageCode : Int){
        print(sortType)
        print(pageCode)
        
        var mobileNo = ""
        let data = getLoginUser()
        if let tempStr = data.object(forKey: "phone") as? String {
            mobileNo = tempStr
        }
        
        self.SortingEnableValue = sortType
        
        var requestUrl = ""
        if sortType.isEmpty == true {
            requestUrl = String(format: "%@?view=product_list&userID=%@&userPhone=%@&page_no=%d", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,pageCode])
        }else{
            requestUrl = String(format: "%@?view=product_list&userID=%@&userPhone=%@&page_no=%d&order_data=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,mobileNo,pageCode,sortType])
        }
        
        print(requestUrl)
        
        
        
        if pageCode <= 0 {
            self.ProductListResul = NSMutableArray()
        }
        TNMWSMethod(nil, url: requestUrl, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            self.loadingStatus = .haveMore
            
            if succeeded == true {
                
                print(data)
                //GetProduct List
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            //GetNumberOfProduct
                            if let tempStr = result.object(forKey: "product_count") as? String {
                                self.totalNumberOfProduct = tempStr
                            }
                            if let tempStr = result.object(forKey: "product_count") as? NSNumber {
                                self.totalNumberOfProduct = tempStr.stringValue
                            }
                            
                            if let resultList = result.object(forKey: "product_list") as? NSArray {
                                //                        self.ProductListResul = NSMutableArray(array: resultList)
                                
                                for tempData in resultList {
                                    self.ProductListResul.add(tempData)
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                        }else{
                                            self.tableview.reloadData()
                                        }
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.collectionView.delegate = self
                                            self.collectionView.dataSource = self
                                        }
                                        self.collectionView.ReloadWithAnimation()
                                        
                                }
                                self.loadingStatus = .haveMore
                                
                            }else{
                                self.loadingStatus = .finished
                                DispatchQueue.main.async
                                    {
                                        self.tableview.reloadData()
                                        self.collectionView.ReloadWithAnimation()
                                }
                            }
                        }else{
                            
                            self.loadingStatus = .finished
                            DispatchQueue.main.async
                                {
                                    self.tableview.reloadData()
                                    self.collectionView.ReloadWithAnimation()
                            }
                        }
                    }
                    
                    
                }
                
                
            }else{
                self.loadingStatus = .finished
            }
            
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }
    
    //Get Last Choice
    func GetWishListResult(sortType : String, pageCode : Int){
        print(sortType)
        print(pageCode)
        
        //Collect WishList
        let products = NSMutableDictionary()
        let productList = NSMutableArray()
        
        let QueryWishList = String(format: "SELECT * FROM %@", kWishListTableName)
        let DBResult = MyDbManager.sharedClass().exicuteSQLiteQuery(QueryWishList)
        if let result = DBResult {
            for data in result {
                if let tempData = data as? NSDictionary {
                    if let tempStr = tempData.object(forKey: kWishListProductID) as? String {
                        productList.add(["p_id":tempStr])
                    }
                }
            }
        }
        if productList.count <= 0 {
            ShowMessage("", message: "No products Found as in Cart")
            return
        }
        
        products.setValue(productList, forKey: "productID")
        let requestStr = GetJson(products)
      
        self.SortingEnableValue = sortType
        
        var requestUrl = ""
        if sortType.isEmpty == true {
            requestUrl = String(format: "%@?view=wish_list&page=wish_data_id&userID=%@&products=%@&pagecode=%d", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,requestStr,pageCode])
        }else{
            requestUrl = String(format: "%@?view=wish_list&page=wish_data_id&userID=%@&products=%@&pagecode=%d&order_data=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,requestStr,pageCode,sortType])
        }
        
        print(requestUrl)
                
        if pageCode <= 0 {
            self.ProductListResul = NSMutableArray()
        }
        TNMWSMethod(nil, url: requestUrl, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            
            self.loadingStatus = .haveMore
            
            if succeeded == true {
                
                print(data)
                //GetProduct List
                if let result = data as? NSDictionary {
                    if let statusCode = result.object(forKey: kStatusCode) as? String {
                        if statusCode == "0" {
                            
                            //GetNumberOfProductproduct_count
                            if let tempStr = result.object(forKey: "product_count") as? String {
                                self.totalNumberOfProduct = tempStr
                            }
                            if let tempStr = result.object(forKey: "product_count") as? NSNumber {
                                self.totalNumberOfProduct = tempStr.stringValue
                            }
                            
                            if let resultList = result.object(forKey: "product_list") as? NSArray {
                                //                        self.ProductListResul = NSMutableArray(array: resultList)
                                
                                for tempData in resultList {
                                    self.ProductListResul.add(tempData)
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.tableview.delegate = self
                                            self.tableview.dataSource = self
                                            self.tableview.ReloadWithAnimation()
                                        }else{
                                            self.tableview.reloadData()
                                        }
                                }
                                DispatchQueue.main.async
                                    {
                                        if pageCode <= 0 {
                                            self.collectionView.delegate = self
                                            self.collectionView.dataSource = self
                                        }
                                        self.collectionView.ReloadWithAnimation()
                                        
                                }
                                self.loadingStatus = .haveMore
                                
                            }else{
                                self.loadingStatus = .finished
                                DispatchQueue.main.async
                                    {
                                        self.tableview.reloadData()
                                        self.collectionView.ReloadWithAnimation()
                                }
                            }
                        }else{
                            
                            self.loadingStatus = .finished
                            DispatchQueue.main.async
                                {
                                    self.tableview.reloadData()
                                    self.collectionView.ReloadWithAnimation()
                            }
                        }
                    }
                    
                    
                }
                
                
            }else{
                self.loadingStatus = .finished
            }
            
            DispatchQueue.main.async {
                self.tableview.tableFooterView = UIView()
            }
        }
        
    }

    //MARK:- Product Cell Delegate - 
    func didUpdateBasket() {
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d", arguments: [totalItems])
        self.barButtonBasket.badgeValue = priceStr
    }
    func wishlistDidDeleteRecord(data: NSDictionary) {
        var productId = ""
        //DB Data
        if let tempStr = data.object(forKey: kWishListProductID) as? String {
            productId = tempStr
        }
        
        //Delete Query
        let queryDelete = String(format: "DELETE FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,productId)
        MyDbManager.sharedClass().exicuteSQLiteQuery(queryDelete)
        
        //ContainObject in Live Array
        if let basketResult = self.ProductListResul.value(forKeyPath: "productID") as? NSArray {
            if basketResult.contains(productId) == true {
                let index = basketResult.index(of: productId)
                
                if index == NSNotFound {
                    print("NSNotFound")
                }else{
                    self.ProductListResul.removeObject(at: index)
                    
                    DispatchQueue.main.async
                        {
                            self.tableview.ReloadWithAnimation()
                            self.collectionView.ReloadWithAnimation()
                    }
                   
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
