//
//  ProductDetailsViewController.swift
//  Sabzilana
//
//  Created by Jeevan on 31/03/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,ProductCellBasketUpdateDelegate,ProductDetailsCellDelegate,DeskBoardCategoryDelegate,SpecificationCellDelegate {

//    @IBOutlet var btnBasket : UIButton!
    
    
    var barButtonBasket : MJBadgeBarButton = MJBadgeBarButton()
    
    @IBOutlet var tableview : UITableView!
    var NoOfResultCell = NSMutableArray()
    var productDetailsResult = NSDictionary()
    var specificationDetailList = NSArray()
    
    var isWishListEnable : Bool = false
    
    var productResult = NSDictionary(){
        didSet{
            self.GetProductDetails()
        }
    }
    
    var toolbar: FormToolbar = FormToolbar()
    
    private weak var activeInput: FormInput?
    var inputs = [FormInput]()
    
    @IBAction func checkoutClick(){
        //MARK: - If User Not Logged in Sabzilana App -
        /*
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to login before checkout")
        }else{
            let basketLocalResul = NSArray(array: GetBasketLocalResult())
            if basketLocalResul.count > 0 {
                
                let objRoot = DeliveryAddressViewController(nibName: "DeliveryAddressViewController", bundle: nil)
                self.navigationController?.pushViewController(objRoot, animated: true)
            }
        }
        */
        
    }
    
    func LoadResultUI(){
        print(self.productDetailsResult)
        if self.tableview != nil {
            let nibName = UINib(nibName: "ProductDetailsCell", bundle: nil)
            self.tableview .register(nibName, forCellReuseIdentifier: "ProductDetailsCell")
            
            let nibName3 = UINib(nibName: "ProductTableCell", bundle: nil)
            self.tableview .register(nibName3, forCellReuseIdentifier: "ProductTableCell")
            
            let nibName2 = UINib(nibName: "SpecificationCell", bundle: nil)
            self.tableview .register(nibName2, forCellReuseIdentifier: "SpecificationCell")
            
            if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductDetailsCell") as? ProductDetailsCell {
                cell.delegateSelf = self
                cell.delegate = self
                cell.isWishListEnable = self.isWishListEnable
                cell.resultData = self.productDetailsResult
                self.NoOfResultCell.add(cell)
            }
            
            if let cell = self.tableview.dequeueReusableCell(withIdentifier: "SpecificationCell") as? SpecificationCell {
                cell.delegate = self
                cell.resultData = self.productDetailsResult
                cell.specificationDetailList = NSArray(array: self.specificationDetailList)
            
                if let text = cell.reviewText {
                    inputs.append(text)
                }
                
                toolbar = FormToolbar(inputs: self.inputs)
                self.NoOfResultCell.add(cell)
            }
            
            DispatchQueue.main.async
                {
                    
                    self.tableview.delegate = self
                    self.tableview.dataSource = self
                    self.tableview.ReloadWithAnimation()
                   
            }
             
            self.setNavigationTitle()
            
        }else{
            self.perform(#selector(ProductDetailsViewController.LoadResultUI), with: nil, afterDelay: 0.2)
        }
    }
    func  setNavigationTitle(){
        if self.navigationController != nil {
            if let tempStr = self.productResult.object(forKey: "name") as? String {
                if tempStr.isEmpty == false {
                    self.navigationItem.title = tempStr
                }else{
                    self.navigationItem.title = "Home Dezin"
                }
                
            }else{
                self.navigationItem.title = "Home Dezin"
            }
        }else {
            self.perform(#selector(ProductDetailsViewController.setNavigationTitle), with: nil, afterDelay: 0.1)
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableview.estimatedRowHeight = 487
        self.BarButtonBasket()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func BarButtonBasket(){
        
        let rightButtonActivity = UIButton(frame: CGRect(x: 0,y: 0,width: 40,height: 40))
        rightButtonActivity.addTarget(self, action: #selector(BasketClic), for: UIControlEvents.touchUpInside)
        rightButtonActivity.setImage(UIImage(named: "newcart"), for: UIControlState())
        rightButtonActivity.setTitleColor(UIColor.blue, for: UIControlState())
        rightButtonActivity.sizeToFit()
        
        self.barButtonBasket = MJBadgeBarButton()
        self.barButtonBasket.setup(rightButtonActivity)
        self.barButtonBasket.badgeValue = ""
        self.barButtonBasket.badgeOriginX = 10.0
        self.barButtonBasket.badgeOriginY = -6
        
//        self.navigationItem.rightBarButtonItem = self.barButtonBasket
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 0.0
        let search = UIBarButtonItem(image: #imageLiteral(resourceName: "newsearch"), style: .plain, target: self, action: #selector(ProductDetailsViewController.SearchClick))
        
        
        self.navigationItem.rightBarButtonItems = [search]
        //        self.navigationItem.rightBarButtonItems = [self.barButtonBasket,fixedSpace,search]
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d", arguments: [totalItems])
        self.barButtonBasket.badgeValue = priceStr
        
        self.UpdateBasketPriceNItems()
        
        let homeButton = UIBarButtonItem(image:  #imageLiteral(resourceName: "home_icon"), style: .plain, target: self, action: #selector(self.HomeButtonClick))
        self.navigationItem.rightBarButtonItems = [homeButton]
    }
    @objc func HomeButtonClick(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    func SearchClick(){
        let objRoot = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)

    }
    func BasketClic(){
        let objRoot = BasketViewController(nibName: "BasketViewController", bundle: nil)
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    func GetProductDetails(){
        print(self.productResult)
        var productId = ""
        if let tempStr = self.productResult.object(forKey: "productID") as? String {
            productId = tempStr
        }
        
        
        let urlStr = String(format: "%@?view=product_details&userID=%@&productID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,productId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                
                //New Product
                if let result = data as? NSDictionary {
                    if let detail_list = result.object(forKey: "detail_list") as? NSArray {
                        self.specificationDetailList = NSArray(array: detail_list)
                    }
                    if let resultList = result.object(forKey: "product") as? NSArray {
                        if resultList.count > 0 {
                            if let data = resultList.object(at: 0) as? NSDictionary {
                                self.productDetailsResult = NSDictionary(dictionary: data)
                                DispatchQueue.main.async
                                    {
                                        self.LoadResultUI()
                                }
                            }
                        }
                        
                    }
                }
                
            }
            DispatchQueue.main.async
                {
                    self.GetRelatedProduct()
            }
                        
            
        }
        
    }


    func GetRelatedProduct(){
        
        
        var productId = ""
        if let tempStr = self.productResult.object(forKey: "productID") as? String {
            productId = tempStr
        }
        
        
        let urlStr = String(format: "%@?view=related_product&userID=%@&productID=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,productId])
        print(urlStr)
        
        TNMWSMethod(nil, url: urlStr, isMethod: kGetMethod, AuthToken: "") { (succeeded, data) in
            if succeeded == true {
                print(data)
                
                //New Product
                if let result = data as? NSDictionary {
                    
                    if let matching_product = result.object(forKey: "matching_product") as? String {
                        if matching_product.caseInsensitiveCompare("yes") == .orderedSame {
                            if let matching_product_list = result.object(forKey: "matching_product_list") as? NSArray {
                                if matching_product_list.count > 0 {
                                    if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                                        cell.productTitleLabel.text = "Matching Products"
                                        cell.ResponseResult = matching_product_list
                                        self.NoOfResultCell.add(cell)
                                        
                                        DispatchQueue.main.async
                                            {
                                                self.tableview.delegate = self
                                                self.tableview.dataSource = self
                                                self.tableview.ReloadWithAnimation()
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                    if let related_product = result.object(forKey: "related_product") as? NSArray {
                        if related_product.count > 0 {
                            if let cell = self.tableview.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                                cell.productTitleLabel.text = "Related Product"
                                cell.ResponseResult = related_product
                                self.NoOfResultCell.add(cell)
                                
                                DispatchQueue.main.async
                                    {
                                        self.tableview.delegate = self
                                        self.tableview.dataSource = self
                                        self.tableview.ReloadWithAnimation()
                                }
                            }
                        }
                        
                        
                        
                    }
                }
                
            }
        }
        
    }

    //MARK:- Action -
    @IBAction func InquiryClick(){
        
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to be signed in to place inquiry.")
            return
        }
        
        DispatchQueue.main.async {
            let controller = AddInquiryViewController(nibName: "AddInquiryViewController", bundle: nil)
            controller.productDetailsResult =  self.productDetailsResult
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    //MARK:- Tableview Delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NoOfResultCell.count
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if let _ = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
//            return 310
//        }else {
//            return UITableViewAutomaticDimension
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
            return 330
        }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? SpecificationCell {
            return 240
        }
        else {
            return UITableViewAutomaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductDetailsCell {
            cell.delegate = self
            return cell
        }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
            cell.delegate = self
            return cell
        }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? SpecificationCell {
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- Specification cell delegate -
    func writeReview(cell: SpecificationCell) {
        if let text = cell.reviewText?.text {
            if text.isEmpty {
                ShowMessage("", message: "Please enter message")
                return
            }
            
            let start = cell.rateEZ.value
            
            
//            http://www.homedezin.com/mapp/index.php?view=add_review&userID=NDU=&productID=ODc=&star=2&review_msg=hiiiiiii
            self.view.endEditing(true)
            
            
            var productId = ""
            if let product = self.productDetailsResult.object(forKey: "productID") as? String {
                productId = product
            }
            
         
            let urlStr = String(format: "%@?view=add_review&userID=%@&productID=%@&star=%0.1f&review_msg=%@", arguments: [kMainDomainUrl,appDelegate.SabzilanaLoginUserId,productId,start,text])
            
            print(urlStr)
            
            TNMWSMethod(nil, url: urlStr, isMethod: kPostMethod, AuthToken: "") { (succeeded, data) in
                print(data)
                
                if succeeded == true {
                    if let result = data as? NSDictionary {
                        if let statusCode = result.object(forKey: kStatusCode) as? String {
                            if statusCode == "0" {
                                DispatchQueue.main.async {
                                    let controller = InquiryFinishViewController(nibName: "InquiryFinishViewController", bundle: nil)
                                    if let message = result.object(forKey: kMessage) as? String {
                                        controller.message = message
                                    }
                                    self.navigationController?.pushViewController(controller, animated: true)
                                }
                            }else{
                                if let message = result.object(forKey: kMessage) as? String {
                                    ShowMessage("", message: message)
                                }
                                
                            }
                        }
                    }
                }
                
            }
            
        }
    }
    
    func viewReviewProductDetails(cell: ProductDetailsCell) {
        let controller = ReviewsViewController(nibName: "ReviewsViewController", bundle: nil)
        controller.productDetailsResult = self.productDetailsResult
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func viewReviews(cell: SpecificationCell) {
        let controller = ReviewsViewController(nibName: "ReviewsViewController", bundle: nil)
        controller.productDetailsResult = self.productDetailsResult
        self.navigationController?.pushViewController(controller, animated: true)
    }
    //MARK:- Cell Delegate -
    func didTapImageView(data: NSDictionary) {
        
        
        if let list = data.object(forKey: "image_list") as? NSArray {
            let objRoot = FullscreenImageViewController(nibName: "FullscreenImageViewController", bundle: nil)
            objRoot.imageList = NSArray(array: list)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
        
    }
    func didUpdateBasket() {
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        let totalItems = basketLocalResul.count
        let priceStr = String(format: "%d", arguments: [totalItems])
        self.barButtonBasket.badgeValue = priceStr
        
        self.UpdateBasketPriceNItems()
    }
    
    func shareProduct(cell: ProductDetailsCell) {
        print(cell.resultData)
        var share_text = ""
        if let tempStr = cell.resultData.object(forKey: "share_text") as? String {
            share_text = tempStr
        }
        let shareImage = cell.shareImage
        
        
        let vc = UIActivityViewController(activityItems: [share_text, shareImage], applicationActivities: [])
        self.present(vc, animated: true)
    }
    func UpdateBasketPriceNItems() {
        
        let basketLocalResul = NSArray(array: GetBasketLocalResult())
        var basketTotalPrice : Float = 0
        
        for tempData in basketLocalResul {
            if let resultData = tempData as? NSDictionary {
                if let tempQty = resultData.object(forKey: kBasketQtyKey) as? String {
                    var LiveItemQty : Float = 0
                    if let tempQty = Float(tempQty) {
                        LiveItemQty = tempQty
                    }
                    
                    //Get Prices Data
                    if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                        var ProductItemId = ""
                        if let tempStr = resultData.object(forKey: kProductItemID) as? String {
                            ProductItemId = tempStr
                        }
                        for tempData in priceList {
                            if let itemData = tempData as? NSDictionary {
                                if let itemId = itemData.object(forKey: "price_ID") as? String {
                                    if ProductItemId == itemId {
                                        //GetPrice
                                        var LiveItemPrice : Float = 0
                                        if let itemPrice = itemData.object(forKey: "price") as? String {
                                            
                                            if let tempPrice = Float(itemPrice) {
                                                LiveItemPrice = tempPrice
                                            }
                                        }
                                        
                                        basketTotalPrice = basketTotalPrice + (LiveItemQty * LiveItemPrice)
                                        break
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
            }
        }
        
        
//        let priceStr = String(format: "₹ %.2f", arguments: [basketTotalPrice])
//        self.btnBasket.setTitle(priceStr, for: .normal)
//        self.btnBasket.isEnabled = false
        
    }
    func wishlistDidDeleteRecord(data: NSDictionary) {
        
    }
    
    //MARK:- Cell Delegate -
    //MARK:- Cell Category Delegate -
    func didLoadCategoryResult(data: NSDictionary) {
        print(data)
        
        if data.object(forKey: "productID") != nil {
            let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }
    
    //MARK:- TextField & KeyBoard Delegate -
    func textFieldDidBeginEditing(_ textField: UITextField) {
        toolbar.update()
        activeInput = textField
        
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 16.0, right: 0)
        tableview.contentInset = contentInsets
        tableview.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        tableview.contentInset = .zero
        tableview.scrollIndicatorInsets = .zero
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Manage Favourite -
    @IBAction func FavouriteClick(){
        
        //MARK: - If User Not Logged in Sabzilana App -
        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
            appDelegate.LoginRequired(message: "You need to be signed in to Create and Save your own wishlist.")
            return
        }
        
        if let tempStr = self.productDetailsResult.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    var productId = ""
                    
                    if let dbData = wishListResult.object(at: 0) as? NSDictionary {
                        //DB Data
                        
                        appDelegate.TNMErrorMessage(message: "Product Already Added wishlist")
//                        if let tempStr = dbData.object(forKey: kWishListProductID) as? String {
//                            productId = tempStr
//                        }
//
//                        //Delete Query
//                        let queryDelete = String(format: "DELETE FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,productId)
//                        MyDbManager.sharedClass().exicuteSQLiteQuery(queryDelete)
                    }
                }else{
                    if tempStr.isEmpty == false {
                        let insertData = [kWishListProductID : tempStr]
                        MyDbManager.sharedClass().insert(into: kWishListTableName, field: insertData )
                        appDelegate.TNMErrorMessage(message: "Product Add wishlist successful")
                    }
                }
            }
           
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
