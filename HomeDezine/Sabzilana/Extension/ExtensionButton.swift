//
//  ExtensionTableview.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//


import UIKit

extension UIButton {
    
    func UnderLineText(text : String){
        let textRange = NSMakeRange(0, text.characters.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        attributedText.addAttribute(NSForegroundColorAttributeName, value: NSTheme().GetNavigationBGColor() , range: textRange)
        // Add other attributes if needed
        self.setAttributedTitle(attributedText, for: .normal)
        
    }
    
    

}

extension UITextField{
    @IBInspectable var placeHolderColorNew: UIColor? {
        get {
            return self.placeHolderColorNew
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}

