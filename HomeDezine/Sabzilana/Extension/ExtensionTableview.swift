//
//  ExtensionTableview.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//


import UIKit

extension UITableView {
    func ReloadWithAnimation(){
        UIView.transition(with: self,
                          duration: 0.333,
                          options: [.transitionCrossDissolve],
                          animations: { () -> Void in
                            self.reloadData()
                            //                                            self.tableview.reloadSections(NSIndexSet(index: 0), withRowAnimation: .None)
        }, completion: nil)
    }

}
