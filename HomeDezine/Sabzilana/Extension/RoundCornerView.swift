//
//  Round.swift
//  synax
//
//  Created by TNM3 on 10/26/15.
//  Copyright © 2015 TNM3. All rights reserved.
//

import UIKit

class RoundCornerView: UIView {

    @IBInspectable var CornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = CornerRadius
//            layer.masksToBounds = CornerRadius > 0
        }
    }
    @IBInspectable var BorderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = BorderWidth
        }
    }
    @IBInspectable var BorderColor: UIColor? {
        didSet {
            layer.borderColor = BorderColor?.cgColor
        }
    }
    @IBInspectable var isShadow: Bool = false {
        didSet {
            if isShadow == true {
                layer.shadowColor = UIColor.gray.cgColor
                layer.masksToBounds = false
                layer.shadowOffset = CGSize(width: 0.0 , height: 3.0)
                layer.shadowOpacity = 0.7
                layer.shadowRadius = 0.7
                
            }
        }
    }
    
    
    init() {
        super.init(frame: CGRect.zero)
        
        //        println("init")
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //        println("override init")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //        println("required init")
    }
    
    func roundCorner(){
//        self.layer.cornerRadius = 5
//        self.clipsToBounds = true
//        self.backgroundColor = NSTheme().GetButtonBGColor()
//        self.setTitleColor(NSTheme().GetButtonTextColor(), forState: UIControlState.Normal)

    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
