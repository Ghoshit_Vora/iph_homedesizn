//
//  ExtensionTableview.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//


import UIKit

extension UILabel {
    func StrikeText(text : String){
        let textRange = NSMakeRange(0, text.characters.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSStrikethroughStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        // Add other attributes if needed
        self.attributedText = attributedText
    }
    func UnderLineText(text : String) -> NSAttributedString{
        let textRange = NSMakeRange(0, text.characters.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        // Add other attributes if needed
        return attributedText
    }
    
    func HintText(hintText : String, normalText : String) -> NSAttributedString{
        let yourAttributes = [NSForegroundColorAttributeName: UIColor.blue, NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        let yourOtherAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 15)]
        
        let partOne = NSMutableAttributedString(string: hintText, attributes: yourAttributes)
        let partTwo = NSMutableAttributedString(string: normalText, attributes: yourOtherAttributes)
        
        let combination = NSMutableAttributedString()
        
        combination.append(partOne)
        combination.append(partTwo)
        return combination
    }

}
