//
//  MyShareClass.h
//  DataBase
//
//  Created by Jigar Zalavadiya on 24/01/14.
//  Copyright (c) 2014 OEPL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyShareClass : NSObject
{
    NSString *strName;
    NSString *strAddress;
    NSString *strPhone;
    
}
@property (nonatomic,strong) NSString *strName;
@property (nonatomic,strong) NSString *strAddress;
@property (nonatomic,strong) NSString *strPhone;
-(id)init;
@end
