//
//  CopyData.m
//  Llyn
//
//  Created by tnmmac4 on 19/04/16.
//  Copyright © 2016 shoebpersonal. All rights reserved.
//

#import "CopyData.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "EncryptedStore.h"

#define USE_ENCRYPTED_STORE 1

@implementation CopyData

-(void)CoyDatabase {
    
 //   static NSPersistentStoreCoordinator *coordinator = nil;
    
    NSString *pathsToReources = [[NSBundle mainBundle] resourcePath];
    NSString *yourOriginalDatabasePath = [pathsToReources stringByAppendingPathComponent:@"Sabzilana.sqlite"];
    
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex: 0];
    
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"Sabzilana.sqlite"];
    NSLog(@"DataBase Path :: %@", dbPath);
    if (![[NSFileManager defaultManager] isReadableFileAtPath: dbPath]) {
        
        if ([[NSFileManager defaultManager] copyItemAtPath: yourOriginalDatabasePath toPath: dbPath error: NULL] != YES){
            NSLog(@"Fail to copy database from %@ to %@",yourOriginalDatabasePath, dbPath);
//            NSAssert2(0, @"Fail to copy database from %@ to %@", yourOriginalDatabasePath, dbPath);
        }
        
    }
}
-(void)CopyFavouriteDatabase{
    NSString *pathsToReourcesFav = [[NSBundle mainBundle] resourcePath];
    NSString *yourOriginalDatabasePathFav = [pathsToReourcesFav stringByAppendingPathComponent:@"favorites.sqlite"];
    NSArray *pathsToDocumentsFav = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryFav = [pathsToDocumentsFav objectAtIndex: 0];
    NSString *dbPathFav = [documentsDirectoryFav stringByAppendingPathComponent:@"favorites.sqlite"];
    NSLog(@"DataBase Path :: %@", dbPathFav);
    
    if (![[NSFileManager defaultManager] isReadableFileAtPath: dbPathFav])
    {
        
        if ([[NSFileManager defaultManager] copyItemAtPath: yourOriginalDatabasePathFav toPath: dbPathFav error: NULL] != YES){
            NSLog(@"Fail to copy database from %@ to %@",yourOriginalDatabasePathFav, dbPathFav);
        }
        
    }
}

-(void)RemoveDatabase {
    
    //   static NSPersistentStoreCoordinator *coordinator = nil;
    
//    NSString *pathsToReources = [[NSBundle mainBundle] resourcePath];
//    NSString *yourOriginalDatabasePath = [pathsToReources stringByAppendingPathComponent:@"50_ROOT.sqlite"];
    
    NSArray *pathsToDocuments = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [pathsToDocuments objectAtIndex: 0];
    
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"Sabzilana.sqlite"];
    NSLog(@"DataBase Path :: %@", dbPath);
    NSError *error;
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:dbPath]) {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:dbPath error:&error];
        if (!success) {
            NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }else{
             NSLog(@"Delete DB");
        }
    }
    
}


//        NSDictionary *dic = [NSDictionary dictionaryWithObject:NSFileProtectionComplete forKey:NSFileProtectionKey];
//
//        [[NSFileManager defaultManager] setAttributes:dic ofItemAtPath:dbPath error: NULL];



/// if ([[NSFileManager defaultManager] copyItemAtPath: yourOriginalDatabasePath toPath: dbPath error: NULL] != YES)
//    NSAssert2(0, @"Fail to copy database from %@ to %@", yourOriginalDatabasePath, dbPath);

//        NSURL *url = [[NSURL alloc] initWithString:dbPath];
//        NSError *error = nil;
//
//        NSDictionary *options = @{
//                                  EncryptedStorePassphraseKey : @"DB_KEY_HERE",
//                                  //            EncryptedStoreDatabaseLocation : databaseURL,
//                                  //            NSMigratePersistentStoresAutomaticallyOption : @YES,
//                                  NSInferMappingModelAutomaticallyOption : @YES
//                                  };
//        NSPersistentStore *store = [coordinator
//                                    addPersistentStoreWithType:EncryptedStoreType
//                                    configuration:nil
//                                    URL:url
//                                    options:options
//                                    error:&error];
//         NSAssert(store, @"Unable to add persistent store!\n%@", error);

-(void) openUrl:(NSString *) urlString
{

    //check if facebook app exists
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
        
        // Facebook app installed
        NSArray *tokens = [urlString componentsSeparatedByString:@"/"];
        NSString *profileName = [tokens lastObject];
        
        //call graph api
        NSURL *apiUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@",profileName]];
        NSData *apiResponse = [NSData dataWithContentsOfURL:apiUrl];
        
        NSError *error = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:apiResponse options:NSJSONReadingMutableContainers error:&error];
        
        //check for parse error
        if (error == nil) {
            
            NSString *profileId = [jsonDict objectForKey:@"id"];
            
            if (profileId.length > 0) {//make sure id key is actually available
                NSURL *fbUrl = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",profileId]];
                [[UIApplication sharedApplication] openURL:fbUrl];
            }
            else{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }
            
        }
        else{//parse error occured
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
        
    }
    else{//facebook app not installed
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
    
}


@end
