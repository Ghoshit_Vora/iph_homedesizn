//
//  AppDelegate.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

//com.sabzilana.store

import UIKit
import Fabric
import Crashlytics

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var SabzilanaLoginUserId : String = ""
    var window: UIWindow?
    var navigationController : UINavigationController = UINavigationController()
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        
        //Tempary
        
        Fabric.with([Crashlytics.self])
        
        let data = getLoginUser()
//        print(data)
        if let tempStr = data.object(forKey: "userID") as? String {
            self.SabzilanaLoginUserId = tempStr
            
        }
        let viewController = ProductHomeViewController(nibName: "ProductHomeViewController", bundle: nil)
//        let viewController = FilterProductViewController(nibName: "FilterProductViewController", bundle: nil)
        
//        let viewController = AddInquiryViewController(nibName: "AddInquiryViewController", bundle: nil)

        
        self.navigationController = UINavigationController(rootViewController: viewController)
        self.styleNavigationController(self.navigationController)
        self.window?.rootViewController = self.navigationController
        self.window?.makeKeyAndVisible()
        /*
        if self.SabzilanaLoginUserId.isEmpty == true || self.SabzilanaLoginUserId == "0" {
//            let viewController = HelpViewController(nibName: "HelpViewController", bundle: nil)
            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController = UINavigationController(rootViewController: viewController)
            self.styleNavigationController(self.navigationController)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }else{
            let viewController = ProductHomeViewController(nibName: "ProductHomeViewController", bundle: nil)
            self.navigationController = UINavigationController(rootViewController: viewController)
            self.styleNavigationController(self.navigationController)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }
        */
        //Init PushNotification
        self.ConfigurationPushNotification()
        
        //Copy SQLite Database
        CopyData().coyDatabase()
        
//        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
//    {
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//    }
    
    func styleNavigationController(_ navigationController: UINavigationController){
        navigationController.navigationBar.barTintColor = NSTheme().GetNavigationBGColor()
        //        navigationController.navigationBar.tintColor = NSTheme().GetNavigationTintColor()
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.isTranslucent = false
        //        self.navigationController!.navigationBar.tintColor = NSTheme().GetNavigationBGColor()
        //        self.navigationController!.navigationBar.barTintColor = NSTheme().GetNavigationBGColor()
        navigationController.navigationBar.barStyle = .blackTranslucent
    }

    func ConfigurationPushNotification(){
        let application = UIApplication.shared
        if application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))) {
            
            let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound])
            let settings = UIUserNotificationSettings(types: userNotificationTypes, categories: nil)
            //            let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: categories as? Set<UIUserNotificationCategory>)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            
        }
        
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        kDeviceToken = token
        
        print("Token: ", token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print(error.localizedDescription)
        kDeviceToken = "0"
    }
    
    // TODO: Rewrite this method with notifications
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print(userInfo)
        
        
    }
    func application(_ application: UIApplication,  didReceiveRemoteNotification userInfo: [AnyHashable: Any],  fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //        self.didReciverNotifcation(userInfo)
        completionHandler(.newData)
        
        print(userInfo)
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // share image
    func ShareAppObject(_ ShareList: NSMutableArray) {
      let shareResult = NSArray(array: ShareList)
        
        let activityViewController = UIActivityViewController(activityItems: shareResult as! [Any], applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//        
//        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        DispatchQueue.main.async {
            self.window?.rootViewController?.present(activityViewController, animated: true, completion: { 
                
            })
           
        }
        
    }
    
    func LoginRequired(message : String){
        var messageStr = "You need to login to use this feature"
        if message.isEmpty == false {
            messageStr = message
        }
        
        let refreshAlert = UIAlertController(title: "Login", message: messageStr, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Login", style: .default, handler: { (action: UIAlertAction!) in
            let viewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController = UINavigationController(rootViewController: viewController)
            self.styleNavigationController(self.navigationController)
            self.window?.rootViewController = self.navigationController
            
        }))
        DispatchQueue.main.async {
            self.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
    
    
    func TNMErrorMessage(message : String){
        
        let refreshAlert = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
            
        }))
        DispatchQueue.main.async {
            self.window?.visibleViewController?.present(refreshAlert, animated: true, completion: { () -> Void in
                
            })
            
        }
    }
    
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
